package be.jhipster.recruitment.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

import be.jhipster.recruitment.domain.enumeration.CandidacyStatus;

/**
 * A Candidacy.
 */
@Entity
@Table(name = "candidacy")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Candidacy implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "candidacy_status", nullable = false)
    private CandidacyStatus candidacyStatus;

    @NotNull
    @Column(name = "motivation", nullable = false)
    private String motivation;

    @ManyToOne
    @JsonIgnoreProperties("candidacies")
    private Candidate candidate;

    @ManyToOne
    @JsonIgnoreProperties("candidacies")
    private Vacancy vacancy;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CandidacyStatus getCandidacyStatus() {
        return candidacyStatus;
    }

    public Candidacy candidacyStatus(CandidacyStatus candidacyStatus) {
        this.candidacyStatus = candidacyStatus;
        return this;
    }

    public void setCandidacyStatus(CandidacyStatus candidacyStatus) {
        this.candidacyStatus = candidacyStatus;
    }

    public String getMotivation() {
        return motivation;
    }

    public Candidacy motivation(String motivation) {
        this.motivation = motivation;
        return this;
    }

    public void setMotivation(String motivation) {
        this.motivation = motivation;
    }

    public Candidate getCandidate() {
        return candidate;
    }

    public Candidacy candidate(Candidate candidate) {
        this.candidate = candidate;
        return this;
    }

    public void setCandidate(Candidate candidate) {
        this.candidate = candidate;
    }

    public Vacancy getVacancy() {
        return vacancy;
    }

    public Candidacy vacancy(Vacancy vacancy) {
        this.vacancy = vacancy;
        return this;
    }

    public void setVacancy(Vacancy vacancy) {
        this.vacancy = vacancy;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Candidacy)) {
            return false;
        }
        return id != null && id.equals(((Candidacy) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Candidacy{" +
            "id=" + getId() +
            ", candidacyStatus='" + getCandidacyStatus() + "'" +
            ", motivation='" + getMotivation() + "'" +
            "}";
    }
}
