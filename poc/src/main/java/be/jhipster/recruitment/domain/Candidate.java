package be.jhipster.recruitment.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import be.jhipster.recruitment.domain.enumeration.CandidateStatus;

import be.jhipster.recruitment.domain.enumeration.CandidateFuntion;

import be.jhipster.recruitment.domain.enumeration.CandidateTrack;

import be.jhipster.recruitment.domain.enumeration.RecruitmentChannel;

/**
 * A Candidate.
 */
@Entity
@Table(name = "candidate")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Candidate implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull
    @Column(name = "phone", nullable = false)
    private String phone;

    @NotNull
    @Column(name = "email", nullable = false)
    private String email;

    @Enumerated(EnumType.STRING)
    @Column(name = "candidate_status")
    private CandidateStatus candidateStatus;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "candidate_function", nullable = false)
    private CandidateFuntion candidateFunction;

    @NotNull
    @Column(name = "date_available", nullable = false)
    private String dateAvailable;

    @Lob
    @Column(name = "image_content")
    private byte[] imageContent;

    @Column(name = "image_content_content_type")
    private String imageContentContentType;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "candidate_track", nullable = false)
    private CandidateTrack candidateTrack;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "recruitment_channel", nullable = false)
    private RecruitmentChannel recruitmentChannel;

    @Column(name = "linked_in_profile_path")
    private String linkedInProfilePath;

    @OneToOne
    @JoinColumn(unique = true)
    private Document document;

    @ManyToOne
    @JsonIgnoreProperties("candidates")
    private CandidateSubClass subClass;

    @OneToMany(mappedBy = "candidate")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Candidacy> candidacies = new HashSet<>();

    @OneToMany(mappedBy = "candidate")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<CandidateComment> candidateComments = new HashSet<>();

    @OneToMany(mappedBy = "candidate")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<ContactMoment> contactMoments = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Candidate name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public Candidate phone(String phone) {
        this.phone = phone;
        return this;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public Candidate email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public CandidateStatus getCandidateStatus() {
        return candidateStatus;
    }

    public Candidate candidateStatus(CandidateStatus candidateStatus) {
        this.candidateStatus = candidateStatus;
        return this;
    }

    public void setCandidateStatus(CandidateStatus candidateStatus) {
        this.candidateStatus = candidateStatus;
    }

    public CandidateFuntion getCandidateFunction() {
        return candidateFunction;
    }

    public Candidate candidateFunction(CandidateFuntion candidateFunction) {
        this.candidateFunction = candidateFunction;
        return this;
    }

    public void setCandidateFunction(CandidateFuntion candidateFunction) {
        this.candidateFunction = candidateFunction;
    }

    public String getDateAvailable() {
        return dateAvailable;
    }

    public Candidate dateAvailable(String dateAvailable) {
        this.dateAvailable = dateAvailable;
        return this;
    }

    public void setDateAvailable(String dateAvailable) {
        this.dateAvailable = dateAvailable;
    }

    public byte[] getImageContent() {
        return imageContent;
    }

    public Candidate imageContent(byte[] imageContent) {
        this.imageContent = imageContent;
        return this;
    }

    public void setImageContent(byte[] imageContent) {
        this.imageContent = imageContent;
    }

    public String getImageContentContentType() {
        return imageContentContentType;
    }

    public Candidate imageContentContentType(String imageContentContentType) {
        this.imageContentContentType = imageContentContentType;
        return this;
    }

    public void setImageContentContentType(String imageContentContentType) {
        this.imageContentContentType = imageContentContentType;
    }

    public CandidateTrack getCandidateTrack() {
        return candidateTrack;
    }

    public Candidate candidateTrack(CandidateTrack candidateTrack) {
        this.candidateTrack = candidateTrack;
        return this;
    }

    public void setCandidateTrack(CandidateTrack candidateTrack) {
        this.candidateTrack = candidateTrack;
    }

    public RecruitmentChannel getRecruitmentChannel() {
        return recruitmentChannel;
    }

    public Candidate recruitmentChannel(RecruitmentChannel recruitmentChannel) {
        this.recruitmentChannel = recruitmentChannel;
        return this;
    }

    public void setRecruitmentChannel(RecruitmentChannel recruitmentChannel) {
        this.recruitmentChannel = recruitmentChannel;
    }

    public String getLinkedInProfilePath() {
        return linkedInProfilePath;
    }

    public Candidate linkedInProfilePath(String linkedInProfilePath) {
        this.linkedInProfilePath = linkedInProfilePath;
        return this;
    }

    public void setLinkedInProfilePath(String linkedInProfilePath) {
        this.linkedInProfilePath = linkedInProfilePath;
    }

    public Document getDocument() {
        return document;
    }

    public Candidate document(Document document) {
        this.document = document;
        return this;
    }

    public void setDocument(Document document) {
        this.document = document;
    }

    public CandidateSubClass getSubClass() {
        return subClass;
    }

    public Candidate subClass(CandidateSubClass candidateSubClass) {
        this.subClass = candidateSubClass;
        return this;
    }

    public void setSubClass(CandidateSubClass candidateSubClass) {
        this.subClass = candidateSubClass;
    }

    public Set<Candidacy> getCandidacies() {
        return candidacies;
    }

    public Candidate candidacies(Set<Candidacy> candidacies) {
        this.candidacies = candidacies;
        return this;
    }

    public Candidate addCandidacies(Candidacy candidacy) {
        this.candidacies.add(candidacy);
        candidacy.setCandidate(this);
        return this;
    }

    public Candidate removeCandidacies(Candidacy candidacy) {
        this.candidacies.remove(candidacy);
        candidacy.setCandidate(null);
        return this;
    }

    public void setCandidacies(Set<Candidacy> candidacies) {
        this.candidacies = candidacies;
    }

    public Set<CandidateComment> getCandidateComments() {
        return candidateComments;
    }

    public Candidate candidateComments(Set<CandidateComment> candidateComments) {
        this.candidateComments = candidateComments;
        return this;
    }

    public Candidate addCandidateComments(CandidateComment candidateComment) {
        this.candidateComments.add(candidateComment);
        candidateComment.setCandidate(this);
        return this;
    }

    public Candidate removeCandidateComments(CandidateComment candidateComment) {
        this.candidateComments.remove(candidateComment);
        candidateComment.setCandidate(null);
        return this;
    }

    public void setCandidateComments(Set<CandidateComment> candidateComments) {
        this.candidateComments = candidateComments;
    }

    public Set<ContactMoment> getContactMoments() {
        return contactMoments;
    }

    public Candidate contactMoments(Set<ContactMoment> contactMoments) {
        this.contactMoments = contactMoments;
        return this;
    }

    public Candidate addContactMoments(ContactMoment contactMoment) {
        this.contactMoments.add(contactMoment);
        contactMoment.setCandidate(this);
        return this;
    }

    public Candidate removeContactMoments(ContactMoment contactMoment) {
        this.contactMoments.remove(contactMoment);
        contactMoment.setCandidate(null);
        return this;
    }

    public void setContactMoments(Set<ContactMoment> contactMoments) {
        this.contactMoments = contactMoments;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Candidate)) {
            return false;
        }
        return id != null && id.equals(((Candidate) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Candidate{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", phone='" + getPhone() + "'" +
            ", email='" + getEmail() + "'" +
            ", candidateStatus='" + getCandidateStatus() + "'" +
            ", candidateFunction='" + getCandidateFunction() + "'" +
            ", dateAvailable='" + getDateAvailable() + "'" +
            ", imageContent='" + getImageContent() + "'" +
            ", imageContentContentType='" + getImageContentContentType() + "'" +
            ", candidateTrack='" + getCandidateTrack() + "'" +
            ", recruitmentChannel='" + getRecruitmentChannel() + "'" +
            ", linkedInProfilePath='" + getLinkedInProfilePath() + "'" +
            "}";
    }
}
