package be.jhipster.recruitment.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A CandidateComment.
 */
@Entity
@Table(name = "candidate_comment")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class CandidateComment implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "context", nullable = false)
    private String context;

    @NotNull
    @Column(name = "commenter", nullable = false)
    private String commenter;

    @ManyToOne
    @JsonIgnoreProperties("candidateComments")
    private Candidate candidate;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContext() {
        return context;
    }

    public CandidateComment context(String context) {
        this.context = context;
        return this;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public String getCommenter() {
        return commenter;
    }

    public CandidateComment commenter(String commenter) {
        this.commenter = commenter;
        return this;
    }

    public void setCommenter(String commenter) {
        this.commenter = commenter;
    }

    public Candidate getCandidate() {
        return candidate;
    }

    public CandidateComment candidate(Candidate candidate) {
        this.candidate = candidate;
        return this;
    }

    public void setCandidate(Candidate candidate) {
        this.candidate = candidate;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CandidateComment)) {
            return false;
        }
        return id != null && id.equals(((CandidateComment) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "CandidateComment{" +
            "id=" + getId() +
            ", context='" + getContext() + "'" +
            ", commenter='" + getCommenter() + "'" +
            "}";
    }
}
