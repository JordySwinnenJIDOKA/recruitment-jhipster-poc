package be.jhipster.recruitment.domain;


import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

import be.jhipster.recruitment.domain.enumeration.CandidateType;

/**
 * A CandidateSubClass.
 */
@Entity
@Table(name = "candidate_sub_class")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class CandidateSubClass implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "candidate_type")
    private CandidateType candidateType;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CandidateType getCandidateType() {
        return candidateType;
    }

    public CandidateSubClass candidateType(CandidateType candidateType) {
        this.candidateType = candidateType;
        return this;
    }

    public void setCandidateType(CandidateType candidateType) {
        this.candidateType = candidateType;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CandidateSubClass)) {
            return false;
        }
        return id != null && id.equals(((CandidateSubClass) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "CandidateSubClass{" +
            "id=" + getId() +
            ", candidateType='" + getCandidateType() + "'" +
            "}";
    }
}
