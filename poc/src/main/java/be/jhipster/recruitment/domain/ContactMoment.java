package be.jhipster.recruitment.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

import be.jhipster.recruitment.domain.enumeration.ContactType;

import be.jhipster.recruitment.domain.enumeration.ContactContext;

/**
 * A ContactMoment.
 */
@Entity
@Table(name = "contact_moment")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ContactMoment implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "contact_type", nullable = false)
    private ContactType contactType;

    @NotNull
    @Column(name = "contact_date", nullable = false)
    private LocalDate contactDate;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "contact_context", nullable = false)
    private ContactContext contactContext;

    @NotNull
    @Column(name = "interviewer", nullable = false)
    private String interviewer;

    @Column(name = "location")
    private String location;

    @ManyToOne
    @JsonIgnoreProperties("contactMoments")
    private Candidacy candidacy;

    @ManyToOne
    @JsonIgnoreProperties("contactMoments")
    private Candidate candidate;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ContactType getContactType() {
        return contactType;
    }

    public ContactMoment contactType(ContactType contactType) {
        this.contactType = contactType;
        return this;
    }

    public void setContactType(ContactType contactType) {
        this.contactType = contactType;
    }

    public LocalDate getContactDate() {
        return contactDate;
    }

    public ContactMoment contactDate(LocalDate contactDate) {
        this.contactDate = contactDate;
        return this;
    }

    public void setContactDate(LocalDate contactDate) {
        this.contactDate = contactDate;
    }

    public ContactContext getContactContext() {
        return contactContext;
    }

    public ContactMoment contactContext(ContactContext contactContext) {
        this.contactContext = contactContext;
        return this;
    }

    public void setContactContext(ContactContext contactContext) {
        this.contactContext = contactContext;
    }

    public String getInterviewer() {
        return interviewer;
    }

    public ContactMoment interviewer(String interviewer) {
        this.interviewer = interviewer;
        return this;
    }

    public void setInterviewer(String interviewer) {
        this.interviewer = interviewer;
    }

    public String getLocation() {
        return location;
    }

    public ContactMoment location(String location) {
        this.location = location;
        return this;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Candidacy getCandidacy() {
        return candidacy;
    }

    public ContactMoment candidacy(Candidacy candidacy) {
        this.candidacy = candidacy;
        return this;
    }

    public void setCandidacy(Candidacy candidacy) {
        this.candidacy = candidacy;
    }

    public Candidate getCandidate() {
        return candidate;
    }

    public ContactMoment candidate(Candidate candidate) {
        this.candidate = candidate;
        return this;
    }

    public void setCandidate(Candidate candidate) {
        this.candidate = candidate;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ContactMoment)) {
            return false;
        }
        return id != null && id.equals(((ContactMoment) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ContactMoment{" +
            "id=" + getId() +
            ", contactType='" + getContactType() + "'" +
            ", contactDate='" + getContactDate() + "'" +
            ", contactContext='" + getContactContext() + "'" +
            ", interviewer='" + getInterviewer() + "'" +
            ", location='" + getLocation() + "'" +
            "}";
    }
}
