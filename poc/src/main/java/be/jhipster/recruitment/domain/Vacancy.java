package be.jhipster.recruitment.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import be.jhipster.recruitment.domain.enumeration.VacancyStatus;

import be.jhipster.recruitment.domain.enumeration.CandidateType;

/**
 * A Vacancy.
 */
@Entity
@Table(name = "vacancy")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Vacancy implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "code", nullable = false)
    private String code;

    @NotNull
    @Column(name = "description", nullable = false)
    private String description;

    @NotNull
    @Column(name = "creation_date", nullable = false)
    private LocalDate creationDate;

    @Enumerated(EnumType.STRING)
    @Column(name = "vacancy_status")
    private VacancyStatus vacancyStatus;

    @Column(name = "characteristics")
    private String characteristics;

    @Column(name = "skills")
    private String skills;

    @Column(name = "daily_routine")
    private String dailyRoutine;

    @Column(name = "location")
    private String location;

    @Enumerated(EnumType.STRING)
    @Column(name = "candidate_types")
    private CandidateType candidateTypes;

    @OneToMany(mappedBy = "vacancy")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Candidacy> candidacies = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public Vacancy code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public Vacancy description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public Vacancy creationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
        return this;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public VacancyStatus getVacancyStatus() {
        return vacancyStatus;
    }

    public Vacancy vacancyStatus(VacancyStatus vacancyStatus) {
        this.vacancyStatus = vacancyStatus;
        return this;
    }

    public void setVacancyStatus(VacancyStatus vacancyStatus) {
        this.vacancyStatus = vacancyStatus;
    }

    public String getCharacteristics() {
        return characteristics;
    }

    public Vacancy characteristics(String characteristics) {
        this.characteristics = characteristics;
        return this;
    }

    public void setCharacteristics(String characteristics) {
        this.characteristics = characteristics;
    }

    public String getSkills() {
        return skills;
    }

    public Vacancy skills(String skills) {
        this.skills = skills;
        return this;
    }

    public void setSkills(String skills) {
        this.skills = skills;
    }

    public String getDailyRoutine() {
        return dailyRoutine;
    }

    public Vacancy dailyRoutine(String dailyRoutine) {
        this.dailyRoutine = dailyRoutine;
        return this;
    }

    public void setDailyRoutine(String dailyRoutine) {
        this.dailyRoutine = dailyRoutine;
    }

    public String getLocation() {
        return location;
    }

    public Vacancy location(String location) {
        this.location = location;
        return this;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public CandidateType getCandidateTypes() {
        return candidateTypes;
    }

    public Vacancy candidateTypes(CandidateType candidateTypes) {
        this.candidateTypes = candidateTypes;
        return this;
    }

    public void setCandidateTypes(CandidateType candidateTypes) {
        this.candidateTypes = candidateTypes;
    }

    public Set<Candidacy> getCandidacies() {
        return candidacies;
    }

    public Vacancy candidacies(Set<Candidacy> candidacies) {
        this.candidacies = candidacies;
        return this;
    }

    public Vacancy addCandidacies(Candidacy candidacy) {
        this.candidacies.add(candidacy);
        candidacy.setVacancy(this);
        return this;
    }

    public Vacancy removeCandidacies(Candidacy candidacy) {
        this.candidacies.remove(candidacy);
        candidacy.setVacancy(null);
        return this;
    }

    public void setCandidacies(Set<Candidacy> candidacies) {
        this.candidacies = candidacies;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Vacancy)) {
            return false;
        }
        return id != null && id.equals(((Vacancy) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Vacancy{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", description='" + getDescription() + "'" +
            ", creationDate='" + getCreationDate() + "'" +
            ", vacancyStatus='" + getVacancyStatus() + "'" +
            ", characteristics='" + getCharacteristics() + "'" +
            ", skills='" + getSkills() + "'" +
            ", dailyRoutine='" + getDailyRoutine() + "'" +
            ", location='" + getLocation() + "'" +
            ", candidateTypes='" + getCandidateTypes() + "'" +
            "}";
    }
}
