package be.jhipster.recruitment.domain.enumeration;

/**
 * The CandidacyStatus enumeration.
 */
public enum CandidacyStatus {
    CV_SCREENING_HR, TELEPHONE_INTAKE, REFERENCE_CALL, F2F_SCREENING_HR, TECHNICAL_INTERVIEW, MT_DISCUSSION, NOT_SELECTED, CANDIDATE_WITHDRAWAL, CONTRACT_OFFER, HIRED, RECRUITMENT_POOL
}
