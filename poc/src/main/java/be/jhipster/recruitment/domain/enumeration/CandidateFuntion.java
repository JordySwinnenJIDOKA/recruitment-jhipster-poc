package be.jhipster.recruitment.domain.enumeration;

/**
 * The CandidateFuntion enumeration.
 */
public enum CandidateFuntion {
    JAVA_DEVELOPER, SALES_MANAGER, BUSINESS_ANALYST, DEVOPS_ENGINEER
}
