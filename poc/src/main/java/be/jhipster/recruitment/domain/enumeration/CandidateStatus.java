package be.jhipster.recruitment.domain.enumeration;

/**
 * The CandidateStatus enumeration.
 */
public enum CandidateStatus {
    NEW_APPLICANT, SCREENING_ONGOING, HIRED, RECRUITMENT_POOL
}
