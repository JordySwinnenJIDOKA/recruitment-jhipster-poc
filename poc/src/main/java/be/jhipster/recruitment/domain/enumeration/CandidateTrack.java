package be.jhipster.recruitment.domain.enumeration;

/**
 * The CandidateTrack enumeration.
 */
public enum CandidateTrack {
    BUSINESS, TECHNICAL, GENERAL
}
