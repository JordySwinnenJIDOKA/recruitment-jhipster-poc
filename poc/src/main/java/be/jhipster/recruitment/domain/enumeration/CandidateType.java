package be.jhipster.recruitment.domain.enumeration;

/**
 * The CandidateType enumeration.
 */
public enum CandidateType {
    FREELANCE, INTERN, PAYROLL, STUDENT
}
