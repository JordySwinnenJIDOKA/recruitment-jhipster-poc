package be.jhipster.recruitment.domain.enumeration;

/**
 * The ContactContext enumeration.
 */
public enum ContactContext {
    INTRODUCTION, FIRST_MEETING, SECOND_MEETING, CONTRACT_NEGOTIATION, TECHNICAL
}
