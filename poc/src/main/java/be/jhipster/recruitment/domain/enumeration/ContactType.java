package be.jhipster.recruitment.domain.enumeration;

/**
 * The ContactType enumeration.
 */
public enum ContactType {
    PHONE_CALL, INTERVIEW, MAIL, VIDEO_CALL
}
