package be.jhipster.recruitment.domain.enumeration;

/**
 * The RecruitmentChannel enumeration.
 */
public enum RecruitmentChannel {
    WEBSITE, SOCIAL_MEDIA, JOB_FAIRS, INTERNSHIP, REFERRAL, JOB_SITE, OTHER
}
