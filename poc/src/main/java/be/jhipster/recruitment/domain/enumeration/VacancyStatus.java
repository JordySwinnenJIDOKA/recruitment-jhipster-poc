package be.jhipster.recruitment.domain.enumeration;

/**
 * The VacancyStatus enumeration.
 */
public enum VacancyStatus {
    OPEN, ON_HOLD, CLOSED
}
