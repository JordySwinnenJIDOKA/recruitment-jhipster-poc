package be.jhipster.recruitment.repository;

import be.jhipster.recruitment.domain.Candidacy;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Candidacy entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CandidacyRepository extends JpaRepository<Candidacy, Long> {

}
