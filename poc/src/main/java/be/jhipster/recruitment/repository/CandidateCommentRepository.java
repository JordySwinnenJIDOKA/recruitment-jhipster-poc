package be.jhipster.recruitment.repository;

import be.jhipster.recruitment.domain.CandidateComment;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the CandidateComment entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CandidateCommentRepository extends JpaRepository<CandidateComment, Long> {

}
