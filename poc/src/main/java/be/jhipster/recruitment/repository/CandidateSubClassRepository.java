package be.jhipster.recruitment.repository;

import be.jhipster.recruitment.domain.CandidateSubClass;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the CandidateSubClass entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CandidateSubClassRepository extends JpaRepository<CandidateSubClass, Long> {

}
