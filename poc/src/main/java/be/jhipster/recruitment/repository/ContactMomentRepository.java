package be.jhipster.recruitment.repository;

import be.jhipster.recruitment.domain.ContactMoment;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ContactMoment entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ContactMomentRepository extends JpaRepository<ContactMoment, Long> {

}
