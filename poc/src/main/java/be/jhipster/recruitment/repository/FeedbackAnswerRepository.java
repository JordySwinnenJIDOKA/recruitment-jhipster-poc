package be.jhipster.recruitment.repository;

import be.jhipster.recruitment.domain.FeedbackAnswer;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the FeedbackAnswer entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FeedbackAnswerRepository extends JpaRepository<FeedbackAnswer, Long> {

}
