package be.jhipster.recruitment.repository;

import be.jhipster.recruitment.domain.FeedbackQuestion;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the FeedbackQuestion entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FeedbackQuestionRepository extends JpaRepository<FeedbackQuestion, Long> {

}
