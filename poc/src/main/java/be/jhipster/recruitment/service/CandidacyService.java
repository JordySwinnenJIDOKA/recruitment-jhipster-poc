package be.jhipster.recruitment.service;

import be.jhipster.recruitment.service.dto.CandidacyDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link be.jhipster.recruitment.domain.Candidacy}.
 */
public interface CandidacyService {

    /**
     * Save a candidacy.
     *
     * @param candidacyDTO the entity to save.
     * @return the persisted entity.
     */
    CandidacyDTO save(CandidacyDTO candidacyDTO);

    /**
     * Get all the candidacies.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<CandidacyDTO> findAll(Pageable pageable);


    /**
     * Get the "id" candidacy.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<CandidacyDTO> findOne(Long id);

    /**
     * Delete the "id" candidacy.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
