package be.jhipster.recruitment.service;

import be.jhipster.recruitment.service.dto.CandidateCommentDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link be.jhipster.recruitment.domain.CandidateComment}.
 */
public interface CandidateCommentService {

    /**
     * Save a candidateComment.
     *
     * @param candidateCommentDTO the entity to save.
     * @return the persisted entity.
     */
    CandidateCommentDTO save(CandidateCommentDTO candidateCommentDTO);

    /**
     * Get all the candidateComments.
     *
     * @return the list of entities.
     */
    List<CandidateCommentDTO> findAll();


    /**
     * Get the "id" candidateComment.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<CandidateCommentDTO> findOne(Long id);

    /**
     * Delete the "id" candidateComment.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
