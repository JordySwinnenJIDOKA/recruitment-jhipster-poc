package be.jhipster.recruitment.service;

import be.jhipster.recruitment.service.dto.CandidateSubClassDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link be.jhipster.recruitment.domain.CandidateSubClass}.
 */
public interface CandidateSubClassService {

    /**
     * Save a candidateSubClass.
     *
     * @param candidateSubClassDTO the entity to save.
     * @return the persisted entity.
     */
    CandidateSubClassDTO save(CandidateSubClassDTO candidateSubClassDTO);

    /**
     * Get all the candidateSubClasses.
     *
     * @return the list of entities.
     */
    List<CandidateSubClassDTO> findAll();


    /**
     * Get the "id" candidateSubClass.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<CandidateSubClassDTO> findOne(Long id);

    /**
     * Delete the "id" candidateSubClass.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
