package be.jhipster.recruitment.service;

import be.jhipster.recruitment.service.dto.ContactMomentDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link be.jhipster.recruitment.domain.ContactMoment}.
 */
public interface ContactMomentService {

    /**
     * Save a contactMoment.
     *
     * @param contactMomentDTO the entity to save.
     * @return the persisted entity.
     */
    ContactMomentDTO save(ContactMomentDTO contactMomentDTO);

    /**
     * Get all the contactMoments.
     *
     * @return the list of entities.
     */
    List<ContactMomentDTO> findAll();


    /**
     * Get the "id" contactMoment.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ContactMomentDTO> findOne(Long id);

    /**
     * Delete the "id" contactMoment.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
