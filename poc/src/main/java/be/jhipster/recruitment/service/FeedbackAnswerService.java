package be.jhipster.recruitment.service;

import be.jhipster.recruitment.service.dto.FeedbackAnswerDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link be.jhipster.recruitment.domain.FeedbackAnswer}.
 */
public interface FeedbackAnswerService {

    /**
     * Save a feedbackAnswer.
     *
     * @param feedbackAnswerDTO the entity to save.
     * @return the persisted entity.
     */
    FeedbackAnswerDTO save(FeedbackAnswerDTO feedbackAnswerDTO);

    /**
     * Get all the feedbackAnswers.
     *
     * @return the list of entities.
     */
    List<FeedbackAnswerDTO> findAll();


    /**
     * Get the "id" feedbackAnswer.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<FeedbackAnswerDTO> findOne(Long id);

    /**
     * Delete the "id" feedbackAnswer.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
