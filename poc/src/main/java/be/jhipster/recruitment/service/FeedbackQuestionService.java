package be.jhipster.recruitment.service;

import be.jhipster.recruitment.service.dto.FeedbackQuestionDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link be.jhipster.recruitment.domain.FeedbackQuestion}.
 */
public interface FeedbackQuestionService {

    /**
     * Save a feedbackQuestion.
     *
     * @param feedbackQuestionDTO the entity to save.
     * @return the persisted entity.
     */
    FeedbackQuestionDTO save(FeedbackQuestionDTO feedbackQuestionDTO);

    /**
     * Get all the feedbackQuestions.
     *
     * @return the list of entities.
     */
    List<FeedbackQuestionDTO> findAll();


    /**
     * Get the "id" feedbackQuestion.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<FeedbackQuestionDTO> findOne(Long id);

    /**
     * Delete the "id" feedbackQuestion.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
