package be.jhipster.recruitment.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;
import be.jhipster.recruitment.domain.enumeration.CandidacyStatus;

/**
 * A DTO for the {@link be.jhipster.recruitment.domain.Candidacy} entity.
 */
public class CandidacyDTO implements Serializable {

    private Long id;

    @NotNull
    private CandidacyStatus candidacyStatus;

    @NotNull
    private String motivation;


    private Long candidateId;

    private Long vacancyId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CandidacyStatus getCandidacyStatus() {
        return candidacyStatus;
    }

    public void setCandidacyStatus(CandidacyStatus candidacyStatus) {
        this.candidacyStatus = candidacyStatus;
    }

    public String getMotivation() {
        return motivation;
    }

    public void setMotivation(String motivation) {
        this.motivation = motivation;
    }

    public Long getCandidateId() {
        return candidateId;
    }

    public void setCandidateId(Long candidateId) {
        this.candidateId = candidateId;
    }

    public Long getVacancyId() {
        return vacancyId;
    }

    public void setVacancyId(Long vacancyId) {
        this.vacancyId = vacancyId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CandidacyDTO candidacyDTO = (CandidacyDTO) o;
        if (candidacyDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), candidacyDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CandidacyDTO{" +
            "id=" + getId() +
            ", candidacyStatus='" + getCandidacyStatus() + "'" +
            ", motivation='" + getMotivation() + "'" +
            ", candidate=" + getCandidateId() +
            ", vacancy=" + getVacancyId() +
            "}";
    }
}
