package be.jhipster.recruitment.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link be.jhipster.recruitment.domain.CandidateComment} entity.
 */
public class CandidateCommentDTO implements Serializable {

    private Long id;

    @NotNull
    private String context;

    @NotNull
    private String commenter;


    private Long candidateId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public String getCommenter() {
        return commenter;
    }

    public void setCommenter(String commenter) {
        this.commenter = commenter;
    }

    public Long getCandidateId() {
        return candidateId;
    }

    public void setCandidateId(Long candidateId) {
        this.candidateId = candidateId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CandidateCommentDTO candidateCommentDTO = (CandidateCommentDTO) o;
        if (candidateCommentDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), candidateCommentDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CandidateCommentDTO{" +
            "id=" + getId() +
            ", context='" + getContext() + "'" +
            ", commenter='" + getCommenter() + "'" +
            ", candidate=" + getCandidateId() +
            "}";
    }
}
