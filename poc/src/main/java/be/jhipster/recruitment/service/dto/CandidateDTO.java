package be.jhipster.recruitment.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Lob;
import be.jhipster.recruitment.domain.enumeration.CandidateStatus;
import be.jhipster.recruitment.domain.enumeration.CandidateFuntion;
import be.jhipster.recruitment.domain.enumeration.CandidateTrack;
import be.jhipster.recruitment.domain.enumeration.RecruitmentChannel;

/**
 * A DTO for the {@link be.jhipster.recruitment.domain.Candidate} entity.
 */
public class CandidateDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;

    @NotNull
    private String phone;

    @NotNull
    private String email;

    private CandidateStatus candidateStatus;

    @NotNull
    private CandidateFuntion candidateFunction;

    @NotNull
    private String dateAvailable;

    @Lob
    private byte[] imageContent;

    private String imageContentContentType;
    @NotNull
    private CandidateTrack candidateTrack;

    @NotNull
    private RecruitmentChannel recruitmentChannel;

    private String linkedInProfilePath;


    private Long documentId;

    private Long subClassId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public CandidateStatus getCandidateStatus() {
        return candidateStatus;
    }

    public void setCandidateStatus(CandidateStatus candidateStatus) {
        this.candidateStatus = candidateStatus;
    }

    public CandidateFuntion getCandidateFunction() {
        return candidateFunction;
    }

    public void setCandidateFunction(CandidateFuntion candidateFunction) {
        this.candidateFunction = candidateFunction;
    }

    public String getDateAvailable() {
        return dateAvailable;
    }

    public void setDateAvailable(String dateAvailable) {
        this.dateAvailable = dateAvailable;
    }

    public byte[] getImageContent() {
        return imageContent;
    }

    public void setImageContent(byte[] imageContent) {
        this.imageContent = imageContent;
    }

    public String getImageContentContentType() {
        return imageContentContentType;
    }

    public void setImageContentContentType(String imageContentContentType) {
        this.imageContentContentType = imageContentContentType;
    }

    public CandidateTrack getCandidateTrack() {
        return candidateTrack;
    }

    public void setCandidateTrack(CandidateTrack candidateTrack) {
        this.candidateTrack = candidateTrack;
    }

    public RecruitmentChannel getRecruitmentChannel() {
        return recruitmentChannel;
    }

    public void setRecruitmentChannel(RecruitmentChannel recruitmentChannel) {
        this.recruitmentChannel = recruitmentChannel;
    }

    public String getLinkedInProfilePath() {
        return linkedInProfilePath;
    }

    public void setLinkedInProfilePath(String linkedInProfilePath) {
        this.linkedInProfilePath = linkedInProfilePath;
    }

    public Long getDocumentId() {
        return documentId;
    }

    public void setDocumentId(Long documentId) {
        this.documentId = documentId;
    }

    public Long getSubClassId() {
        return subClassId;
    }

    public void setSubClassId(Long candidateSubClassId) {
        this.subClassId = candidateSubClassId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CandidateDTO candidateDTO = (CandidateDTO) o;
        if (candidateDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), candidateDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CandidateDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", phone='" + getPhone() + "'" +
            ", email='" + getEmail() + "'" +
            ", candidateStatus='" + getCandidateStatus() + "'" +
            ", candidateFunction='" + getCandidateFunction() + "'" +
            ", dateAvailable='" + getDateAvailable() + "'" +
            ", imageContent='" + getImageContent() + "'" +
            ", candidateTrack='" + getCandidateTrack() + "'" +
            ", recruitmentChannel='" + getRecruitmentChannel() + "'" +
            ", linkedInProfilePath='" + getLinkedInProfilePath() + "'" +
            ", document=" + getDocumentId() +
            ", subClass=" + getSubClassId() +
            "}";
    }
}
