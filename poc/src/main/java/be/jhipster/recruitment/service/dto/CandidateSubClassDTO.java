package be.jhipster.recruitment.service.dto;
import java.io.Serializable;
import java.util.Objects;
import be.jhipster.recruitment.domain.enumeration.CandidateType;

/**
 * A DTO for the {@link be.jhipster.recruitment.domain.CandidateSubClass} entity.
 */
public class CandidateSubClassDTO implements Serializable {

    private Long id;

    private CandidateType candidateType;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CandidateType getCandidateType() {
        return candidateType;
    }

    public void setCandidateType(CandidateType candidateType) {
        this.candidateType = candidateType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CandidateSubClassDTO candidateSubClassDTO = (CandidateSubClassDTO) o;
        if (candidateSubClassDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), candidateSubClassDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CandidateSubClassDTO{" +
            "id=" + getId() +
            ", candidateType='" + getCandidateType() + "'" +
            "}";
    }
}
