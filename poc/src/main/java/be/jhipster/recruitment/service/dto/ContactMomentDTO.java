package be.jhipster.recruitment.service.dto;
import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;
import be.jhipster.recruitment.domain.enumeration.ContactType;
import be.jhipster.recruitment.domain.enumeration.ContactContext;

/**
 * A DTO for the {@link be.jhipster.recruitment.domain.ContactMoment} entity.
 */
public class ContactMomentDTO implements Serializable {

    private Long id;

    @NotNull
    private ContactType contactType;

    @NotNull
    private LocalDate contactDate;

    @NotNull
    private ContactContext contactContext;

    @NotNull
    private String interviewer;

    private String location;


    private Long candidacyId;

    private Long candidateId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ContactType getContactType() {
        return contactType;
    }

    public void setContactType(ContactType contactType) {
        this.contactType = contactType;
    }

    public LocalDate getContactDate() {
        return contactDate;
    }

    public void setContactDate(LocalDate contactDate) {
        this.contactDate = contactDate;
    }

    public ContactContext getContactContext() {
        return contactContext;
    }

    public void setContactContext(ContactContext contactContext) {
        this.contactContext = contactContext;
    }

    public String getInterviewer() {
        return interviewer;
    }

    public void setInterviewer(String interviewer) {
        this.interviewer = interviewer;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Long getCandidacyId() {
        return candidacyId;
    }

    public void setCandidacyId(Long candidacyId) {
        this.candidacyId = candidacyId;
    }

    public Long getCandidateId() {
        return candidateId;
    }

    public void setCandidateId(Long candidateId) {
        this.candidateId = candidateId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ContactMomentDTO contactMomentDTO = (ContactMomentDTO) o;
        if (contactMomentDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), contactMomentDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ContactMomentDTO{" +
            "id=" + getId() +
            ", contactType='" + getContactType() + "'" +
            ", contactDate='" + getContactDate() + "'" +
            ", contactContext='" + getContactContext() + "'" +
            ", interviewer='" + getInterviewer() + "'" +
            ", location='" + getLocation() + "'" +
            ", candidacy=" + getCandidacyId() +
            ", candidate=" + getCandidateId() +
            "}";
    }
}
