package be.jhipster.recruitment.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link be.jhipster.recruitment.domain.FeedbackAnswer} entity.
 */
public class FeedbackAnswerDTO implements Serializable {

    private Long id;

    @NotNull
    private String answer;


    private Long feedbackQuestionId;

    private Long candidacyId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Long getFeedbackQuestionId() {
        return feedbackQuestionId;
    }

    public void setFeedbackQuestionId(Long feedbackQuestionId) {
        this.feedbackQuestionId = feedbackQuestionId;
    }

    public Long getCandidacyId() {
        return candidacyId;
    }

    public void setCandidacyId(Long candidacyId) {
        this.candidacyId = candidacyId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        FeedbackAnswerDTO feedbackAnswerDTO = (FeedbackAnswerDTO) o;
        if (feedbackAnswerDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), feedbackAnswerDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "FeedbackAnswerDTO{" +
            "id=" + getId() +
            ", answer='" + getAnswer() + "'" +
            ", feedbackQuestion=" + getFeedbackQuestionId() +
            ", candidacy=" + getCandidacyId() +
            "}";
    }
}
