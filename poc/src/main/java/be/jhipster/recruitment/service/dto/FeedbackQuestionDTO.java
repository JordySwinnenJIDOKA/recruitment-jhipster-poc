package be.jhipster.recruitment.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link be.jhipster.recruitment.domain.FeedbackQuestion} entity.
 */
public class FeedbackQuestionDTO implements Serializable {

    private Long id;

    @NotNull
    private String question;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        FeedbackQuestionDTO feedbackQuestionDTO = (FeedbackQuestionDTO) o;
        if (feedbackQuestionDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), feedbackQuestionDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "FeedbackQuestionDTO{" +
            "id=" + getId() +
            ", question='" + getQuestion() + "'" +
            "}";
    }
}
