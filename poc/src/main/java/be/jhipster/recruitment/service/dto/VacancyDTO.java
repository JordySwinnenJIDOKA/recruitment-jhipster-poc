package be.jhipster.recruitment.service.dto;
import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;
import be.jhipster.recruitment.domain.enumeration.VacancyStatus;
import be.jhipster.recruitment.domain.enumeration.CandidateType;

/**
 * A DTO for the {@link be.jhipster.recruitment.domain.Vacancy} entity.
 */
public class VacancyDTO implements Serializable {

    private Long id;

    @NotNull
    private String code;

    @NotNull
    private String description;

    @NotNull
    private LocalDate creationDate;

    private VacancyStatus vacancyStatus;

    private String characteristics;

    private String skills;

    private String dailyRoutine;

    private String location;

    private CandidateType candidateTypes;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public VacancyStatus getVacancyStatus() {
        return vacancyStatus;
    }

    public void setVacancyStatus(VacancyStatus vacancyStatus) {
        this.vacancyStatus = vacancyStatus;
    }

    public String getCharacteristics() {
        return characteristics;
    }

    public void setCharacteristics(String characteristics) {
        this.characteristics = characteristics;
    }

    public String getSkills() {
        return skills;
    }

    public void setSkills(String skills) {
        this.skills = skills;
    }

    public String getDailyRoutine() {
        return dailyRoutine;
    }

    public void setDailyRoutine(String dailyRoutine) {
        this.dailyRoutine = dailyRoutine;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public CandidateType getCandidateTypes() {
        return candidateTypes;
    }

    public void setCandidateTypes(CandidateType candidateTypes) {
        this.candidateTypes = candidateTypes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        VacancyDTO vacancyDTO = (VacancyDTO) o;
        if (vacancyDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), vacancyDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "VacancyDTO{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", description='" + getDescription() + "'" +
            ", creationDate='" + getCreationDate() + "'" +
            ", vacancyStatus='" + getVacancyStatus() + "'" +
            ", characteristics='" + getCharacteristics() + "'" +
            ", skills='" + getSkills() + "'" +
            ", dailyRoutine='" + getDailyRoutine() + "'" +
            ", location='" + getLocation() + "'" +
            ", candidateTypes='" + getCandidateTypes() + "'" +
            "}";
    }
}
