package be.jhipster.recruitment.service.impl;

import be.jhipster.recruitment.service.CandidacyService;
import be.jhipster.recruitment.domain.Candidacy;
import be.jhipster.recruitment.repository.CandidacyRepository;
import be.jhipster.recruitment.service.dto.CandidacyDTO;
import be.jhipster.recruitment.service.mapper.CandidacyMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Candidacy}.
 */
@Service
@Transactional
public class CandidacyServiceImpl implements CandidacyService {

    private final Logger log = LoggerFactory.getLogger(CandidacyServiceImpl.class);

    private final CandidacyRepository candidacyRepository;

    private final CandidacyMapper candidacyMapper;

    public CandidacyServiceImpl(CandidacyRepository candidacyRepository, CandidacyMapper candidacyMapper) {
        this.candidacyRepository = candidacyRepository;
        this.candidacyMapper = candidacyMapper;
    }

    /**
     * Save a candidacy.
     *
     * @param candidacyDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public CandidacyDTO save(CandidacyDTO candidacyDTO) {
        log.debug("Request to save Candidacy : {}", candidacyDTO);
        Candidacy candidacy = candidacyMapper.toEntity(candidacyDTO);
        candidacy = candidacyRepository.save(candidacy);
        return candidacyMapper.toDto(candidacy);
    }

    /**
     * Get all the candidacies.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<CandidacyDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Candidacies");
        return candidacyRepository.findAll(pageable)
            .map(candidacyMapper::toDto);
    }


    /**
     * Get one candidacy by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<CandidacyDTO> findOne(Long id) {
        log.debug("Request to get Candidacy : {}", id);
        return candidacyRepository.findById(id)
            .map(candidacyMapper::toDto);
    }

    /**
     * Delete the candidacy by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Candidacy : {}", id);
        candidacyRepository.deleteById(id);
    }
}
