package be.jhipster.recruitment.service.impl;

import be.jhipster.recruitment.service.CandidateCommentService;
import be.jhipster.recruitment.domain.CandidateComment;
import be.jhipster.recruitment.repository.CandidateCommentRepository;
import be.jhipster.recruitment.service.dto.CandidateCommentDTO;
import be.jhipster.recruitment.service.mapper.CandidateCommentMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link CandidateComment}.
 */
@Service
@Transactional
public class CandidateCommentServiceImpl implements CandidateCommentService {

    private final Logger log = LoggerFactory.getLogger(CandidateCommentServiceImpl.class);

    private final CandidateCommentRepository candidateCommentRepository;

    private final CandidateCommentMapper candidateCommentMapper;

    public CandidateCommentServiceImpl(CandidateCommentRepository candidateCommentRepository, CandidateCommentMapper candidateCommentMapper) {
        this.candidateCommentRepository = candidateCommentRepository;
        this.candidateCommentMapper = candidateCommentMapper;
    }

    /**
     * Save a candidateComment.
     *
     * @param candidateCommentDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public CandidateCommentDTO save(CandidateCommentDTO candidateCommentDTO) {
        log.debug("Request to save CandidateComment : {}", candidateCommentDTO);
        CandidateComment candidateComment = candidateCommentMapper.toEntity(candidateCommentDTO);
        candidateComment = candidateCommentRepository.save(candidateComment);
        return candidateCommentMapper.toDto(candidateComment);
    }

    /**
     * Get all the candidateComments.
     *
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<CandidateCommentDTO> findAll() {
        log.debug("Request to get all CandidateComments");
        return candidateCommentRepository.findAll().stream()
            .map(candidateCommentMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one candidateComment by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<CandidateCommentDTO> findOne(Long id) {
        log.debug("Request to get CandidateComment : {}", id);
        return candidateCommentRepository.findById(id)
            .map(candidateCommentMapper::toDto);
    }

    /**
     * Delete the candidateComment by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete CandidateComment : {}", id);
        candidateCommentRepository.deleteById(id);
    }
}
