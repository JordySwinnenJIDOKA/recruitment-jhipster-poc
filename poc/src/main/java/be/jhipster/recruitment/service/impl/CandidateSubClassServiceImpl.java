package be.jhipster.recruitment.service.impl;

import be.jhipster.recruitment.service.CandidateSubClassService;
import be.jhipster.recruitment.domain.CandidateSubClass;
import be.jhipster.recruitment.repository.CandidateSubClassRepository;
import be.jhipster.recruitment.service.dto.CandidateSubClassDTO;
import be.jhipster.recruitment.service.mapper.CandidateSubClassMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link CandidateSubClass}.
 */
@Service
@Transactional
public class CandidateSubClassServiceImpl implements CandidateSubClassService {

    private final Logger log = LoggerFactory.getLogger(CandidateSubClassServiceImpl.class);

    private final CandidateSubClassRepository candidateSubClassRepository;

    private final CandidateSubClassMapper candidateSubClassMapper;

    public CandidateSubClassServiceImpl(CandidateSubClassRepository candidateSubClassRepository, CandidateSubClassMapper candidateSubClassMapper) {
        this.candidateSubClassRepository = candidateSubClassRepository;
        this.candidateSubClassMapper = candidateSubClassMapper;
    }

    /**
     * Save a candidateSubClass.
     *
     * @param candidateSubClassDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public CandidateSubClassDTO save(CandidateSubClassDTO candidateSubClassDTO) {
        log.debug("Request to save CandidateSubClass : {}", candidateSubClassDTO);
        CandidateSubClass candidateSubClass = candidateSubClassMapper.toEntity(candidateSubClassDTO);
        candidateSubClass = candidateSubClassRepository.save(candidateSubClass);
        return candidateSubClassMapper.toDto(candidateSubClass);
    }

    /**
     * Get all the candidateSubClasses.
     *
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<CandidateSubClassDTO> findAll() {
        log.debug("Request to get all CandidateSubClasses");
        return candidateSubClassRepository.findAll().stream()
            .map(candidateSubClassMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one candidateSubClass by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<CandidateSubClassDTO> findOne(Long id) {
        log.debug("Request to get CandidateSubClass : {}", id);
        return candidateSubClassRepository.findById(id)
            .map(candidateSubClassMapper::toDto);
    }

    /**
     * Delete the candidateSubClass by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete CandidateSubClass : {}", id);
        candidateSubClassRepository.deleteById(id);
    }
}
