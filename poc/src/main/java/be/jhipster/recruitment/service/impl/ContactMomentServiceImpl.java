package be.jhipster.recruitment.service.impl;

import be.jhipster.recruitment.service.ContactMomentService;
import be.jhipster.recruitment.domain.ContactMoment;
import be.jhipster.recruitment.repository.ContactMomentRepository;
import be.jhipster.recruitment.service.dto.ContactMomentDTO;
import be.jhipster.recruitment.service.mapper.ContactMomentMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link ContactMoment}.
 */
@Service
@Transactional
public class ContactMomentServiceImpl implements ContactMomentService {

    private final Logger log = LoggerFactory.getLogger(ContactMomentServiceImpl.class);

    private final ContactMomentRepository contactMomentRepository;

    private final ContactMomentMapper contactMomentMapper;

    public ContactMomentServiceImpl(ContactMomentRepository contactMomentRepository, ContactMomentMapper contactMomentMapper) {
        this.contactMomentRepository = contactMomentRepository;
        this.contactMomentMapper = contactMomentMapper;
    }

    /**
     * Save a contactMoment.
     *
     * @param contactMomentDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ContactMomentDTO save(ContactMomentDTO contactMomentDTO) {
        log.debug("Request to save ContactMoment : {}", contactMomentDTO);
        ContactMoment contactMoment = contactMomentMapper.toEntity(contactMomentDTO);
        contactMoment = contactMomentRepository.save(contactMoment);
        return contactMomentMapper.toDto(contactMoment);
    }

    /**
     * Get all the contactMoments.
     *
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<ContactMomentDTO> findAll() {
        log.debug("Request to get all ContactMoments");
        return contactMomentRepository.findAll().stream()
            .map(contactMomentMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one contactMoment by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ContactMomentDTO> findOne(Long id) {
        log.debug("Request to get ContactMoment : {}", id);
        return contactMomentRepository.findById(id)
            .map(contactMomentMapper::toDto);
    }

    /**
     * Delete the contactMoment by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ContactMoment : {}", id);
        contactMomentRepository.deleteById(id);
    }
}
