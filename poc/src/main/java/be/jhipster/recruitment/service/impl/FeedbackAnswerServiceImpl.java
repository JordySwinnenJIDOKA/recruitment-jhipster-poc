package be.jhipster.recruitment.service.impl;

import be.jhipster.recruitment.service.FeedbackAnswerService;
import be.jhipster.recruitment.domain.FeedbackAnswer;
import be.jhipster.recruitment.repository.FeedbackAnswerRepository;
import be.jhipster.recruitment.service.dto.FeedbackAnswerDTO;
import be.jhipster.recruitment.service.mapper.FeedbackAnswerMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link FeedbackAnswer}.
 */
@Service
@Transactional
public class FeedbackAnswerServiceImpl implements FeedbackAnswerService {

    private final Logger log = LoggerFactory.getLogger(FeedbackAnswerServiceImpl.class);

    private final FeedbackAnswerRepository feedbackAnswerRepository;

    private final FeedbackAnswerMapper feedbackAnswerMapper;

    public FeedbackAnswerServiceImpl(FeedbackAnswerRepository feedbackAnswerRepository, FeedbackAnswerMapper feedbackAnswerMapper) {
        this.feedbackAnswerRepository = feedbackAnswerRepository;
        this.feedbackAnswerMapper = feedbackAnswerMapper;
    }

    /**
     * Save a feedbackAnswer.
     *
     * @param feedbackAnswerDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public FeedbackAnswerDTO save(FeedbackAnswerDTO feedbackAnswerDTO) {
        log.debug("Request to save FeedbackAnswer : {}", feedbackAnswerDTO);
        FeedbackAnswer feedbackAnswer = feedbackAnswerMapper.toEntity(feedbackAnswerDTO);
        feedbackAnswer = feedbackAnswerRepository.save(feedbackAnswer);
        return feedbackAnswerMapper.toDto(feedbackAnswer);
    }

    /**
     * Get all the feedbackAnswers.
     *
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<FeedbackAnswerDTO> findAll() {
        log.debug("Request to get all FeedbackAnswers");
        return feedbackAnswerRepository.findAll().stream()
            .map(feedbackAnswerMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one feedbackAnswer by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<FeedbackAnswerDTO> findOne(Long id) {
        log.debug("Request to get FeedbackAnswer : {}", id);
        return feedbackAnswerRepository.findById(id)
            .map(feedbackAnswerMapper::toDto);
    }

    /**
     * Delete the feedbackAnswer by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete FeedbackAnswer : {}", id);
        feedbackAnswerRepository.deleteById(id);
    }
}
