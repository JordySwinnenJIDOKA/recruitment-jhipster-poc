package be.jhipster.recruitment.service.impl;

import be.jhipster.recruitment.service.FeedbackQuestionService;
import be.jhipster.recruitment.domain.FeedbackQuestion;
import be.jhipster.recruitment.repository.FeedbackQuestionRepository;
import be.jhipster.recruitment.service.dto.FeedbackQuestionDTO;
import be.jhipster.recruitment.service.mapper.FeedbackQuestionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link FeedbackQuestion}.
 */
@Service
@Transactional
public class FeedbackQuestionServiceImpl implements FeedbackQuestionService {

    private final Logger log = LoggerFactory.getLogger(FeedbackQuestionServiceImpl.class);

    private final FeedbackQuestionRepository feedbackQuestionRepository;

    private final FeedbackQuestionMapper feedbackQuestionMapper;

    public FeedbackQuestionServiceImpl(FeedbackQuestionRepository feedbackQuestionRepository, FeedbackQuestionMapper feedbackQuestionMapper) {
        this.feedbackQuestionRepository = feedbackQuestionRepository;
        this.feedbackQuestionMapper = feedbackQuestionMapper;
    }

    /**
     * Save a feedbackQuestion.
     *
     * @param feedbackQuestionDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public FeedbackQuestionDTO save(FeedbackQuestionDTO feedbackQuestionDTO) {
        log.debug("Request to save FeedbackQuestion : {}", feedbackQuestionDTO);
        FeedbackQuestion feedbackQuestion = feedbackQuestionMapper.toEntity(feedbackQuestionDTO);
        feedbackQuestion = feedbackQuestionRepository.save(feedbackQuestion);
        return feedbackQuestionMapper.toDto(feedbackQuestion);
    }

    /**
     * Get all the feedbackQuestions.
     *
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<FeedbackQuestionDTO> findAll() {
        log.debug("Request to get all FeedbackQuestions");
        return feedbackQuestionRepository.findAll().stream()
            .map(feedbackQuestionMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one feedbackQuestion by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<FeedbackQuestionDTO> findOne(Long id) {
        log.debug("Request to get FeedbackQuestion : {}", id);
        return feedbackQuestionRepository.findById(id)
            .map(feedbackQuestionMapper::toDto);
    }

    /**
     * Delete the feedbackQuestion by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete FeedbackQuestion : {}", id);
        feedbackQuestionRepository.deleteById(id);
    }
}
