package be.jhipster.recruitment.service.mapper;

import be.jhipster.recruitment.domain.*;
import be.jhipster.recruitment.service.dto.CandidacyDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Candidacy} and its DTO {@link CandidacyDTO}.
 */
@Mapper(componentModel = "spring", uses = {CandidateMapper.class, VacancyMapper.class})
public interface CandidacyMapper extends EntityMapper<CandidacyDTO, Candidacy> {

    @Mapping(source = "candidate.id", target = "candidateId")
    @Mapping(source = "vacancy.id", target = "vacancyId")
    CandidacyDTO toDto(Candidacy candidacy);

    @Mapping(source = "candidateId", target = "candidate")
    @Mapping(source = "vacancyId", target = "vacancy")
    Candidacy toEntity(CandidacyDTO candidacyDTO);

    default Candidacy fromId(Long id) {
        if (id == null) {
            return null;
        }
        Candidacy candidacy = new Candidacy();
        candidacy.setId(id);
        return candidacy;
    }
}
