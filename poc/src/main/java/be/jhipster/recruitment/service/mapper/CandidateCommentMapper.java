package be.jhipster.recruitment.service.mapper;

import be.jhipster.recruitment.domain.*;
import be.jhipster.recruitment.service.dto.CandidateCommentDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link CandidateComment} and its DTO {@link CandidateCommentDTO}.
 */
@Mapper(componentModel = "spring", uses = {CandidateMapper.class})
public interface CandidateCommentMapper extends EntityMapper<CandidateCommentDTO, CandidateComment> {

    @Mapping(source = "candidate.id", target = "candidateId")
    CandidateCommentDTO toDto(CandidateComment candidateComment);

    @Mapping(source = "candidateId", target = "candidate")
    CandidateComment toEntity(CandidateCommentDTO candidateCommentDTO);

    default CandidateComment fromId(Long id) {
        if (id == null) {
            return null;
        }
        CandidateComment candidateComment = new CandidateComment();
        candidateComment.setId(id);
        return candidateComment;
    }
}
