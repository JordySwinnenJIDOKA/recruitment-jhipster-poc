package be.jhipster.recruitment.service.mapper;

import be.jhipster.recruitment.domain.*;
import be.jhipster.recruitment.service.dto.CandidateDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Candidate} and its DTO {@link CandidateDTO}.
 */
@Mapper(componentModel = "spring", uses = {DocumentMapper.class, CandidateSubClassMapper.class})
public interface CandidateMapper extends EntityMapper<CandidateDTO, Candidate> {

    @Mapping(source = "document.id", target = "documentId")
    @Mapping(source = "subClass.id", target = "subClassId")
    CandidateDTO toDto(Candidate candidate);

    @Mapping(source = "documentId", target = "document")
    @Mapping(source = "subClassId", target = "subClass")
    @Mapping(target = "candidacies", ignore = true)
    @Mapping(target = "candidateComments", ignore = true)
    @Mapping(target = "contactMoments", ignore = true)
    Candidate toEntity(CandidateDTO candidateDTO);

    default Candidate fromId(Long id) {
        if (id == null) {
            return null;
        }
        Candidate candidate = new Candidate();
        candidate.setId(id);
        return candidate;
    }
}
