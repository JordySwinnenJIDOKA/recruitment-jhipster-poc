package be.jhipster.recruitment.service.mapper;

import be.jhipster.recruitment.domain.*;
import be.jhipster.recruitment.service.dto.CandidateSubClassDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link CandidateSubClass} and its DTO {@link CandidateSubClassDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CandidateSubClassMapper extends EntityMapper<CandidateSubClassDTO, CandidateSubClass> {



    default CandidateSubClass fromId(Long id) {
        if (id == null) {
            return null;
        }
        CandidateSubClass candidateSubClass = new CandidateSubClass();
        candidateSubClass.setId(id);
        return candidateSubClass;
    }
}
