package be.jhipster.recruitment.service.mapper;

import be.jhipster.recruitment.domain.*;
import be.jhipster.recruitment.service.dto.ContactMomentDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ContactMoment} and its DTO {@link ContactMomentDTO}.
 */
@Mapper(componentModel = "spring", uses = {CandidacyMapper.class, CandidateMapper.class})
public interface ContactMomentMapper extends EntityMapper<ContactMomentDTO, ContactMoment> {

    @Mapping(source = "candidacy.id", target = "candidacyId")
    @Mapping(source = "candidate.id", target = "candidateId")
    ContactMomentDTO toDto(ContactMoment contactMoment);

    @Mapping(source = "candidacyId", target = "candidacy")
    @Mapping(source = "candidateId", target = "candidate")
    ContactMoment toEntity(ContactMomentDTO contactMomentDTO);

    default ContactMoment fromId(Long id) {
        if (id == null) {
            return null;
        }
        ContactMoment contactMoment = new ContactMoment();
        contactMoment.setId(id);
        return contactMoment;
    }
}
