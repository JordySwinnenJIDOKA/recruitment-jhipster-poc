package be.jhipster.recruitment.service.mapper;

import be.jhipster.recruitment.domain.*;
import be.jhipster.recruitment.service.dto.FeedbackAnswerDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link FeedbackAnswer} and its DTO {@link FeedbackAnswerDTO}.
 */
@Mapper(componentModel = "spring", uses = {FeedbackQuestionMapper.class, CandidacyMapper.class})
public interface FeedbackAnswerMapper extends EntityMapper<FeedbackAnswerDTO, FeedbackAnswer> {

    @Mapping(source = "feedbackQuestion.id", target = "feedbackQuestionId")
    @Mapping(source = "candidacy.id", target = "candidacyId")
    FeedbackAnswerDTO toDto(FeedbackAnswer feedbackAnswer);

    @Mapping(source = "feedbackQuestionId", target = "feedbackQuestion")
    @Mapping(source = "candidacyId", target = "candidacy")
    FeedbackAnswer toEntity(FeedbackAnswerDTO feedbackAnswerDTO);

    default FeedbackAnswer fromId(Long id) {
        if (id == null) {
            return null;
        }
        FeedbackAnswer feedbackAnswer = new FeedbackAnswer();
        feedbackAnswer.setId(id);
        return feedbackAnswer;
    }
}
