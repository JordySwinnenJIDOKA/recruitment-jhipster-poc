package be.jhipster.recruitment.service.mapper;

import be.jhipster.recruitment.domain.*;
import be.jhipster.recruitment.service.dto.FeedbackQuestionDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link FeedbackQuestion} and its DTO {@link FeedbackQuestionDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface FeedbackQuestionMapper extends EntityMapper<FeedbackQuestionDTO, FeedbackQuestion> {



    default FeedbackQuestion fromId(Long id) {
        if (id == null) {
            return null;
        }
        FeedbackQuestion feedbackQuestion = new FeedbackQuestion();
        feedbackQuestion.setId(id);
        return feedbackQuestion;
    }
}
