package be.jhipster.recruitment.service.mapper;

import be.jhipster.recruitment.domain.*;
import be.jhipster.recruitment.service.dto.VacancyDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Vacancy} and its DTO {@link VacancyDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface VacancyMapper extends EntityMapper<VacancyDTO, Vacancy> {


    @Mapping(target = "candidacies", ignore = true)
    Vacancy toEntity(VacancyDTO vacancyDTO);

    default Vacancy fromId(Long id) {
        if (id == null) {
            return null;
        }
        Vacancy vacancy = new Vacancy();
        vacancy.setId(id);
        return vacancy;
    }
}
