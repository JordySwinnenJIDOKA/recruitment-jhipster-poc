package be.jhipster.recruitment.web.rest;

import be.jhipster.recruitment.service.CandidacyService;
import be.jhipster.recruitment.web.rest.errors.BadRequestAlertException;
import be.jhipster.recruitment.service.dto.CandidacyDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link be.jhipster.recruitment.domain.Candidacy}.
 */
@RestController
@RequestMapping("/api")
public class CandidacyResource {

    private final Logger log = LoggerFactory.getLogger(CandidacyResource.class);

    private static final String ENTITY_NAME = "candidacy";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CandidacyService candidacyService;

    public CandidacyResource(CandidacyService candidacyService) {
        this.candidacyService = candidacyService;
    }

    /**
     * {@code POST  /candidacies} : Create a new candidacy.
     *
     * @param candidacyDTO the candidacyDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new candidacyDTO, or with status {@code 400 (Bad Request)} if the candidacy has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/candidacies")
    public ResponseEntity<CandidacyDTO> createCandidacy(@Valid @RequestBody CandidacyDTO candidacyDTO) throws URISyntaxException {
        log.debug("REST request to save Candidacy : {}", candidacyDTO);
        if (candidacyDTO.getId() != null) {
            throw new BadRequestAlertException("A new candidacy cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CandidacyDTO result = candidacyService.save(candidacyDTO);
        return ResponseEntity.created(new URI("/api/candidacies/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /candidacies} : Updates an existing candidacy.
     *
     * @param candidacyDTO the candidacyDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated candidacyDTO,
     * or with status {@code 400 (Bad Request)} if the candidacyDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the candidacyDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/candidacies")
    public ResponseEntity<CandidacyDTO> updateCandidacy(@Valid @RequestBody CandidacyDTO candidacyDTO) throws URISyntaxException {
        log.debug("REST request to update Candidacy : {}", candidacyDTO);
        if (candidacyDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CandidacyDTO result = candidacyService.save(candidacyDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, candidacyDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /candidacies} : get all the candidacies.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of candidacies in body.
     */
    @GetMapping("/candidacies")
    public ResponseEntity<List<CandidacyDTO>> getAllCandidacies(Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get a page of Candidacies");
        Page<CandidacyDTO> page = candidacyService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /candidacies/:id} : get the "id" candidacy.
     *
     * @param id the id of the candidacyDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the candidacyDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/candidacies/{id}")
    public ResponseEntity<CandidacyDTO> getCandidacy(@PathVariable Long id) {
        log.debug("REST request to get Candidacy : {}", id);
        Optional<CandidacyDTO> candidacyDTO = candidacyService.findOne(id);
        return ResponseUtil.wrapOrNotFound(candidacyDTO);
    }

    /**
     * {@code DELETE  /candidacies/:id} : delete the "id" candidacy.
     *
     * @param id the id of the candidacyDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/candidacies/{id}")
    public ResponseEntity<Void> deleteCandidacy(@PathVariable Long id) {
        log.debug("REST request to delete Candidacy : {}", id);
        candidacyService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
