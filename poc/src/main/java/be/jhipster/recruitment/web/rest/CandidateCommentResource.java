package be.jhipster.recruitment.web.rest;

import be.jhipster.recruitment.service.CandidateCommentService;
import be.jhipster.recruitment.web.rest.errors.BadRequestAlertException;
import be.jhipster.recruitment.service.dto.CandidateCommentDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link be.jhipster.recruitment.domain.CandidateComment}.
 */
@RestController
@RequestMapping("/api")
public class CandidateCommentResource {

    private final Logger log = LoggerFactory.getLogger(CandidateCommentResource.class);

    private static final String ENTITY_NAME = "candidateComment";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CandidateCommentService candidateCommentService;

    public CandidateCommentResource(CandidateCommentService candidateCommentService) {
        this.candidateCommentService = candidateCommentService;
    }

    /**
     * {@code POST  /candidate-comments} : Create a new candidateComment.
     *
     * @param candidateCommentDTO the candidateCommentDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new candidateCommentDTO, or with status {@code 400 (Bad Request)} if the candidateComment has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/candidate-comments")
    public ResponseEntity<CandidateCommentDTO> createCandidateComment(@Valid @RequestBody CandidateCommentDTO candidateCommentDTO) throws URISyntaxException {
        log.debug("REST request to save CandidateComment : {}", candidateCommentDTO);
        if (candidateCommentDTO.getId() != null) {
            throw new BadRequestAlertException("A new candidateComment cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CandidateCommentDTO result = candidateCommentService.save(candidateCommentDTO);
        return ResponseEntity.created(new URI("/api/candidate-comments/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /candidate-comments} : Updates an existing candidateComment.
     *
     * @param candidateCommentDTO the candidateCommentDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated candidateCommentDTO,
     * or with status {@code 400 (Bad Request)} if the candidateCommentDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the candidateCommentDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/candidate-comments")
    public ResponseEntity<CandidateCommentDTO> updateCandidateComment(@Valid @RequestBody CandidateCommentDTO candidateCommentDTO) throws URISyntaxException {
        log.debug("REST request to update CandidateComment : {}", candidateCommentDTO);
        if (candidateCommentDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CandidateCommentDTO result = candidateCommentService.save(candidateCommentDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, candidateCommentDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /candidate-comments} : get all the candidateComments.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of candidateComments in body.
     */
    @GetMapping("/candidate-comments")
    public List<CandidateCommentDTO> getAllCandidateComments() {
        log.debug("REST request to get all CandidateComments");
        return candidateCommentService.findAll();
    }

    /**
     * {@code GET  /candidate-comments/:id} : get the "id" candidateComment.
     *
     * @param id the id of the candidateCommentDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the candidateCommentDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/candidate-comments/{id}")
    public ResponseEntity<CandidateCommentDTO> getCandidateComment(@PathVariable Long id) {
        log.debug("REST request to get CandidateComment : {}", id);
        Optional<CandidateCommentDTO> candidateCommentDTO = candidateCommentService.findOne(id);
        return ResponseUtil.wrapOrNotFound(candidateCommentDTO);
    }

    /**
     * {@code DELETE  /candidate-comments/:id} : delete the "id" candidateComment.
     *
     * @param id the id of the candidateCommentDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/candidate-comments/{id}")
    public ResponseEntity<Void> deleteCandidateComment(@PathVariable Long id) {
        log.debug("REST request to delete CandidateComment : {}", id);
        candidateCommentService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
