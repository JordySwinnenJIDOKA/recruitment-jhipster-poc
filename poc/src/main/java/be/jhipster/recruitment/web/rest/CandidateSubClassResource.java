package be.jhipster.recruitment.web.rest;

import be.jhipster.recruitment.service.CandidateSubClassService;
import be.jhipster.recruitment.web.rest.errors.BadRequestAlertException;
import be.jhipster.recruitment.service.dto.CandidateSubClassDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link be.jhipster.recruitment.domain.CandidateSubClass}.
 */
@RestController
@RequestMapping("/api")
public class CandidateSubClassResource {

    private final Logger log = LoggerFactory.getLogger(CandidateSubClassResource.class);

    private static final String ENTITY_NAME = "candidateSubClass";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CandidateSubClassService candidateSubClassService;

    public CandidateSubClassResource(CandidateSubClassService candidateSubClassService) {
        this.candidateSubClassService = candidateSubClassService;
    }

    /**
     * {@code POST  /candidate-sub-classes} : Create a new candidateSubClass.
     *
     * @param candidateSubClassDTO the candidateSubClassDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new candidateSubClassDTO, or with status {@code 400 (Bad Request)} if the candidateSubClass has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/candidate-sub-classes")
    public ResponseEntity<CandidateSubClassDTO> createCandidateSubClass(@RequestBody CandidateSubClassDTO candidateSubClassDTO) throws URISyntaxException {
        log.debug("REST request to save CandidateSubClass : {}", candidateSubClassDTO);
        if (candidateSubClassDTO.getId() != null) {
            throw new BadRequestAlertException("A new candidateSubClass cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CandidateSubClassDTO result = candidateSubClassService.save(candidateSubClassDTO);
        return ResponseEntity.created(new URI("/api/candidate-sub-classes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /candidate-sub-classes} : Updates an existing candidateSubClass.
     *
     * @param candidateSubClassDTO the candidateSubClassDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated candidateSubClassDTO,
     * or with status {@code 400 (Bad Request)} if the candidateSubClassDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the candidateSubClassDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/candidate-sub-classes")
    public ResponseEntity<CandidateSubClassDTO> updateCandidateSubClass(@RequestBody CandidateSubClassDTO candidateSubClassDTO) throws URISyntaxException {
        log.debug("REST request to update CandidateSubClass : {}", candidateSubClassDTO);
        if (candidateSubClassDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CandidateSubClassDTO result = candidateSubClassService.save(candidateSubClassDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, candidateSubClassDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /candidate-sub-classes} : get all the candidateSubClasses.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of candidateSubClasses in body.
     */
    @GetMapping("/candidate-sub-classes")
    public List<CandidateSubClassDTO> getAllCandidateSubClasses() {
        log.debug("REST request to get all CandidateSubClasses");
        return candidateSubClassService.findAll();
    }

    /**
     * {@code GET  /candidate-sub-classes/:id} : get the "id" candidateSubClass.
     *
     * @param id the id of the candidateSubClassDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the candidateSubClassDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/candidate-sub-classes/{id}")
    public ResponseEntity<CandidateSubClassDTO> getCandidateSubClass(@PathVariable Long id) {
        log.debug("REST request to get CandidateSubClass : {}", id);
        Optional<CandidateSubClassDTO> candidateSubClassDTO = candidateSubClassService.findOne(id);
        return ResponseUtil.wrapOrNotFound(candidateSubClassDTO);
    }

    /**
     * {@code DELETE  /candidate-sub-classes/:id} : delete the "id" candidateSubClass.
     *
     * @param id the id of the candidateSubClassDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/candidate-sub-classes/{id}")
    public ResponseEntity<Void> deleteCandidateSubClass(@PathVariable Long id) {
        log.debug("REST request to delete CandidateSubClass : {}", id);
        candidateSubClassService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
