package be.jhipster.recruitment.web.rest;

import be.jhipster.recruitment.service.ContactMomentService;
import be.jhipster.recruitment.web.rest.errors.BadRequestAlertException;
import be.jhipster.recruitment.service.dto.ContactMomentDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link be.jhipster.recruitment.domain.ContactMoment}.
 */
@RestController
@RequestMapping("/api")
public class ContactMomentResource {

    private final Logger log = LoggerFactory.getLogger(ContactMomentResource.class);

    private static final String ENTITY_NAME = "contactMoment";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ContactMomentService contactMomentService;

    public ContactMomentResource(ContactMomentService contactMomentService) {
        this.contactMomentService = contactMomentService;
    }

    /**
     * {@code POST  /contact-moments} : Create a new contactMoment.
     *
     * @param contactMomentDTO the contactMomentDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new contactMomentDTO, or with status {@code 400 (Bad Request)} if the contactMoment has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/contact-moments")
    public ResponseEntity<ContactMomentDTO> createContactMoment(@Valid @RequestBody ContactMomentDTO contactMomentDTO) throws URISyntaxException {
        log.debug("REST request to save ContactMoment : {}", contactMomentDTO);
        if (contactMomentDTO.getId() != null) {
            throw new BadRequestAlertException("A new contactMoment cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ContactMomentDTO result = contactMomentService.save(contactMomentDTO);
        return ResponseEntity.created(new URI("/api/contact-moments/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /contact-moments} : Updates an existing contactMoment.
     *
     * @param contactMomentDTO the contactMomentDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated contactMomentDTO,
     * or with status {@code 400 (Bad Request)} if the contactMomentDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the contactMomentDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/contact-moments")
    public ResponseEntity<ContactMomentDTO> updateContactMoment(@Valid @RequestBody ContactMomentDTO contactMomentDTO) throws URISyntaxException {
        log.debug("REST request to update ContactMoment : {}", contactMomentDTO);
        if (contactMomentDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ContactMomentDTO result = contactMomentService.save(contactMomentDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, contactMomentDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /contact-moments} : get all the contactMoments.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of contactMoments in body.
     */
    @GetMapping("/contact-moments")
    public List<ContactMomentDTO> getAllContactMoments() {
        log.debug("REST request to get all ContactMoments");
        return contactMomentService.findAll();
    }

    /**
     * {@code GET  /contact-moments/:id} : get the "id" contactMoment.
     *
     * @param id the id of the contactMomentDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the contactMomentDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/contact-moments/{id}")
    public ResponseEntity<ContactMomentDTO> getContactMoment(@PathVariable Long id) {
        log.debug("REST request to get ContactMoment : {}", id);
        Optional<ContactMomentDTO> contactMomentDTO = contactMomentService.findOne(id);
        return ResponseUtil.wrapOrNotFound(contactMomentDTO);
    }

    /**
     * {@code DELETE  /contact-moments/:id} : delete the "id" contactMoment.
     *
     * @param id the id of the contactMomentDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/contact-moments/{id}")
    public ResponseEntity<Void> deleteContactMoment(@PathVariable Long id) {
        log.debug("REST request to delete ContactMoment : {}", id);
        contactMomentService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
