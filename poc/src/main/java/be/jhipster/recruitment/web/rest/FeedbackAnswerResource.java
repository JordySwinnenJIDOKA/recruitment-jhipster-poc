package be.jhipster.recruitment.web.rest;

import be.jhipster.recruitment.service.FeedbackAnswerService;
import be.jhipster.recruitment.web.rest.errors.BadRequestAlertException;
import be.jhipster.recruitment.service.dto.FeedbackAnswerDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link be.jhipster.recruitment.domain.FeedbackAnswer}.
 */
@RestController
@RequestMapping("/api")
public class FeedbackAnswerResource {

    private final Logger log = LoggerFactory.getLogger(FeedbackAnswerResource.class);

    private static final String ENTITY_NAME = "feedbackAnswer";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final FeedbackAnswerService feedbackAnswerService;

    public FeedbackAnswerResource(FeedbackAnswerService feedbackAnswerService) {
        this.feedbackAnswerService = feedbackAnswerService;
    }

    /**
     * {@code POST  /feedback-answers} : Create a new feedbackAnswer.
     *
     * @param feedbackAnswerDTO the feedbackAnswerDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new feedbackAnswerDTO, or with status {@code 400 (Bad Request)} if the feedbackAnswer has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/feedback-answers")
    public ResponseEntity<FeedbackAnswerDTO> createFeedbackAnswer(@Valid @RequestBody FeedbackAnswerDTO feedbackAnswerDTO) throws URISyntaxException {
        log.debug("REST request to save FeedbackAnswer : {}", feedbackAnswerDTO);
        if (feedbackAnswerDTO.getId() != null) {
            throw new BadRequestAlertException("A new feedbackAnswer cannot already have an ID", ENTITY_NAME, "idexists");
        }
        FeedbackAnswerDTO result = feedbackAnswerService.save(feedbackAnswerDTO);
        return ResponseEntity.created(new URI("/api/feedback-answers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /feedback-answers} : Updates an existing feedbackAnswer.
     *
     * @param feedbackAnswerDTO the feedbackAnswerDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated feedbackAnswerDTO,
     * or with status {@code 400 (Bad Request)} if the feedbackAnswerDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the feedbackAnswerDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/feedback-answers")
    public ResponseEntity<FeedbackAnswerDTO> updateFeedbackAnswer(@Valid @RequestBody FeedbackAnswerDTO feedbackAnswerDTO) throws URISyntaxException {
        log.debug("REST request to update FeedbackAnswer : {}", feedbackAnswerDTO);
        if (feedbackAnswerDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        FeedbackAnswerDTO result = feedbackAnswerService.save(feedbackAnswerDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, feedbackAnswerDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /feedback-answers} : get all the feedbackAnswers.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of feedbackAnswers in body.
     */
    @GetMapping("/feedback-answers")
    public List<FeedbackAnswerDTO> getAllFeedbackAnswers() {
        log.debug("REST request to get all FeedbackAnswers");
        return feedbackAnswerService.findAll();
    }

    /**
     * {@code GET  /feedback-answers/:id} : get the "id" feedbackAnswer.
     *
     * @param id the id of the feedbackAnswerDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the feedbackAnswerDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/feedback-answers/{id}")
    public ResponseEntity<FeedbackAnswerDTO> getFeedbackAnswer(@PathVariable Long id) {
        log.debug("REST request to get FeedbackAnswer : {}", id);
        Optional<FeedbackAnswerDTO> feedbackAnswerDTO = feedbackAnswerService.findOne(id);
        return ResponseUtil.wrapOrNotFound(feedbackAnswerDTO);
    }

    /**
     * {@code DELETE  /feedback-answers/:id} : delete the "id" feedbackAnswer.
     *
     * @param id the id of the feedbackAnswerDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/feedback-answers/{id}")
    public ResponseEntity<Void> deleteFeedbackAnswer(@PathVariable Long id) {
        log.debug("REST request to delete FeedbackAnswer : {}", id);
        feedbackAnswerService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
