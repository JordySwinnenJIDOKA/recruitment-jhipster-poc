package be.jhipster.recruitment.web.rest;

import be.jhipster.recruitment.service.FeedbackQuestionService;
import be.jhipster.recruitment.web.rest.errors.BadRequestAlertException;
import be.jhipster.recruitment.service.dto.FeedbackQuestionDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link be.jhipster.recruitment.domain.FeedbackQuestion}.
 */
@RestController
@RequestMapping("/api")
public class FeedbackQuestionResource {

    private final Logger log = LoggerFactory.getLogger(FeedbackQuestionResource.class);

    private static final String ENTITY_NAME = "feedbackQuestion";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final FeedbackQuestionService feedbackQuestionService;

    public FeedbackQuestionResource(FeedbackQuestionService feedbackQuestionService) {
        this.feedbackQuestionService = feedbackQuestionService;
    }

    /**
     * {@code POST  /feedback-questions} : Create a new feedbackQuestion.
     *
     * @param feedbackQuestionDTO the feedbackQuestionDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new feedbackQuestionDTO, or with status {@code 400 (Bad Request)} if the feedbackQuestion has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/feedback-questions")
    public ResponseEntity<FeedbackQuestionDTO> createFeedbackQuestion(@Valid @RequestBody FeedbackQuestionDTO feedbackQuestionDTO) throws URISyntaxException {
        log.debug("REST request to save FeedbackQuestion : {}", feedbackQuestionDTO);
        if (feedbackQuestionDTO.getId() != null) {
            throw new BadRequestAlertException("A new feedbackQuestion cannot already have an ID", ENTITY_NAME, "idexists");
        }
        FeedbackQuestionDTO result = feedbackQuestionService.save(feedbackQuestionDTO);
        return ResponseEntity.created(new URI("/api/feedback-questions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /feedback-questions} : Updates an existing feedbackQuestion.
     *
     * @param feedbackQuestionDTO the feedbackQuestionDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated feedbackQuestionDTO,
     * or with status {@code 400 (Bad Request)} if the feedbackQuestionDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the feedbackQuestionDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/feedback-questions")
    public ResponseEntity<FeedbackQuestionDTO> updateFeedbackQuestion(@Valid @RequestBody FeedbackQuestionDTO feedbackQuestionDTO) throws URISyntaxException {
        log.debug("REST request to update FeedbackQuestion : {}", feedbackQuestionDTO);
        if (feedbackQuestionDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        FeedbackQuestionDTO result = feedbackQuestionService.save(feedbackQuestionDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, feedbackQuestionDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /feedback-questions} : get all the feedbackQuestions.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of feedbackQuestions in body.
     */
    @GetMapping("/feedback-questions")
    public List<FeedbackQuestionDTO> getAllFeedbackQuestions() {
        log.debug("REST request to get all FeedbackQuestions");
        return feedbackQuestionService.findAll();
    }

    /**
     * {@code GET  /feedback-questions/:id} : get the "id" feedbackQuestion.
     *
     * @param id the id of the feedbackQuestionDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the feedbackQuestionDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/feedback-questions/{id}")
    public ResponseEntity<FeedbackQuestionDTO> getFeedbackQuestion(@PathVariable Long id) {
        log.debug("REST request to get FeedbackQuestion : {}", id);
        Optional<FeedbackQuestionDTO> feedbackQuestionDTO = feedbackQuestionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(feedbackQuestionDTO);
    }

    /**
     * {@code DELETE  /feedback-questions/:id} : delete the "id" feedbackQuestion.
     *
     * @param id the id of the feedbackQuestionDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/feedback-questions/{id}")
    public ResponseEntity<Void> deleteFeedbackQuestion(@PathVariable Long id) {
        log.debug("REST request to delete FeedbackQuestion : {}", id);
        feedbackQuestionService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
