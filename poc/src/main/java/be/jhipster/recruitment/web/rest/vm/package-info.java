/**
 * View Models used by Spring MVC REST controllers.
 */
package be.jhipster.recruitment.web.rest.vm;
