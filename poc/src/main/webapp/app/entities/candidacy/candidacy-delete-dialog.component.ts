import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ICandidacy } from 'app/shared/model/candidacy.model';
import { CandidacyService } from './candidacy.service';

@Component({
  selector: 'jhi-candidacy-delete-dialog',
  templateUrl: './candidacy-delete-dialog.component.html'
})
export class CandidacyDeleteDialogComponent {
  candidacy: ICandidacy;

  constructor(protected candidacyService: CandidacyService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.candidacyService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'candidacyListModification',
        content: 'Deleted an candidacy'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-candidacy-delete-popup',
  template: ''
})
export class CandidacyDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ candidacy }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(CandidacyDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.candidacy = candidacy;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/candidacy', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/candidacy', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
