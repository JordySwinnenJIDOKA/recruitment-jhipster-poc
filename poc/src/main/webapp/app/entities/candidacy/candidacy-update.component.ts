import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { ICandidacy, Candidacy } from 'app/shared/model/candidacy.model';
import { CandidacyService } from './candidacy.service';
import { ICandidate } from 'app/shared/model/candidate.model';
import { CandidateService } from 'app/entities/candidate';
import { IVacancy } from 'app/shared/model/vacancy.model';
import { VacancyService } from 'app/entities/vacancy';

@Component({
  selector: 'jhi-candidacy-update',
  templateUrl: './candidacy-update.component.html'
})
export class CandidacyUpdateComponent implements OnInit {
  candidacy: ICandidacy;
  isSaving: boolean;

  candidates: ICandidate[];

  vacancies: IVacancy[];

  editForm = this.fb.group({
    id: [],
    candidacyStatus: [null, [Validators.required]],
    motivation: [null, [Validators.required]],
    candidateId: [],
    vacancyId: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected candidacyService: CandidacyService,
    protected candidateService: CandidateService,
    protected vacancyService: VacancyService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ candidacy }) => {
      this.updateForm(candidacy);
      this.candidacy = candidacy;
    });
    this.candidateService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<ICandidate[]>) => mayBeOk.ok),
        map((response: HttpResponse<ICandidate[]>) => response.body)
      )
      .subscribe((res: ICandidate[]) => (this.candidates = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.vacancyService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IVacancy[]>) => mayBeOk.ok),
        map((response: HttpResponse<IVacancy[]>) => response.body)
      )
      .subscribe((res: IVacancy[]) => (this.vacancies = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(candidacy: ICandidacy) {
    this.editForm.patchValue({
      id: candidacy.id,
      candidacyStatus: candidacy.candidacyStatus,
      motivation: candidacy.motivation,
      candidateId: candidacy.candidateId,
      vacancyId: candidacy.vacancyId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const candidacy = this.createFromForm();
    if (candidacy.id !== undefined) {
      this.subscribeToSaveResponse(this.candidacyService.update(candidacy));
    } else {
      this.subscribeToSaveResponse(this.candidacyService.create(candidacy));
    }
  }

  private createFromForm(): ICandidacy {
    const entity = {
      ...new Candidacy(),
      id: this.editForm.get(['id']).value,
      candidacyStatus: this.editForm.get(['candidacyStatus']).value,
      motivation: this.editForm.get(['motivation']).value,
      candidateId: this.editForm.get(['candidateId']).value,
      vacancyId: this.editForm.get(['vacancyId']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICandidacy>>) {
    result.subscribe((res: HttpResponse<ICandidacy>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackCandidateById(index: number, item: ICandidate) {
    return item.id;
  }

  trackVacancyById(index: number, item: IVacancy) {
    return item.id;
  }
}
