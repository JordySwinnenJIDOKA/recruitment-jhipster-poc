import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { JhipsterRecruitmentSharedModule } from 'app/shared';
import {
  CandidacyComponent,
  CandidacyDetailComponent,
  CandidacyUpdateComponent,
  CandidacyDeletePopupComponent,
  CandidacyDeleteDialogComponent,
  candidacyRoute,
  candidacyPopupRoute
} from './';

const ENTITY_STATES = [...candidacyRoute, ...candidacyPopupRoute];

@NgModule({
  imports: [JhipsterRecruitmentSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    CandidacyComponent,
    CandidacyDetailComponent,
    CandidacyUpdateComponent,
    CandidacyDeleteDialogComponent,
    CandidacyDeletePopupComponent
  ],
  entryComponents: [CandidacyComponent, CandidacyUpdateComponent, CandidacyDeleteDialogComponent, CandidacyDeletePopupComponent],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JhipsterRecruitmentCandidacyModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
