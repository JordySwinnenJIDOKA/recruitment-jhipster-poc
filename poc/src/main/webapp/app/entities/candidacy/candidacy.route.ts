import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Candidacy } from 'app/shared/model/candidacy.model';
import { CandidacyService } from './candidacy.service';
import { CandidacyComponent } from './candidacy.component';
import { CandidacyDetailComponent } from './candidacy-detail.component';
import { CandidacyUpdateComponent } from './candidacy-update.component';
import { CandidacyDeletePopupComponent } from './candidacy-delete-dialog.component';
import { ICandidacy } from 'app/shared/model/candidacy.model';

@Injectable({ providedIn: 'root' })
export class CandidacyResolve implements Resolve<ICandidacy> {
  constructor(private service: CandidacyService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ICandidacy> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Candidacy>) => response.ok),
        map((candidacy: HttpResponse<Candidacy>) => candidacy.body)
      );
    }
    return of(new Candidacy());
  }
}

export const candidacyRoute: Routes = [
  {
    path: '',
    component: CandidacyComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'jhipsterRecruitmentApp.candidacy.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: CandidacyDetailComponent,
    resolve: {
      candidacy: CandidacyResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'jhipsterRecruitmentApp.candidacy.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: CandidacyUpdateComponent,
    resolve: {
      candidacy: CandidacyResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'jhipsterRecruitmentApp.candidacy.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: CandidacyUpdateComponent,
    resolve: {
      candidacy: CandidacyResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'jhipsterRecruitmentApp.candidacy.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const candidacyPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: CandidacyDeletePopupComponent,
    resolve: {
      candidacy: CandidacyResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'jhipsterRecruitmentApp.candidacy.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
