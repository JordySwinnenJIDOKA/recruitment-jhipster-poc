import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ICandidacy } from 'app/shared/model/candidacy.model';

type EntityResponseType = HttpResponse<ICandidacy>;
type EntityArrayResponseType = HttpResponse<ICandidacy[]>;

@Injectable({ providedIn: 'root' })
export class CandidacyService {
  public resourceUrl = SERVER_API_URL + 'api/candidacies';

  constructor(protected http: HttpClient) {}

  create(candidacy: ICandidacy): Observable<EntityResponseType> {
    return this.http.post<ICandidacy>(this.resourceUrl, candidacy, { observe: 'response' });
  }

  update(candidacy: ICandidacy): Observable<EntityResponseType> {
    return this.http.put<ICandidacy>(this.resourceUrl, candidacy, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ICandidacy>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICandidacy[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
