import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ICandidateComment } from 'app/shared/model/candidate-comment.model';
import { CandidateCommentService } from './candidate-comment.service';

@Component({
  selector: 'jhi-candidate-comment-delete-dialog',
  templateUrl: './candidate-comment-delete-dialog.component.html'
})
export class CandidateCommentDeleteDialogComponent {
  candidateComment: ICandidateComment;

  constructor(
    protected candidateCommentService: CandidateCommentService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.candidateCommentService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'candidateCommentListModification',
        content: 'Deleted an candidateComment'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-candidate-comment-delete-popup',
  template: ''
})
export class CandidateCommentDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ candidateComment }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(CandidateCommentDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.candidateComment = candidateComment;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/candidate-comment', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/candidate-comment', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
