import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICandidateComment } from 'app/shared/model/candidate-comment.model';

@Component({
  selector: 'jhi-candidate-comment-detail',
  templateUrl: './candidate-comment-detail.component.html'
})
export class CandidateCommentDetailComponent implements OnInit {
  candidateComment: ICandidateComment;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ candidateComment }) => {
      this.candidateComment = candidateComment;
    });
  }

  previousState() {
    window.history.back();
  }
}
