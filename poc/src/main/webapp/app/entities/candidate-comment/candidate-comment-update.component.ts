import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { ICandidateComment, CandidateComment } from 'app/shared/model/candidate-comment.model';
import { CandidateCommentService } from './candidate-comment.service';
import { ICandidate } from 'app/shared/model/candidate.model';
import { CandidateService } from 'app/entities/candidate';

@Component({
  selector: 'jhi-candidate-comment-update',
  templateUrl: './candidate-comment-update.component.html'
})
export class CandidateCommentUpdateComponent implements OnInit {
  candidateComment: ICandidateComment;
  isSaving: boolean;

  candidates: ICandidate[];

  editForm = this.fb.group({
    id: [],
    context: [null, [Validators.required]],
    commenter: [null, [Validators.required]],
    candidateId: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected candidateCommentService: CandidateCommentService,
    protected candidateService: CandidateService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ candidateComment }) => {
      this.updateForm(candidateComment);
      this.candidateComment = candidateComment;
    });
    this.candidateService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<ICandidate[]>) => mayBeOk.ok),
        map((response: HttpResponse<ICandidate[]>) => response.body)
      )
      .subscribe((res: ICandidate[]) => (this.candidates = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(candidateComment: ICandidateComment) {
    this.editForm.patchValue({
      id: candidateComment.id,
      context: candidateComment.context,
      commenter: candidateComment.commenter,
      candidateId: candidateComment.candidateId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const candidateComment = this.createFromForm();
    if (candidateComment.id !== undefined) {
      this.subscribeToSaveResponse(this.candidateCommentService.update(candidateComment));
    } else {
      this.subscribeToSaveResponse(this.candidateCommentService.create(candidateComment));
    }
  }

  private createFromForm(): ICandidateComment {
    const entity = {
      ...new CandidateComment(),
      id: this.editForm.get(['id']).value,
      context: this.editForm.get(['context']).value,
      commenter: this.editForm.get(['commenter']).value,
      candidateId: this.editForm.get(['candidateId']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICandidateComment>>) {
    result.subscribe((res: HttpResponse<ICandidateComment>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackCandidateById(index: number, item: ICandidate) {
    return item.id;
  }
}
