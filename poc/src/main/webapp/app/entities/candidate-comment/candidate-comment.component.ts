import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ICandidateComment } from 'app/shared/model/candidate-comment.model';
import { AccountService } from 'app/core';
import { CandidateCommentService } from './candidate-comment.service';

@Component({
  selector: 'jhi-candidate-comment',
  templateUrl: './candidate-comment.component.html'
})
export class CandidateCommentComponent implements OnInit, OnDestroy {
  candidateComments: ICandidateComment[];
  currentAccount: any;
  eventSubscriber: Subscription;

  constructor(
    protected candidateCommentService: CandidateCommentService,
    protected jhiAlertService: JhiAlertService,
    protected eventManager: JhiEventManager,
    protected accountService: AccountService
  ) {}

  loadAll() {
    this.candidateCommentService
      .query()
      .pipe(
        filter((res: HttpResponse<ICandidateComment[]>) => res.ok),
        map((res: HttpResponse<ICandidateComment[]>) => res.body)
      )
      .subscribe(
        (res: ICandidateComment[]) => {
          this.candidateComments = res;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInCandidateComments();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: ICandidateComment) {
    return item.id;
  }

  registerChangeInCandidateComments() {
    this.eventSubscriber = this.eventManager.subscribe('candidateCommentListModification', response => this.loadAll());
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
