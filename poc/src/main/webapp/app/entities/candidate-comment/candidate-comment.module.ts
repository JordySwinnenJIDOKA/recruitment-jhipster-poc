import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { JhipsterRecruitmentSharedModule } from 'app/shared';
import {
  CandidateCommentComponent,
  CandidateCommentDetailComponent,
  CandidateCommentUpdateComponent,
  CandidateCommentDeletePopupComponent,
  CandidateCommentDeleteDialogComponent,
  candidateCommentRoute,
  candidateCommentPopupRoute
} from './';

const ENTITY_STATES = [...candidateCommentRoute, ...candidateCommentPopupRoute];

@NgModule({
  imports: [JhipsterRecruitmentSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    CandidateCommentComponent,
    CandidateCommentDetailComponent,
    CandidateCommentUpdateComponent,
    CandidateCommentDeleteDialogComponent,
    CandidateCommentDeletePopupComponent
  ],
  entryComponents: [
    CandidateCommentComponent,
    CandidateCommentUpdateComponent,
    CandidateCommentDeleteDialogComponent,
    CandidateCommentDeletePopupComponent
  ],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JhipsterRecruitmentCandidateCommentModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
