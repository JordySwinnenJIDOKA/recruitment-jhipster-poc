import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { CandidateComment } from 'app/shared/model/candidate-comment.model';
import { CandidateCommentService } from './candidate-comment.service';
import { CandidateCommentComponent } from './candidate-comment.component';
import { CandidateCommentDetailComponent } from './candidate-comment-detail.component';
import { CandidateCommentUpdateComponent } from './candidate-comment-update.component';
import { CandidateCommentDeletePopupComponent } from './candidate-comment-delete-dialog.component';
import { ICandidateComment } from 'app/shared/model/candidate-comment.model';

@Injectable({ providedIn: 'root' })
export class CandidateCommentResolve implements Resolve<ICandidateComment> {
  constructor(private service: CandidateCommentService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ICandidateComment> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<CandidateComment>) => response.ok),
        map((candidateComment: HttpResponse<CandidateComment>) => candidateComment.body)
      );
    }
    return of(new CandidateComment());
  }
}

export const candidateCommentRoute: Routes = [
  {
    path: '',
    component: CandidateCommentComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'jhipsterRecruitmentApp.candidateComment.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: CandidateCommentDetailComponent,
    resolve: {
      candidateComment: CandidateCommentResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'jhipsterRecruitmentApp.candidateComment.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: CandidateCommentUpdateComponent,
    resolve: {
      candidateComment: CandidateCommentResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'jhipsterRecruitmentApp.candidateComment.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: CandidateCommentUpdateComponent,
    resolve: {
      candidateComment: CandidateCommentResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'jhipsterRecruitmentApp.candidateComment.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const candidateCommentPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: CandidateCommentDeletePopupComponent,
    resolve: {
      candidateComment: CandidateCommentResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'jhipsterRecruitmentApp.candidateComment.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
