import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ICandidateComment } from 'app/shared/model/candidate-comment.model';

type EntityResponseType = HttpResponse<ICandidateComment>;
type EntityArrayResponseType = HttpResponse<ICandidateComment[]>;

@Injectable({ providedIn: 'root' })
export class CandidateCommentService {
  public resourceUrl = SERVER_API_URL + 'api/candidate-comments';

  constructor(protected http: HttpClient) {}

  create(candidateComment: ICandidateComment): Observable<EntityResponseType> {
    return this.http.post<ICandidateComment>(this.resourceUrl, candidateComment, { observe: 'response' });
  }

  update(candidateComment: ICandidateComment): Observable<EntityResponseType> {
    return this.http.put<ICandidateComment>(this.resourceUrl, candidateComment, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ICandidateComment>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICandidateComment[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
