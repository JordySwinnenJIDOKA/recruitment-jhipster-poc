export * from './candidate-comment.service';
export * from './candidate-comment-update.component';
export * from './candidate-comment-delete-dialog.component';
export * from './candidate-comment-detail.component';
export * from './candidate-comment.component';
export * from './candidate-comment.route';
