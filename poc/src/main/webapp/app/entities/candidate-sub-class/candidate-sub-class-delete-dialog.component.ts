import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ICandidateSubClass } from 'app/shared/model/candidate-sub-class.model';
import { CandidateSubClassService } from './candidate-sub-class.service';

@Component({
  selector: 'jhi-candidate-sub-class-delete-dialog',
  templateUrl: './candidate-sub-class-delete-dialog.component.html'
})
export class CandidateSubClassDeleteDialogComponent {
  candidateSubClass: ICandidateSubClass;

  constructor(
    protected candidateSubClassService: CandidateSubClassService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.candidateSubClassService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'candidateSubClassListModification',
        content: 'Deleted an candidateSubClass'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-candidate-sub-class-delete-popup',
  template: ''
})
export class CandidateSubClassDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ candidateSubClass }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(CandidateSubClassDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.candidateSubClass = candidateSubClass;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/candidate-sub-class', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/candidate-sub-class', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
