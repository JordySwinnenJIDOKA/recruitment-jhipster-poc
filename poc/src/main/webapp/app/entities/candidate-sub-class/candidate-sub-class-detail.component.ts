import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICandidateSubClass } from 'app/shared/model/candidate-sub-class.model';

@Component({
  selector: 'jhi-candidate-sub-class-detail',
  templateUrl: './candidate-sub-class-detail.component.html'
})
export class CandidateSubClassDetailComponent implements OnInit {
  candidateSubClass: ICandidateSubClass;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ candidateSubClass }) => {
      this.candidateSubClass = candidateSubClass;
    });
  }

  previousState() {
    window.history.back();
  }
}
