import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { ICandidateSubClass, CandidateSubClass } from 'app/shared/model/candidate-sub-class.model';
import { CandidateSubClassService } from './candidate-sub-class.service';

@Component({
  selector: 'jhi-candidate-sub-class-update',
  templateUrl: './candidate-sub-class-update.component.html'
})
export class CandidateSubClassUpdateComponent implements OnInit {
  candidateSubClass: ICandidateSubClass;
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    candidateType: []
  });

  constructor(
    protected candidateSubClassService: CandidateSubClassService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ candidateSubClass }) => {
      this.updateForm(candidateSubClass);
      this.candidateSubClass = candidateSubClass;
    });
  }

  updateForm(candidateSubClass: ICandidateSubClass) {
    this.editForm.patchValue({
      id: candidateSubClass.id,
      candidateType: candidateSubClass.candidateType
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const candidateSubClass = this.createFromForm();
    if (candidateSubClass.id !== undefined) {
      this.subscribeToSaveResponse(this.candidateSubClassService.update(candidateSubClass));
    } else {
      this.subscribeToSaveResponse(this.candidateSubClassService.create(candidateSubClass));
    }
  }

  private createFromForm(): ICandidateSubClass {
    const entity = {
      ...new CandidateSubClass(),
      id: this.editForm.get(['id']).value,
      candidateType: this.editForm.get(['candidateType']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICandidateSubClass>>) {
    result.subscribe((res: HttpResponse<ICandidateSubClass>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
