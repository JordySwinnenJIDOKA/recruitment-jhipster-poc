import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ICandidateSubClass } from 'app/shared/model/candidate-sub-class.model';
import { AccountService } from 'app/core';
import { CandidateSubClassService } from './candidate-sub-class.service';

@Component({
  selector: 'jhi-candidate-sub-class',
  templateUrl: './candidate-sub-class.component.html'
})
export class CandidateSubClassComponent implements OnInit, OnDestroy {
  candidateSubClasses: ICandidateSubClass[];
  currentAccount: any;
  eventSubscriber: Subscription;

  constructor(
    protected candidateSubClassService: CandidateSubClassService,
    protected jhiAlertService: JhiAlertService,
    protected eventManager: JhiEventManager,
    protected accountService: AccountService
  ) {}

  loadAll() {
    this.candidateSubClassService
      .query()
      .pipe(
        filter((res: HttpResponse<ICandidateSubClass[]>) => res.ok),
        map((res: HttpResponse<ICandidateSubClass[]>) => res.body)
      )
      .subscribe(
        (res: ICandidateSubClass[]) => {
          this.candidateSubClasses = res;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInCandidateSubClasses();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: ICandidateSubClass) {
    return item.id;
  }

  registerChangeInCandidateSubClasses() {
    this.eventSubscriber = this.eventManager.subscribe('candidateSubClassListModification', response => this.loadAll());
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
