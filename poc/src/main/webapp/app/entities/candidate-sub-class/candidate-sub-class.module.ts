import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { JhipsterRecruitmentSharedModule } from 'app/shared';
import {
  CandidateSubClassComponent,
  CandidateSubClassDetailComponent,
  CandidateSubClassUpdateComponent,
  CandidateSubClassDeletePopupComponent,
  CandidateSubClassDeleteDialogComponent,
  candidateSubClassRoute,
  candidateSubClassPopupRoute
} from './';

const ENTITY_STATES = [...candidateSubClassRoute, ...candidateSubClassPopupRoute];

@NgModule({
  imports: [JhipsterRecruitmentSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    CandidateSubClassComponent,
    CandidateSubClassDetailComponent,
    CandidateSubClassUpdateComponent,
    CandidateSubClassDeleteDialogComponent,
    CandidateSubClassDeletePopupComponent
  ],
  entryComponents: [
    CandidateSubClassComponent,
    CandidateSubClassUpdateComponent,
    CandidateSubClassDeleteDialogComponent,
    CandidateSubClassDeletePopupComponent
  ],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JhipsterRecruitmentCandidateSubClassModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
