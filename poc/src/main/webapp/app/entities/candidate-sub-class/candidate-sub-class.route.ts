import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { CandidateSubClass } from 'app/shared/model/candidate-sub-class.model';
import { CandidateSubClassService } from './candidate-sub-class.service';
import { CandidateSubClassComponent } from './candidate-sub-class.component';
import { CandidateSubClassDetailComponent } from './candidate-sub-class-detail.component';
import { CandidateSubClassUpdateComponent } from './candidate-sub-class-update.component';
import { CandidateSubClassDeletePopupComponent } from './candidate-sub-class-delete-dialog.component';
import { ICandidateSubClass } from 'app/shared/model/candidate-sub-class.model';

@Injectable({ providedIn: 'root' })
export class CandidateSubClassResolve implements Resolve<ICandidateSubClass> {
  constructor(private service: CandidateSubClassService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ICandidateSubClass> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<CandidateSubClass>) => response.ok),
        map((candidateSubClass: HttpResponse<CandidateSubClass>) => candidateSubClass.body)
      );
    }
    return of(new CandidateSubClass());
  }
}

export const candidateSubClassRoute: Routes = [
  {
    path: '',
    component: CandidateSubClassComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'jhipsterRecruitmentApp.candidateSubClass.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: CandidateSubClassDetailComponent,
    resolve: {
      candidateSubClass: CandidateSubClassResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'jhipsterRecruitmentApp.candidateSubClass.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: CandidateSubClassUpdateComponent,
    resolve: {
      candidateSubClass: CandidateSubClassResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'jhipsterRecruitmentApp.candidateSubClass.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: CandidateSubClassUpdateComponent,
    resolve: {
      candidateSubClass: CandidateSubClassResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'jhipsterRecruitmentApp.candidateSubClass.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const candidateSubClassPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: CandidateSubClassDeletePopupComponent,
    resolve: {
      candidateSubClass: CandidateSubClassResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'jhipsterRecruitmentApp.candidateSubClass.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
