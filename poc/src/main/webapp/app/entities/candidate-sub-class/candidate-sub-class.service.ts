import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ICandidateSubClass } from 'app/shared/model/candidate-sub-class.model';

type EntityResponseType = HttpResponse<ICandidateSubClass>;
type EntityArrayResponseType = HttpResponse<ICandidateSubClass[]>;

@Injectable({ providedIn: 'root' })
export class CandidateSubClassService {
  public resourceUrl = SERVER_API_URL + 'api/candidate-sub-classes';

  constructor(protected http: HttpClient) {}

  create(candidateSubClass: ICandidateSubClass): Observable<EntityResponseType> {
    return this.http.post<ICandidateSubClass>(this.resourceUrl, candidateSubClass, { observe: 'response' });
  }

  update(candidateSubClass: ICandidateSubClass): Observable<EntityResponseType> {
    return this.http.put<ICandidateSubClass>(this.resourceUrl, candidateSubClass, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ICandidateSubClass>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICandidateSubClass[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
