import { Component, OnInit, ElementRef } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService, JhiDataUtils } from 'ng-jhipster';
import { ICandidate, Candidate } from 'app/shared/model/candidate.model';
import { CandidateService } from './candidate.service';
import { IDocument } from 'app/shared/model/document.model';
import { DocumentService } from 'app/entities/document';
import { ICandidateSubClass } from 'app/shared/model/candidate-sub-class.model';
import { CandidateSubClassService } from 'app/entities/candidate-sub-class';

@Component({
  selector: 'jhi-candidate-update',
  templateUrl: './candidate-update.component.html'
})
export class CandidateUpdateComponent implements OnInit {
  candidate: ICandidate;
  isSaving: boolean;

  documents: IDocument[];

  candidatesubclasses: ICandidateSubClass[];

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required]],
    phone: [null, [Validators.required]],
    email: [null, [Validators.required]],
    candidateStatus: [],
    candidateFunction: [null, [Validators.required]],
    dateAvailable: [null, [Validators.required]],
    imageContent: [],
    imageContentContentType: [],
    candidateTrack: [null, [Validators.required]],
    recruitmentChannel: [null, [Validators.required]],
    linkedInProfilePath: [],
    documentId: [],
    subClassId: []
  });

  constructor(
    protected dataUtils: JhiDataUtils,
    protected jhiAlertService: JhiAlertService,
    protected candidateService: CandidateService,
    protected documentService: DocumentService,
    protected candidateSubClassService: CandidateSubClassService,
    protected elementRef: ElementRef,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ candidate }) => {
      this.updateForm(candidate);
      this.candidate = candidate;
    });
    this.documentService
      .query({ filter: 'candidate-is-null' })
      .pipe(
        filter((mayBeOk: HttpResponse<IDocument[]>) => mayBeOk.ok),
        map((response: HttpResponse<IDocument[]>) => response.body)
      )
      .subscribe(
        (res: IDocument[]) => {
          if (!this.candidate.documentId) {
            this.documents = res;
          } else {
            this.documentService
              .find(this.candidate.documentId)
              .pipe(
                filter((subResMayBeOk: HttpResponse<IDocument>) => subResMayBeOk.ok),
                map((subResponse: HttpResponse<IDocument>) => subResponse.body)
              )
              .subscribe(
                (subRes: IDocument) => (this.documents = [subRes].concat(res)),
                (subRes: HttpErrorResponse) => this.onError(subRes.message)
              );
          }
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
    this.candidateSubClassService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<ICandidateSubClass[]>) => mayBeOk.ok),
        map((response: HttpResponse<ICandidateSubClass[]>) => response.body)
      )
      .subscribe((res: ICandidateSubClass[]) => (this.candidatesubclasses = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(candidate: ICandidate) {
    this.editForm.patchValue({
      id: candidate.id,
      name: candidate.name,
      phone: candidate.phone,
      email: candidate.email,
      candidateStatus: candidate.candidateStatus,
      candidateFunction: candidate.candidateFunction,
      dateAvailable: candidate.dateAvailable,
      imageContent: candidate.imageContent,
      imageContentContentType: candidate.imageContentContentType,
      candidateTrack: candidate.candidateTrack,
      recruitmentChannel: candidate.recruitmentChannel,
      linkedInProfilePath: candidate.linkedInProfilePath,
      documentId: candidate.documentId,
      subClassId: candidate.subClassId
    });
  }

  byteSize(field) {
    return this.dataUtils.byteSize(field);
  }

  openFile(contentType, field) {
    return this.dataUtils.openFile(contentType, field);
  }

  setFileData(event, field: string, isImage) {
    return new Promise((resolve, reject) => {
      if (event && event.target && event.target.files && event.target.files[0]) {
        const file = event.target.files[0];
        if (isImage && !/^image\//.test(file.type)) {
          reject(`File was expected to be an image but was found to be ${file.type}`);
        } else {
          const filedContentType: string = field + 'ContentType';
          this.dataUtils.toBase64(file, base64Data => {
            this.editForm.patchValue({
              [field]: base64Data,
              [filedContentType]: file.type
            });
          });
        }
      } else {
        reject(`Base64 data was not set as file could not be extracted from passed parameter: ${event}`);
      }
    }).then(
      () => console.log('blob added'), // sucess
      this.onError
    );
  }

  clearInputImage(field: string, fieldContentType: string, idInput: string) {
    this.editForm.patchValue({
      [field]: null,
      [fieldContentType]: null
    });
    if (this.elementRef && idInput && this.elementRef.nativeElement.querySelector('#' + idInput)) {
      this.elementRef.nativeElement.querySelector('#' + idInput).value = null;
    }
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const candidate = this.createFromForm();
    if (candidate.id !== undefined) {
      this.subscribeToSaveResponse(this.candidateService.update(candidate));
    } else {
      this.subscribeToSaveResponse(this.candidateService.create(candidate));
    }
  }

  private createFromForm(): ICandidate {
    const entity = {
      ...new Candidate(),
      id: this.editForm.get(['id']).value,
      name: this.editForm.get(['name']).value,
      phone: this.editForm.get(['phone']).value,
      email: this.editForm.get(['email']).value,
      candidateStatus: this.editForm.get(['candidateStatus']).value,
      candidateFunction: this.editForm.get(['candidateFunction']).value,
      dateAvailable: this.editForm.get(['dateAvailable']).value,
      imageContentContentType: this.editForm.get(['imageContentContentType']).value,
      imageContent: this.editForm.get(['imageContent']).value,
      candidateTrack: this.editForm.get(['candidateTrack']).value,
      recruitmentChannel: this.editForm.get(['recruitmentChannel']).value,
      linkedInProfilePath: this.editForm.get(['linkedInProfilePath']).value,
      documentId: this.editForm.get(['documentId']).value,
      subClassId: this.editForm.get(['subClassId']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICandidate>>) {
    result.subscribe((res: HttpResponse<ICandidate>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackDocumentById(index: number, item: IDocument) {
    return item.id;
  }

  trackCandidateSubClassById(index: number, item: ICandidateSubClass) {
    return item.id;
  }
}
