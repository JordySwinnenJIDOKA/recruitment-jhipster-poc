import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IContactMoment } from 'app/shared/model/contact-moment.model';
import { ContactMomentService } from './contact-moment.service';

@Component({
  selector: 'jhi-contact-moment-delete-dialog',
  templateUrl: './contact-moment-delete-dialog.component.html'
})
export class ContactMomentDeleteDialogComponent {
  contactMoment: IContactMoment;

  constructor(
    protected contactMomentService: ContactMomentService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.contactMomentService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'contactMomentListModification',
        content: 'Deleted an contactMoment'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-contact-moment-delete-popup',
  template: ''
})
export class ContactMomentDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ contactMoment }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(ContactMomentDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.contactMoment = contactMoment;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/contact-moment', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/contact-moment', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
