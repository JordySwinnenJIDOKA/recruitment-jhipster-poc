import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IContactMoment } from 'app/shared/model/contact-moment.model';

@Component({
  selector: 'jhi-contact-moment-detail',
  templateUrl: './contact-moment-detail.component.html'
})
export class ContactMomentDetailComponent implements OnInit {
  contactMoment: IContactMoment;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ contactMoment }) => {
      this.contactMoment = contactMoment;
    });
  }

  previousState() {
    window.history.back();
  }
}
