import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { JhiAlertService } from 'ng-jhipster';
import { IContactMoment, ContactMoment } from 'app/shared/model/contact-moment.model';
import { ContactMomentService } from './contact-moment.service';
import { ICandidacy } from 'app/shared/model/candidacy.model';
import { CandidacyService } from 'app/entities/candidacy';
import { ICandidate } from 'app/shared/model/candidate.model';
import { CandidateService } from 'app/entities/candidate';

@Component({
  selector: 'jhi-contact-moment-update',
  templateUrl: './contact-moment-update.component.html'
})
export class ContactMomentUpdateComponent implements OnInit {
  contactMoment: IContactMoment;
  isSaving: boolean;

  candidacies: ICandidacy[];

  candidates: ICandidate[];
  contactDateDp: any;

  editForm = this.fb.group({
    id: [],
    contactType: [null, [Validators.required]],
    contactDate: [null, [Validators.required]],
    contactContext: [null, [Validators.required]],
    interviewer: [null, [Validators.required]],
    location: [],
    candidacyId: [],
    candidateId: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected contactMomentService: ContactMomentService,
    protected candidacyService: CandidacyService,
    protected candidateService: CandidateService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ contactMoment }) => {
      this.updateForm(contactMoment);
      this.contactMoment = contactMoment;
    });
    this.candidacyService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<ICandidacy[]>) => mayBeOk.ok),
        map((response: HttpResponse<ICandidacy[]>) => response.body)
      )
      .subscribe((res: ICandidacy[]) => (this.candidacies = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.candidateService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<ICandidate[]>) => mayBeOk.ok),
        map((response: HttpResponse<ICandidate[]>) => response.body)
      )
      .subscribe((res: ICandidate[]) => (this.candidates = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(contactMoment: IContactMoment) {
    this.editForm.patchValue({
      id: contactMoment.id,
      contactType: contactMoment.contactType,
      contactDate: contactMoment.contactDate,
      contactContext: contactMoment.contactContext,
      interviewer: contactMoment.interviewer,
      location: contactMoment.location,
      candidacyId: contactMoment.candidacyId,
      candidateId: contactMoment.candidateId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const contactMoment = this.createFromForm();
    if (contactMoment.id !== undefined) {
      this.subscribeToSaveResponse(this.contactMomentService.update(contactMoment));
    } else {
      this.subscribeToSaveResponse(this.contactMomentService.create(contactMoment));
    }
  }

  private createFromForm(): IContactMoment {
    const entity = {
      ...new ContactMoment(),
      id: this.editForm.get(['id']).value,
      contactType: this.editForm.get(['contactType']).value,
      contactDate: this.editForm.get(['contactDate']).value,
      contactContext: this.editForm.get(['contactContext']).value,
      interviewer: this.editForm.get(['interviewer']).value,
      location: this.editForm.get(['location']).value,
      candidacyId: this.editForm.get(['candidacyId']).value,
      candidateId: this.editForm.get(['candidateId']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IContactMoment>>) {
    result.subscribe((res: HttpResponse<IContactMoment>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackCandidacyById(index: number, item: ICandidacy) {
    return item.id;
  }

  trackCandidateById(index: number, item: ICandidate) {
    return item.id;
  }
}
