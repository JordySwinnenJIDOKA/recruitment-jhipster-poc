import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IContactMoment } from 'app/shared/model/contact-moment.model';
import { AccountService } from 'app/core';
import { ContactMomentService } from './contact-moment.service';

@Component({
  selector: 'jhi-contact-moment',
  templateUrl: './contact-moment.component.html'
})
export class ContactMomentComponent implements OnInit, OnDestroy {
  contactMoments: IContactMoment[];
  currentAccount: any;
  eventSubscriber: Subscription;

  constructor(
    protected contactMomentService: ContactMomentService,
    protected jhiAlertService: JhiAlertService,
    protected eventManager: JhiEventManager,
    protected accountService: AccountService
  ) {}

  loadAll() {
    this.contactMomentService
      .query()
      .pipe(
        filter((res: HttpResponse<IContactMoment[]>) => res.ok),
        map((res: HttpResponse<IContactMoment[]>) => res.body)
      )
      .subscribe(
        (res: IContactMoment[]) => {
          this.contactMoments = res;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInContactMoments();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IContactMoment) {
    return item.id;
  }

  registerChangeInContactMoments() {
    this.eventSubscriber = this.eventManager.subscribe('contactMomentListModification', response => this.loadAll());
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
