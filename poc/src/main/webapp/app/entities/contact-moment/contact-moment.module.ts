import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { JhipsterRecruitmentSharedModule } from 'app/shared';
import {
  ContactMomentComponent,
  ContactMomentDetailComponent,
  ContactMomentUpdateComponent,
  ContactMomentDeletePopupComponent,
  ContactMomentDeleteDialogComponent,
  contactMomentRoute,
  contactMomentPopupRoute
} from './';

const ENTITY_STATES = [...contactMomentRoute, ...contactMomentPopupRoute];

@NgModule({
  imports: [JhipsterRecruitmentSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    ContactMomentComponent,
    ContactMomentDetailComponent,
    ContactMomentUpdateComponent,
    ContactMomentDeleteDialogComponent,
    ContactMomentDeletePopupComponent
  ],
  entryComponents: [
    ContactMomentComponent,
    ContactMomentUpdateComponent,
    ContactMomentDeleteDialogComponent,
    ContactMomentDeletePopupComponent
  ],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JhipsterRecruitmentContactMomentModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
