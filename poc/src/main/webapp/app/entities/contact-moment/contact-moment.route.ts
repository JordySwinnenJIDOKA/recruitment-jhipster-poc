import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { ContactMoment } from 'app/shared/model/contact-moment.model';
import { ContactMomentService } from './contact-moment.service';
import { ContactMomentComponent } from './contact-moment.component';
import { ContactMomentDetailComponent } from './contact-moment-detail.component';
import { ContactMomentUpdateComponent } from './contact-moment-update.component';
import { ContactMomentDeletePopupComponent } from './contact-moment-delete-dialog.component';
import { IContactMoment } from 'app/shared/model/contact-moment.model';

@Injectable({ providedIn: 'root' })
export class ContactMomentResolve implements Resolve<IContactMoment> {
  constructor(private service: ContactMomentService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IContactMoment> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<ContactMoment>) => response.ok),
        map((contactMoment: HttpResponse<ContactMoment>) => contactMoment.body)
      );
    }
    return of(new ContactMoment());
  }
}

export const contactMomentRoute: Routes = [
  {
    path: '',
    component: ContactMomentComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'jhipsterRecruitmentApp.contactMoment.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: ContactMomentDetailComponent,
    resolve: {
      contactMoment: ContactMomentResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'jhipsterRecruitmentApp.contactMoment.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: ContactMomentUpdateComponent,
    resolve: {
      contactMoment: ContactMomentResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'jhipsterRecruitmentApp.contactMoment.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: ContactMomentUpdateComponent,
    resolve: {
      contactMoment: ContactMomentResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'jhipsterRecruitmentApp.contactMoment.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const contactMomentPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: ContactMomentDeletePopupComponent,
    resolve: {
      contactMoment: ContactMomentResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'jhipsterRecruitmentApp.contactMoment.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
