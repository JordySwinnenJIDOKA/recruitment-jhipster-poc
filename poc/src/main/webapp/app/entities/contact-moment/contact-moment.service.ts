import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IContactMoment } from 'app/shared/model/contact-moment.model';

type EntityResponseType = HttpResponse<IContactMoment>;
type EntityArrayResponseType = HttpResponse<IContactMoment[]>;

@Injectable({ providedIn: 'root' })
export class ContactMomentService {
  public resourceUrl = SERVER_API_URL + 'api/contact-moments';

  constructor(protected http: HttpClient) {}

  create(contactMoment: IContactMoment): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(contactMoment);
    return this.http
      .post<IContactMoment>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(contactMoment: IContactMoment): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(contactMoment);
    return this.http
      .put<IContactMoment>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IContactMoment>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IContactMoment[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(contactMoment: IContactMoment): IContactMoment {
    const copy: IContactMoment = Object.assign({}, contactMoment, {
      contactDate:
        contactMoment.contactDate != null && contactMoment.contactDate.isValid() ? contactMoment.contactDate.format(DATE_FORMAT) : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.contactDate = res.body.contactDate != null ? moment(res.body.contactDate) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((contactMoment: IContactMoment) => {
        contactMoment.contactDate = contactMoment.contactDate != null ? moment(contactMoment.contactDate) : null;
      });
    }
    return res;
  }
}
