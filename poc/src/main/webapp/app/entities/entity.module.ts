import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'candidacy',
        loadChildren: './candidacy/candidacy.module#JhipsterRecruitmentCandidacyModule'
      },
      {
        path: 'candidate',
        loadChildren: './candidate/candidate.module#JhipsterRecruitmentCandidateModule'
      },
      {
        path: 'candidate-sub-class',
        loadChildren: './candidate-sub-class/candidate-sub-class.module#JhipsterRecruitmentCandidateSubClassModule'
      },
      {
        path: 'candidate-comment',
        loadChildren: './candidate-comment/candidate-comment.module#JhipsterRecruitmentCandidateCommentModule'
      },
      {
        path: 'contact-moment',
        loadChildren: './contact-moment/contact-moment.module#JhipsterRecruitmentContactMomentModule'
      },
      {
        path: 'document',
        loadChildren: './document/document.module#JhipsterRecruitmentDocumentModule'
      },
      {
        path: 'feedback-answer',
        loadChildren: './feedback-answer/feedback-answer.module#JhipsterRecruitmentFeedbackAnswerModule'
      },
      {
        path: 'feedback-question',
        loadChildren: './feedback-question/feedback-question.module#JhipsterRecruitmentFeedbackQuestionModule'
      },
      {
        path: 'vacancy',
        loadChildren: './vacancy/vacancy.module#JhipsterRecruitmentVacancyModule'
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ],
  declarations: [],
  entryComponents: [],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JhipsterRecruitmentEntityModule {}
