import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IFeedbackAnswer } from 'app/shared/model/feedback-answer.model';
import { FeedbackAnswerService } from './feedback-answer.service';

@Component({
  selector: 'jhi-feedback-answer-delete-dialog',
  templateUrl: './feedback-answer-delete-dialog.component.html'
})
export class FeedbackAnswerDeleteDialogComponent {
  feedbackAnswer: IFeedbackAnswer;

  constructor(
    protected feedbackAnswerService: FeedbackAnswerService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.feedbackAnswerService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'feedbackAnswerListModification',
        content: 'Deleted an feedbackAnswer'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-feedback-answer-delete-popup',
  template: ''
})
export class FeedbackAnswerDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ feedbackAnswer }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(FeedbackAnswerDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.feedbackAnswer = feedbackAnswer;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/feedback-answer', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/feedback-answer', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
