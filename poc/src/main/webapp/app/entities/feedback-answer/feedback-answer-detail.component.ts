import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IFeedbackAnswer } from 'app/shared/model/feedback-answer.model';

@Component({
  selector: 'jhi-feedback-answer-detail',
  templateUrl: './feedback-answer-detail.component.html'
})
export class FeedbackAnswerDetailComponent implements OnInit {
  feedbackAnswer: IFeedbackAnswer;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ feedbackAnswer }) => {
      this.feedbackAnswer = feedbackAnswer;
    });
  }

  previousState() {
    window.history.back();
  }
}
