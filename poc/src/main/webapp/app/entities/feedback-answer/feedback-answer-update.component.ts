import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IFeedbackAnswer, FeedbackAnswer } from 'app/shared/model/feedback-answer.model';
import { FeedbackAnswerService } from './feedback-answer.service';
import { IFeedbackQuestion } from 'app/shared/model/feedback-question.model';
import { FeedbackQuestionService } from 'app/entities/feedback-question';
import { ICandidacy } from 'app/shared/model/candidacy.model';
import { CandidacyService } from 'app/entities/candidacy';

@Component({
  selector: 'jhi-feedback-answer-update',
  templateUrl: './feedback-answer-update.component.html'
})
export class FeedbackAnswerUpdateComponent implements OnInit {
  feedbackAnswer: IFeedbackAnswer;
  isSaving: boolean;

  feedbackquestions: IFeedbackQuestion[];

  candidacies: ICandidacy[];

  editForm = this.fb.group({
    id: [],
    answer: [null, [Validators.required]],
    feedbackQuestionId: [],
    candidacyId: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected feedbackAnswerService: FeedbackAnswerService,
    protected feedbackQuestionService: FeedbackQuestionService,
    protected candidacyService: CandidacyService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ feedbackAnswer }) => {
      this.updateForm(feedbackAnswer);
      this.feedbackAnswer = feedbackAnswer;
    });
    this.feedbackQuestionService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IFeedbackQuestion[]>) => mayBeOk.ok),
        map((response: HttpResponse<IFeedbackQuestion[]>) => response.body)
      )
      .subscribe((res: IFeedbackQuestion[]) => (this.feedbackquestions = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.candidacyService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<ICandidacy[]>) => mayBeOk.ok),
        map((response: HttpResponse<ICandidacy[]>) => response.body)
      )
      .subscribe((res: ICandidacy[]) => (this.candidacies = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(feedbackAnswer: IFeedbackAnswer) {
    this.editForm.patchValue({
      id: feedbackAnswer.id,
      answer: feedbackAnswer.answer,
      feedbackQuestionId: feedbackAnswer.feedbackQuestionId,
      candidacyId: feedbackAnswer.candidacyId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const feedbackAnswer = this.createFromForm();
    if (feedbackAnswer.id !== undefined) {
      this.subscribeToSaveResponse(this.feedbackAnswerService.update(feedbackAnswer));
    } else {
      this.subscribeToSaveResponse(this.feedbackAnswerService.create(feedbackAnswer));
    }
  }

  private createFromForm(): IFeedbackAnswer {
    const entity = {
      ...new FeedbackAnswer(),
      id: this.editForm.get(['id']).value,
      answer: this.editForm.get(['answer']).value,
      feedbackQuestionId: this.editForm.get(['feedbackQuestionId']).value,
      candidacyId: this.editForm.get(['candidacyId']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IFeedbackAnswer>>) {
    result.subscribe((res: HttpResponse<IFeedbackAnswer>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackFeedbackQuestionById(index: number, item: IFeedbackQuestion) {
    return item.id;
  }

  trackCandidacyById(index: number, item: ICandidacy) {
    return item.id;
  }
}
