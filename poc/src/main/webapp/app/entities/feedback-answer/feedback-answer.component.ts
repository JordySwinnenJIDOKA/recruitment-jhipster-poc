import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IFeedbackAnswer } from 'app/shared/model/feedback-answer.model';
import { AccountService } from 'app/core';
import { FeedbackAnswerService } from './feedback-answer.service';

@Component({
  selector: 'jhi-feedback-answer',
  templateUrl: './feedback-answer.component.html'
})
export class FeedbackAnswerComponent implements OnInit, OnDestroy {
  feedbackAnswers: IFeedbackAnswer[];
  currentAccount: any;
  eventSubscriber: Subscription;

  constructor(
    protected feedbackAnswerService: FeedbackAnswerService,
    protected jhiAlertService: JhiAlertService,
    protected eventManager: JhiEventManager,
    protected accountService: AccountService
  ) {}

  loadAll() {
    this.feedbackAnswerService
      .query()
      .pipe(
        filter((res: HttpResponse<IFeedbackAnswer[]>) => res.ok),
        map((res: HttpResponse<IFeedbackAnswer[]>) => res.body)
      )
      .subscribe(
        (res: IFeedbackAnswer[]) => {
          this.feedbackAnswers = res;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInFeedbackAnswers();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IFeedbackAnswer) {
    return item.id;
  }

  registerChangeInFeedbackAnswers() {
    this.eventSubscriber = this.eventManager.subscribe('feedbackAnswerListModification', response => this.loadAll());
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
