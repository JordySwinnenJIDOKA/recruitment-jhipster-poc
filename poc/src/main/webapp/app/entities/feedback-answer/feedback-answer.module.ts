import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { JhipsterRecruitmentSharedModule } from 'app/shared';
import {
  FeedbackAnswerComponent,
  FeedbackAnswerDetailComponent,
  FeedbackAnswerUpdateComponent,
  FeedbackAnswerDeletePopupComponent,
  FeedbackAnswerDeleteDialogComponent,
  feedbackAnswerRoute,
  feedbackAnswerPopupRoute
} from './';

const ENTITY_STATES = [...feedbackAnswerRoute, ...feedbackAnswerPopupRoute];

@NgModule({
  imports: [JhipsterRecruitmentSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    FeedbackAnswerComponent,
    FeedbackAnswerDetailComponent,
    FeedbackAnswerUpdateComponent,
    FeedbackAnswerDeleteDialogComponent,
    FeedbackAnswerDeletePopupComponent
  ],
  entryComponents: [
    FeedbackAnswerComponent,
    FeedbackAnswerUpdateComponent,
    FeedbackAnswerDeleteDialogComponent,
    FeedbackAnswerDeletePopupComponent
  ],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JhipsterRecruitmentFeedbackAnswerModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
