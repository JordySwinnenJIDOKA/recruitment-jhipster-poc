import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { FeedbackAnswer } from 'app/shared/model/feedback-answer.model';
import { FeedbackAnswerService } from './feedback-answer.service';
import { FeedbackAnswerComponent } from './feedback-answer.component';
import { FeedbackAnswerDetailComponent } from './feedback-answer-detail.component';
import { FeedbackAnswerUpdateComponent } from './feedback-answer-update.component';
import { FeedbackAnswerDeletePopupComponent } from './feedback-answer-delete-dialog.component';
import { IFeedbackAnswer } from 'app/shared/model/feedback-answer.model';

@Injectable({ providedIn: 'root' })
export class FeedbackAnswerResolve implements Resolve<IFeedbackAnswer> {
  constructor(private service: FeedbackAnswerService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IFeedbackAnswer> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<FeedbackAnswer>) => response.ok),
        map((feedbackAnswer: HttpResponse<FeedbackAnswer>) => feedbackAnswer.body)
      );
    }
    return of(new FeedbackAnswer());
  }
}

export const feedbackAnswerRoute: Routes = [
  {
    path: '',
    component: FeedbackAnswerComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'jhipsterRecruitmentApp.feedbackAnswer.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: FeedbackAnswerDetailComponent,
    resolve: {
      feedbackAnswer: FeedbackAnswerResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'jhipsterRecruitmentApp.feedbackAnswer.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: FeedbackAnswerUpdateComponent,
    resolve: {
      feedbackAnswer: FeedbackAnswerResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'jhipsterRecruitmentApp.feedbackAnswer.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: FeedbackAnswerUpdateComponent,
    resolve: {
      feedbackAnswer: FeedbackAnswerResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'jhipsterRecruitmentApp.feedbackAnswer.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const feedbackAnswerPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: FeedbackAnswerDeletePopupComponent,
    resolve: {
      feedbackAnswer: FeedbackAnswerResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'jhipsterRecruitmentApp.feedbackAnswer.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
