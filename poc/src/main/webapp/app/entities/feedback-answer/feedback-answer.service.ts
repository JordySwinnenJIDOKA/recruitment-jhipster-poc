import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IFeedbackAnswer } from 'app/shared/model/feedback-answer.model';

type EntityResponseType = HttpResponse<IFeedbackAnswer>;
type EntityArrayResponseType = HttpResponse<IFeedbackAnswer[]>;

@Injectable({ providedIn: 'root' })
export class FeedbackAnswerService {
  public resourceUrl = SERVER_API_URL + 'api/feedback-answers';

  constructor(protected http: HttpClient) {}

  create(feedbackAnswer: IFeedbackAnswer): Observable<EntityResponseType> {
    return this.http.post<IFeedbackAnswer>(this.resourceUrl, feedbackAnswer, { observe: 'response' });
  }

  update(feedbackAnswer: IFeedbackAnswer): Observable<EntityResponseType> {
    return this.http.put<IFeedbackAnswer>(this.resourceUrl, feedbackAnswer, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IFeedbackAnswer>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IFeedbackAnswer[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
