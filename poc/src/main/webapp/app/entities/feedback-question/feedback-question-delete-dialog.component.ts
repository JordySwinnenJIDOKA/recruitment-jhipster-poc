import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IFeedbackQuestion } from 'app/shared/model/feedback-question.model';
import { FeedbackQuestionService } from './feedback-question.service';

@Component({
  selector: 'jhi-feedback-question-delete-dialog',
  templateUrl: './feedback-question-delete-dialog.component.html'
})
export class FeedbackQuestionDeleteDialogComponent {
  feedbackQuestion: IFeedbackQuestion;

  constructor(
    protected feedbackQuestionService: FeedbackQuestionService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.feedbackQuestionService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'feedbackQuestionListModification',
        content: 'Deleted an feedbackQuestion'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-feedback-question-delete-popup',
  template: ''
})
export class FeedbackQuestionDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ feedbackQuestion }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(FeedbackQuestionDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.feedbackQuestion = feedbackQuestion;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/feedback-question', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/feedback-question', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
