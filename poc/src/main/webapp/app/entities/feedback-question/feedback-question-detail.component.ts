import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IFeedbackQuestion } from 'app/shared/model/feedback-question.model';

@Component({
  selector: 'jhi-feedback-question-detail',
  templateUrl: './feedback-question-detail.component.html'
})
export class FeedbackQuestionDetailComponent implements OnInit {
  feedbackQuestion: IFeedbackQuestion;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ feedbackQuestion }) => {
      this.feedbackQuestion = feedbackQuestion;
    });
  }

  previousState() {
    window.history.back();
  }
}
