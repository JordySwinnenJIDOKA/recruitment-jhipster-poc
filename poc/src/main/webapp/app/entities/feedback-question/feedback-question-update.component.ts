import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { IFeedbackQuestion, FeedbackQuestion } from 'app/shared/model/feedback-question.model';
import { FeedbackQuestionService } from './feedback-question.service';

@Component({
  selector: 'jhi-feedback-question-update',
  templateUrl: './feedback-question-update.component.html'
})
export class FeedbackQuestionUpdateComponent implements OnInit {
  feedbackQuestion: IFeedbackQuestion;
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    question: [null, [Validators.required]]
  });

  constructor(
    protected feedbackQuestionService: FeedbackQuestionService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ feedbackQuestion }) => {
      this.updateForm(feedbackQuestion);
      this.feedbackQuestion = feedbackQuestion;
    });
  }

  updateForm(feedbackQuestion: IFeedbackQuestion) {
    this.editForm.patchValue({
      id: feedbackQuestion.id,
      question: feedbackQuestion.question
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const feedbackQuestion = this.createFromForm();
    if (feedbackQuestion.id !== undefined) {
      this.subscribeToSaveResponse(this.feedbackQuestionService.update(feedbackQuestion));
    } else {
      this.subscribeToSaveResponse(this.feedbackQuestionService.create(feedbackQuestion));
    }
  }

  private createFromForm(): IFeedbackQuestion {
    const entity = {
      ...new FeedbackQuestion(),
      id: this.editForm.get(['id']).value,
      question: this.editForm.get(['question']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IFeedbackQuestion>>) {
    result.subscribe((res: HttpResponse<IFeedbackQuestion>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
