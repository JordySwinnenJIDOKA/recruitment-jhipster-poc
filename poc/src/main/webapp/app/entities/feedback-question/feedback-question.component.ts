import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IFeedbackQuestion } from 'app/shared/model/feedback-question.model';
import { AccountService } from 'app/core';
import { FeedbackQuestionService } from './feedback-question.service';

@Component({
  selector: 'jhi-feedback-question',
  templateUrl: './feedback-question.component.html'
})
export class FeedbackQuestionComponent implements OnInit, OnDestroy {
  feedbackQuestions: IFeedbackQuestion[];
  currentAccount: any;
  eventSubscriber: Subscription;

  constructor(
    protected feedbackQuestionService: FeedbackQuestionService,
    protected jhiAlertService: JhiAlertService,
    protected eventManager: JhiEventManager,
    protected accountService: AccountService
  ) {}

  loadAll() {
    this.feedbackQuestionService
      .query()
      .pipe(
        filter((res: HttpResponse<IFeedbackQuestion[]>) => res.ok),
        map((res: HttpResponse<IFeedbackQuestion[]>) => res.body)
      )
      .subscribe(
        (res: IFeedbackQuestion[]) => {
          this.feedbackQuestions = res;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInFeedbackQuestions();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IFeedbackQuestion) {
    return item.id;
  }

  registerChangeInFeedbackQuestions() {
    this.eventSubscriber = this.eventManager.subscribe('feedbackQuestionListModification', response => this.loadAll());
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
