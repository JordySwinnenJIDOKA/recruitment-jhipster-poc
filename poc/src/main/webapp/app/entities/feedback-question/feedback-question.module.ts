import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { JhipsterRecruitmentSharedModule } from 'app/shared';
import {
  FeedbackQuestionComponent,
  FeedbackQuestionDetailComponent,
  FeedbackQuestionUpdateComponent,
  FeedbackQuestionDeletePopupComponent,
  FeedbackQuestionDeleteDialogComponent,
  feedbackQuestionRoute,
  feedbackQuestionPopupRoute
} from './';

const ENTITY_STATES = [...feedbackQuestionRoute, ...feedbackQuestionPopupRoute];

@NgModule({
  imports: [JhipsterRecruitmentSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    FeedbackQuestionComponent,
    FeedbackQuestionDetailComponent,
    FeedbackQuestionUpdateComponent,
    FeedbackQuestionDeleteDialogComponent,
    FeedbackQuestionDeletePopupComponent
  ],
  entryComponents: [
    FeedbackQuestionComponent,
    FeedbackQuestionUpdateComponent,
    FeedbackQuestionDeleteDialogComponent,
    FeedbackQuestionDeletePopupComponent
  ],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JhipsterRecruitmentFeedbackQuestionModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
