import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { FeedbackQuestion } from 'app/shared/model/feedback-question.model';
import { FeedbackQuestionService } from './feedback-question.service';
import { FeedbackQuestionComponent } from './feedback-question.component';
import { FeedbackQuestionDetailComponent } from './feedback-question-detail.component';
import { FeedbackQuestionUpdateComponent } from './feedback-question-update.component';
import { FeedbackQuestionDeletePopupComponent } from './feedback-question-delete-dialog.component';
import { IFeedbackQuestion } from 'app/shared/model/feedback-question.model';

@Injectable({ providedIn: 'root' })
export class FeedbackQuestionResolve implements Resolve<IFeedbackQuestion> {
  constructor(private service: FeedbackQuestionService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IFeedbackQuestion> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<FeedbackQuestion>) => response.ok),
        map((feedbackQuestion: HttpResponse<FeedbackQuestion>) => feedbackQuestion.body)
      );
    }
    return of(new FeedbackQuestion());
  }
}

export const feedbackQuestionRoute: Routes = [
  {
    path: '',
    component: FeedbackQuestionComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'jhipsterRecruitmentApp.feedbackQuestion.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: FeedbackQuestionDetailComponent,
    resolve: {
      feedbackQuestion: FeedbackQuestionResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'jhipsterRecruitmentApp.feedbackQuestion.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: FeedbackQuestionUpdateComponent,
    resolve: {
      feedbackQuestion: FeedbackQuestionResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'jhipsterRecruitmentApp.feedbackQuestion.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: FeedbackQuestionUpdateComponent,
    resolve: {
      feedbackQuestion: FeedbackQuestionResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'jhipsterRecruitmentApp.feedbackQuestion.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const feedbackQuestionPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: FeedbackQuestionDeletePopupComponent,
    resolve: {
      feedbackQuestion: FeedbackQuestionResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'jhipsterRecruitmentApp.feedbackQuestion.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
