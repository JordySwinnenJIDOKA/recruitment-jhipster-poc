import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IFeedbackQuestion } from 'app/shared/model/feedback-question.model';

type EntityResponseType = HttpResponse<IFeedbackQuestion>;
type EntityArrayResponseType = HttpResponse<IFeedbackQuestion[]>;

@Injectable({ providedIn: 'root' })
export class FeedbackQuestionService {
  public resourceUrl = SERVER_API_URL + 'api/feedback-questions';

  constructor(protected http: HttpClient) {}

  create(feedbackQuestion: IFeedbackQuestion): Observable<EntityResponseType> {
    return this.http.post<IFeedbackQuestion>(this.resourceUrl, feedbackQuestion, { observe: 'response' });
  }

  update(feedbackQuestion: IFeedbackQuestion): Observable<EntityResponseType> {
    return this.http.put<IFeedbackQuestion>(this.resourceUrl, feedbackQuestion, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IFeedbackQuestion>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IFeedbackQuestion[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
