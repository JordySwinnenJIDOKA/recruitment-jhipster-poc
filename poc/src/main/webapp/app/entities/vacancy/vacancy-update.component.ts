import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { IVacancy, Vacancy } from 'app/shared/model/vacancy.model';
import { VacancyService } from './vacancy.service';

@Component({
  selector: 'jhi-vacancy-update',
  templateUrl: './vacancy-update.component.html'
})
export class VacancyUpdateComponent implements OnInit {
  vacancy: IVacancy;
  isSaving: boolean;
  creationDateDp: any;

  editForm = this.fb.group({
    id: [],
    code: [null, [Validators.required]],
    description: [null, [Validators.required]],
    creationDate: [null, [Validators.required]],
    vacancyStatus: [],
    characteristics: [],
    skills: [],
    dailyRoutine: [],
    location: [],
    candidateTypes: []
  });

  constructor(protected vacancyService: VacancyService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ vacancy }) => {
      this.updateForm(vacancy);
      this.vacancy = vacancy;
    });
  }

  updateForm(vacancy: IVacancy) {
    this.editForm.patchValue({
      id: vacancy.id,
      code: vacancy.code,
      description: vacancy.description,
      creationDate: vacancy.creationDate,
      vacancyStatus: vacancy.vacancyStatus,
      characteristics: vacancy.characteristics,
      skills: vacancy.skills,
      dailyRoutine: vacancy.dailyRoutine,
      location: vacancy.location,
      candidateTypes: vacancy.candidateTypes
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const vacancy = this.createFromForm();
    if (vacancy.id !== undefined) {
      this.subscribeToSaveResponse(this.vacancyService.update(vacancy));
    } else {
      this.subscribeToSaveResponse(this.vacancyService.create(vacancy));
    }
  }

  private createFromForm(): IVacancy {
    const entity = {
      ...new Vacancy(),
      id: this.editForm.get(['id']).value,
      code: this.editForm.get(['code']).value,
      description: this.editForm.get(['description']).value,
      creationDate: this.editForm.get(['creationDate']).value,
      vacancyStatus: this.editForm.get(['vacancyStatus']).value,
      characteristics: this.editForm.get(['characteristics']).value,
      skills: this.editForm.get(['skills']).value,
      dailyRoutine: this.editForm.get(['dailyRoutine']).value,
      location: this.editForm.get(['location']).value,
      candidateTypes: this.editForm.get(['candidateTypes']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IVacancy>>) {
    result.subscribe((res: HttpResponse<IVacancy>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
