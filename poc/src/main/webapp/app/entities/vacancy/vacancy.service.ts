import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IVacancy } from 'app/shared/model/vacancy.model';

type EntityResponseType = HttpResponse<IVacancy>;
type EntityArrayResponseType = HttpResponse<IVacancy[]>;

@Injectable({ providedIn: 'root' })
export class VacancyService {
  public resourceUrl = SERVER_API_URL + 'api/vacancies';

  constructor(protected http: HttpClient) {}

  create(vacancy: IVacancy): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(vacancy);
    return this.http
      .post<IVacancy>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(vacancy: IVacancy): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(vacancy);
    return this.http
      .put<IVacancy>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IVacancy>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IVacancy[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(vacancy: IVacancy): IVacancy {
    const copy: IVacancy = Object.assign({}, vacancy, {
      creationDate: vacancy.creationDate != null && vacancy.creationDate.isValid() ? vacancy.creationDate.format(DATE_FORMAT) : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.creationDate = res.body.creationDate != null ? moment(res.body.creationDate) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((vacancy: IVacancy) => {
        vacancy.creationDate = vacancy.creationDate != null ? moment(vacancy.creationDate) : null;
      });
    }
    return res;
  }
}
