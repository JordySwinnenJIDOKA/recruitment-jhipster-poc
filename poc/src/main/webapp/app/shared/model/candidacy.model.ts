export const enum CandidacyStatus {
  CV_SCREENING_HR = 'CV_SCREENING_HR',
  TELEPHONE_INTAKE = 'TELEPHONE_INTAKE',
  REFERENCE_CALL = 'REFERENCE_CALL',
  F2F_SCREENING_HR = 'F2F_SCREENING_HR',
  TECHNICAL_INTERVIEW = 'TECHNICAL_INTERVIEW',
  MT_DISCUSSION = 'MT_DISCUSSION',
  NOT_SELECTED = 'NOT_SELECTED',
  CANDIDATE_WITHDRAWAL = 'CANDIDATE_WITHDRAWAL',
  CONTRACT_OFFER = 'CONTRACT_OFFER',
  HIRED = 'HIRED',
  RECRUITMENT_POOL = 'RECRUITMENT_POOL'
}

export interface ICandidacy {
  id?: number;
  candidacyStatus?: CandidacyStatus;
  motivation?: string;
  candidateId?: number;
  vacancyId?: number;
}

export class Candidacy implements ICandidacy {
  constructor(
    public id?: number,
    public candidacyStatus?: CandidacyStatus,
    public motivation?: string,
    public candidateId?: number,
    public vacancyId?: number
  ) {}
}
