export interface ICandidateComment {
  id?: number;
  context?: string;
  commenter?: string;
  candidateId?: number;
}

export class CandidateComment implements ICandidateComment {
  constructor(public id?: number, public context?: string, public commenter?: string, public candidateId?: number) {}
}
