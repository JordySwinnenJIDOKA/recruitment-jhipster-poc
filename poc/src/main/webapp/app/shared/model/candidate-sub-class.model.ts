export const enum CandidateType {
  FREELANCE = 'FREELANCE',
  INTERN = 'INTERN',
  PAYROLL = 'PAYROLL',
  STUDENT = 'STUDENT'
}

export interface ICandidateSubClass {
  id?: number;
  candidateType?: CandidateType;
}

export class CandidateSubClass implements ICandidateSubClass {
  constructor(public id?: number, public candidateType?: CandidateType) {}
}
