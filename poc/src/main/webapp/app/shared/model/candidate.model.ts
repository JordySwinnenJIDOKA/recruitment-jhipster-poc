import { ICandidacy } from 'app/shared/model/candidacy.model';
import { ICandidateComment } from 'app/shared/model/candidate-comment.model';
import { IContactMoment } from 'app/shared/model/contact-moment.model';

export const enum CandidateStatus {
  NEW_APPLICANT = 'NEW_APPLICANT',
  SCREENING_ONGOING = 'SCREENING_ONGOING',
  HIRED = 'HIRED',
  RECRUITMENT_POOL = 'RECRUITMENT_POOL'
}

export const enum CandidateFuntion {
  JAVA_DEVELOPER = 'JAVA_DEVELOPER',
  SALES_MANAGER = 'SALES_MANAGER',
  BUSINESS_ANALYST = 'BUSINESS_ANALYST',
  DEVOPS_ENGINEER = 'DEVOPS_ENGINEER'
}

export const enum CandidateTrack {
  BUSINESS = 'BUSINESS',
  TECHNICAL = 'TECHNICAL',
  GENERAL = 'GENERAL'
}

export const enum RecruitmentChannel {
  WEBSITE = 'WEBSITE',
  SOCIAL_MEDIA = 'SOCIAL_MEDIA',
  JOB_FAIRS = 'JOB_FAIRS',
  INTERNSHIP = 'INTERNSHIP',
  REFERRAL = 'REFERRAL',
  JOB_SITE = 'JOB_SITE',
  OTHER = 'OTHER'
}

export interface ICandidate {
  id?: number;
  name?: string;
  phone?: string;
  email?: string;
  candidateStatus?: CandidateStatus;
  candidateFunction?: CandidateFuntion;
  dateAvailable?: string;
  imageContentContentType?: string;
  imageContent?: any;
  candidateTrack?: CandidateTrack;
  recruitmentChannel?: RecruitmentChannel;
  linkedInProfilePath?: string;
  documentId?: number;
  subClassId?: number;
  candidacies?: ICandidacy[];
  candidateComments?: ICandidateComment[];
  contactMoments?: IContactMoment[];
}

export class Candidate implements ICandidate {
  constructor(
    public id?: number,
    public name?: string,
    public phone?: string,
    public email?: string,
    public candidateStatus?: CandidateStatus,
    public candidateFunction?: CandidateFuntion,
    public dateAvailable?: string,
    public imageContentContentType?: string,
    public imageContent?: any,
    public candidateTrack?: CandidateTrack,
    public recruitmentChannel?: RecruitmentChannel,
    public linkedInProfilePath?: string,
    public documentId?: number,
    public subClassId?: number,
    public candidacies?: ICandidacy[],
    public candidateComments?: ICandidateComment[],
    public contactMoments?: IContactMoment[]
  ) {}
}
