import { Moment } from 'moment';

export const enum ContactType {
  PHONE_CALL = 'PHONE_CALL',
  INTERVIEW = 'INTERVIEW',
  MAIL = 'MAIL',
  VIDEO_CALL = 'VIDEO_CALL'
}

export const enum ContactContext {
  INTRODUCTION = 'INTRODUCTION',
  FIRST_MEETING = 'FIRST_MEETING',
  SECOND_MEETING = 'SECOND_MEETING',
  CONTRACT_NEGOTIATION = 'CONTRACT_NEGOTIATION',
  TECHNICAL = 'TECHNICAL'
}

export interface IContactMoment {
  id?: number;
  contactType?: ContactType;
  contactDate?: Moment;
  contactContext?: ContactContext;
  interviewer?: string;
  location?: string;
  candidacyId?: number;
  candidateId?: number;
}

export class ContactMoment implements IContactMoment {
  constructor(
    public id?: number,
    public contactType?: ContactType,
    public contactDate?: Moment,
    public contactContext?: ContactContext,
    public interviewer?: string,
    public location?: string,
    public candidacyId?: number,
    public candidateId?: number
  ) {}
}
