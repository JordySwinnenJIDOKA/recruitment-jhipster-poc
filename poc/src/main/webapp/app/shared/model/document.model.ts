export interface IDocument {
  id?: number;
  name?: string;
  filename?: string;
  contentContentType?: string;
  content?: any;
}

export class Document implements IDocument {
  constructor(
    public id?: number,
    public name?: string,
    public filename?: string,
    public contentContentType?: string,
    public content?: any
  ) {}
}
