export interface IFeedbackAnswer {
  id?: number;
  answer?: string;
  feedbackQuestionId?: number;
  candidacyId?: number;
}

export class FeedbackAnswer implements IFeedbackAnswer {
  constructor(public id?: number, public answer?: string, public feedbackQuestionId?: number, public candidacyId?: number) {}
}
