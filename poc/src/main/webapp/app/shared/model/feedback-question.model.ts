export interface IFeedbackQuestion {
  id?: number;
  question?: string;
}

export class FeedbackQuestion implements IFeedbackQuestion {
  constructor(public id?: number, public question?: string) {}
}
