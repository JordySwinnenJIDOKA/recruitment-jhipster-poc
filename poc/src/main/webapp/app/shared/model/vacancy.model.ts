import { Moment } from 'moment';
import { ICandidacy } from 'app/shared/model/candidacy.model';

export const enum VacancyStatus {
  OPEN = 'OPEN',
  ON_HOLD = 'ON_HOLD',
  CLOSED = 'CLOSED'
}

export const enum CandidateType {
  FREELANCE = 'FREELANCE',
  INTERN = 'INTERN',
  PAYROLL = 'PAYROLL',
  STUDENT = 'STUDENT'
}

export interface IVacancy {
  id?: number;
  code?: string;
  description?: string;
  creationDate?: Moment;
  vacancyStatus?: VacancyStatus;
  characteristics?: string;
  skills?: string;
  dailyRoutine?: string;
  location?: string;
  candidateTypes?: CandidateType;
  candidacies?: ICandidacy[];
}

export class Vacancy implements IVacancy {
  constructor(
    public id?: number,
    public code?: string,
    public description?: string,
    public creationDate?: Moment,
    public vacancyStatus?: VacancyStatus,
    public characteristics?: string,
    public skills?: string,
    public dailyRoutine?: string,
    public location?: string,
    public candidateTypes?: CandidateType,
    public candidacies?: ICandidacy[]
  ) {}
}
