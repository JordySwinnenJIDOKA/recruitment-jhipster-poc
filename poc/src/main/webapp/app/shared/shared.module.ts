import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { JhipsterRecruitmentSharedLibsModule, JhipsterRecruitmentSharedCommonModule, HasAnyAuthorityDirective } from './';

@NgModule({
  imports: [JhipsterRecruitmentSharedLibsModule, JhipsterRecruitmentSharedCommonModule],
  declarations: [HasAnyAuthorityDirective],
  exports: [JhipsterRecruitmentSharedCommonModule, HasAnyAuthorityDirective],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JhipsterRecruitmentSharedModule {
  static forRoot() {
    return {
      ngModule: JhipsterRecruitmentSharedModule
    };
  }
}
