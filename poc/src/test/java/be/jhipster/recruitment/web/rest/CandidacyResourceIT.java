package be.jhipster.recruitment.web.rest;

import be.jhipster.recruitment.JhipsterRecruitmentApp;
import be.jhipster.recruitment.config.TestSecurityConfiguration;
import be.jhipster.recruitment.domain.Candidacy;
import be.jhipster.recruitment.repository.CandidacyRepository;
import be.jhipster.recruitment.service.CandidacyService;
import be.jhipster.recruitment.service.dto.CandidacyDTO;
import be.jhipster.recruitment.service.mapper.CandidacyMapper;
import be.jhipster.recruitment.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static be.jhipster.recruitment.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import be.jhipster.recruitment.domain.enumeration.CandidacyStatus;
/**
 * Integration tests for the {@Link CandidacyResource} REST controller.
 */
@SpringBootTest(classes = {JhipsterRecruitmentApp.class, TestSecurityConfiguration.class})
public class CandidacyResourceIT {

    private static final CandidacyStatus DEFAULT_CANDIDACY_STATUS = CandidacyStatus.CV_SCREENING_HR;
    private static final CandidacyStatus UPDATED_CANDIDACY_STATUS = CandidacyStatus.TELEPHONE_INTAKE;

    private static final String DEFAULT_MOTIVATION = "AAAAAAAAAA";
    private static final String UPDATED_MOTIVATION = "BBBBBBBBBB";

    @Autowired
    private CandidacyRepository candidacyRepository;

    @Autowired
    private CandidacyMapper candidacyMapper;

    @Autowired
    private CandidacyService candidacyService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restCandidacyMockMvc;

    private Candidacy candidacy;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final CandidacyResource candidacyResource = new CandidacyResource(candidacyService);
        this.restCandidacyMockMvc = MockMvcBuilders.standaloneSetup(candidacyResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Candidacy createEntity(EntityManager em) {
        Candidacy candidacy = new Candidacy()
            .candidacyStatus(DEFAULT_CANDIDACY_STATUS)
            .motivation(DEFAULT_MOTIVATION);
        return candidacy;
    }

    @BeforeEach
    public void initTest() {
        candidacy = createEntity(em);
    }

    @Test
    @Transactional
    public void createCandidacy() throws Exception {
        int databaseSizeBeforeCreate = candidacyRepository.findAll().size();

        // Create the Candidacy
        CandidacyDTO candidacyDTO = candidacyMapper.toDto(candidacy);
        restCandidacyMockMvc.perform(post("/api/candidacies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(candidacyDTO)))
            .andExpect(status().isCreated());

        // Validate the Candidacy in the database
        List<Candidacy> candidacyList = candidacyRepository.findAll();
        assertThat(candidacyList).hasSize(databaseSizeBeforeCreate + 1);
        Candidacy testCandidacy = candidacyList.get(candidacyList.size() - 1);
        assertThat(testCandidacy.getCandidacyStatus()).isEqualTo(DEFAULT_CANDIDACY_STATUS);
        assertThat(testCandidacy.getMotivation()).isEqualTo(DEFAULT_MOTIVATION);
    }

    @Test
    @Transactional
    public void createCandidacyWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = candidacyRepository.findAll().size();

        // Create the Candidacy with an existing ID
        candidacy.setId(1L);
        CandidacyDTO candidacyDTO = candidacyMapper.toDto(candidacy);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCandidacyMockMvc.perform(post("/api/candidacies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(candidacyDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Candidacy in the database
        List<Candidacy> candidacyList = candidacyRepository.findAll();
        assertThat(candidacyList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkCandidacyStatusIsRequired() throws Exception {
        int databaseSizeBeforeTest = candidacyRepository.findAll().size();
        // set the field null
        candidacy.setCandidacyStatus(null);

        // Create the Candidacy, which fails.
        CandidacyDTO candidacyDTO = candidacyMapper.toDto(candidacy);

        restCandidacyMockMvc.perform(post("/api/candidacies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(candidacyDTO)))
            .andExpect(status().isBadRequest());

        List<Candidacy> candidacyList = candidacyRepository.findAll();
        assertThat(candidacyList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkMotivationIsRequired() throws Exception {
        int databaseSizeBeforeTest = candidacyRepository.findAll().size();
        // set the field null
        candidacy.setMotivation(null);

        // Create the Candidacy, which fails.
        CandidacyDTO candidacyDTO = candidacyMapper.toDto(candidacy);

        restCandidacyMockMvc.perform(post("/api/candidacies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(candidacyDTO)))
            .andExpect(status().isBadRequest());

        List<Candidacy> candidacyList = candidacyRepository.findAll();
        assertThat(candidacyList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllCandidacies() throws Exception {
        // Initialize the database
        candidacyRepository.saveAndFlush(candidacy);

        // Get all the candidacyList
        restCandidacyMockMvc.perform(get("/api/candidacies?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(candidacy.getId().intValue())))
            .andExpect(jsonPath("$.[*].candidacyStatus").value(hasItem(DEFAULT_CANDIDACY_STATUS.toString())))
            .andExpect(jsonPath("$.[*].motivation").value(hasItem(DEFAULT_MOTIVATION.toString())));
    }
    
    @Test
    @Transactional
    public void getCandidacy() throws Exception {
        // Initialize the database
        candidacyRepository.saveAndFlush(candidacy);

        // Get the candidacy
        restCandidacyMockMvc.perform(get("/api/candidacies/{id}", candidacy.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(candidacy.getId().intValue()))
            .andExpect(jsonPath("$.candidacyStatus").value(DEFAULT_CANDIDACY_STATUS.toString()))
            .andExpect(jsonPath("$.motivation").value(DEFAULT_MOTIVATION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingCandidacy() throws Exception {
        // Get the candidacy
        restCandidacyMockMvc.perform(get("/api/candidacies/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCandidacy() throws Exception {
        // Initialize the database
        candidacyRepository.saveAndFlush(candidacy);

        int databaseSizeBeforeUpdate = candidacyRepository.findAll().size();

        // Update the candidacy
        Candidacy updatedCandidacy = candidacyRepository.findById(candidacy.getId()).get();
        // Disconnect from session so that the updates on updatedCandidacy are not directly saved in db
        em.detach(updatedCandidacy);
        updatedCandidacy
            .candidacyStatus(UPDATED_CANDIDACY_STATUS)
            .motivation(UPDATED_MOTIVATION);
        CandidacyDTO candidacyDTO = candidacyMapper.toDto(updatedCandidacy);

        restCandidacyMockMvc.perform(put("/api/candidacies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(candidacyDTO)))
            .andExpect(status().isOk());

        // Validate the Candidacy in the database
        List<Candidacy> candidacyList = candidacyRepository.findAll();
        assertThat(candidacyList).hasSize(databaseSizeBeforeUpdate);
        Candidacy testCandidacy = candidacyList.get(candidacyList.size() - 1);
        assertThat(testCandidacy.getCandidacyStatus()).isEqualTo(UPDATED_CANDIDACY_STATUS);
        assertThat(testCandidacy.getMotivation()).isEqualTo(UPDATED_MOTIVATION);
    }

    @Test
    @Transactional
    public void updateNonExistingCandidacy() throws Exception {
        int databaseSizeBeforeUpdate = candidacyRepository.findAll().size();

        // Create the Candidacy
        CandidacyDTO candidacyDTO = candidacyMapper.toDto(candidacy);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCandidacyMockMvc.perform(put("/api/candidacies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(candidacyDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Candidacy in the database
        List<Candidacy> candidacyList = candidacyRepository.findAll();
        assertThat(candidacyList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCandidacy() throws Exception {
        // Initialize the database
        candidacyRepository.saveAndFlush(candidacy);

        int databaseSizeBeforeDelete = candidacyRepository.findAll().size();

        // Delete the candidacy
        restCandidacyMockMvc.perform(delete("/api/candidacies/{id}", candidacy.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<Candidacy> candidacyList = candidacyRepository.findAll();
        assertThat(candidacyList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Candidacy.class);
        Candidacy candidacy1 = new Candidacy();
        candidacy1.setId(1L);
        Candidacy candidacy2 = new Candidacy();
        candidacy2.setId(candidacy1.getId());
        assertThat(candidacy1).isEqualTo(candidacy2);
        candidacy2.setId(2L);
        assertThat(candidacy1).isNotEqualTo(candidacy2);
        candidacy1.setId(null);
        assertThat(candidacy1).isNotEqualTo(candidacy2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CandidacyDTO.class);
        CandidacyDTO candidacyDTO1 = new CandidacyDTO();
        candidacyDTO1.setId(1L);
        CandidacyDTO candidacyDTO2 = new CandidacyDTO();
        assertThat(candidacyDTO1).isNotEqualTo(candidacyDTO2);
        candidacyDTO2.setId(candidacyDTO1.getId());
        assertThat(candidacyDTO1).isEqualTo(candidacyDTO2);
        candidacyDTO2.setId(2L);
        assertThat(candidacyDTO1).isNotEqualTo(candidacyDTO2);
        candidacyDTO1.setId(null);
        assertThat(candidacyDTO1).isNotEqualTo(candidacyDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(candidacyMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(candidacyMapper.fromId(null)).isNull();
    }
}
