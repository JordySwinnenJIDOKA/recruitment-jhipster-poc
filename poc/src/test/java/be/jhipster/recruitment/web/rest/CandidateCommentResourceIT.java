package be.jhipster.recruitment.web.rest;

import be.jhipster.recruitment.JhipsterRecruitmentApp;
import be.jhipster.recruitment.config.TestSecurityConfiguration;
import be.jhipster.recruitment.domain.CandidateComment;
import be.jhipster.recruitment.repository.CandidateCommentRepository;
import be.jhipster.recruitment.service.CandidateCommentService;
import be.jhipster.recruitment.service.dto.CandidateCommentDTO;
import be.jhipster.recruitment.service.mapper.CandidateCommentMapper;
import be.jhipster.recruitment.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static be.jhipster.recruitment.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link CandidateCommentResource} REST controller.
 */
@SpringBootTest(classes = {JhipsterRecruitmentApp.class, TestSecurityConfiguration.class})
public class CandidateCommentResourceIT {

    private static final String DEFAULT_CONTEXT = "AAAAAAAAAA";
    private static final String UPDATED_CONTEXT = "BBBBBBBBBB";

    private static final String DEFAULT_COMMENTER = "AAAAAAAAAA";
    private static final String UPDATED_COMMENTER = "BBBBBBBBBB";

    @Autowired
    private CandidateCommentRepository candidateCommentRepository;

    @Autowired
    private CandidateCommentMapper candidateCommentMapper;

    @Autowired
    private CandidateCommentService candidateCommentService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restCandidateCommentMockMvc;

    private CandidateComment candidateComment;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final CandidateCommentResource candidateCommentResource = new CandidateCommentResource(candidateCommentService);
        this.restCandidateCommentMockMvc = MockMvcBuilders.standaloneSetup(candidateCommentResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CandidateComment createEntity(EntityManager em) {
        CandidateComment candidateComment = new CandidateComment()
            .context(DEFAULT_CONTEXT)
            .commenter(DEFAULT_COMMENTER);
        return candidateComment;
    }

    @BeforeEach
    public void initTest() {
        candidateComment = createEntity(em);
    }

    @Test
    @Transactional
    public void createCandidateComment() throws Exception {
        int databaseSizeBeforeCreate = candidateCommentRepository.findAll().size();

        // Create the CandidateComment
        CandidateCommentDTO candidateCommentDTO = candidateCommentMapper.toDto(candidateComment);
        restCandidateCommentMockMvc.perform(post("/api/candidate-comments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(candidateCommentDTO)))
            .andExpect(status().isCreated());

        // Validate the CandidateComment in the database
        List<CandidateComment> candidateCommentList = candidateCommentRepository.findAll();
        assertThat(candidateCommentList).hasSize(databaseSizeBeforeCreate + 1);
        CandidateComment testCandidateComment = candidateCommentList.get(candidateCommentList.size() - 1);
        assertThat(testCandidateComment.getContext()).isEqualTo(DEFAULT_CONTEXT);
        assertThat(testCandidateComment.getCommenter()).isEqualTo(DEFAULT_COMMENTER);
    }

    @Test
    @Transactional
    public void createCandidateCommentWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = candidateCommentRepository.findAll().size();

        // Create the CandidateComment with an existing ID
        candidateComment.setId(1L);
        CandidateCommentDTO candidateCommentDTO = candidateCommentMapper.toDto(candidateComment);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCandidateCommentMockMvc.perform(post("/api/candidate-comments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(candidateCommentDTO)))
            .andExpect(status().isBadRequest());

        // Validate the CandidateComment in the database
        List<CandidateComment> candidateCommentList = candidateCommentRepository.findAll();
        assertThat(candidateCommentList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkContextIsRequired() throws Exception {
        int databaseSizeBeforeTest = candidateCommentRepository.findAll().size();
        // set the field null
        candidateComment.setContext(null);

        // Create the CandidateComment, which fails.
        CandidateCommentDTO candidateCommentDTO = candidateCommentMapper.toDto(candidateComment);

        restCandidateCommentMockMvc.perform(post("/api/candidate-comments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(candidateCommentDTO)))
            .andExpect(status().isBadRequest());

        List<CandidateComment> candidateCommentList = candidateCommentRepository.findAll();
        assertThat(candidateCommentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCommenterIsRequired() throws Exception {
        int databaseSizeBeforeTest = candidateCommentRepository.findAll().size();
        // set the field null
        candidateComment.setCommenter(null);

        // Create the CandidateComment, which fails.
        CandidateCommentDTO candidateCommentDTO = candidateCommentMapper.toDto(candidateComment);

        restCandidateCommentMockMvc.perform(post("/api/candidate-comments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(candidateCommentDTO)))
            .andExpect(status().isBadRequest());

        List<CandidateComment> candidateCommentList = candidateCommentRepository.findAll();
        assertThat(candidateCommentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllCandidateComments() throws Exception {
        // Initialize the database
        candidateCommentRepository.saveAndFlush(candidateComment);

        // Get all the candidateCommentList
        restCandidateCommentMockMvc.perform(get("/api/candidate-comments?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(candidateComment.getId().intValue())))
            .andExpect(jsonPath("$.[*].context").value(hasItem(DEFAULT_CONTEXT.toString())))
            .andExpect(jsonPath("$.[*].commenter").value(hasItem(DEFAULT_COMMENTER.toString())));
    }
    
    @Test
    @Transactional
    public void getCandidateComment() throws Exception {
        // Initialize the database
        candidateCommentRepository.saveAndFlush(candidateComment);

        // Get the candidateComment
        restCandidateCommentMockMvc.perform(get("/api/candidate-comments/{id}", candidateComment.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(candidateComment.getId().intValue()))
            .andExpect(jsonPath("$.context").value(DEFAULT_CONTEXT.toString()))
            .andExpect(jsonPath("$.commenter").value(DEFAULT_COMMENTER.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingCandidateComment() throws Exception {
        // Get the candidateComment
        restCandidateCommentMockMvc.perform(get("/api/candidate-comments/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCandidateComment() throws Exception {
        // Initialize the database
        candidateCommentRepository.saveAndFlush(candidateComment);

        int databaseSizeBeforeUpdate = candidateCommentRepository.findAll().size();

        // Update the candidateComment
        CandidateComment updatedCandidateComment = candidateCommentRepository.findById(candidateComment.getId()).get();
        // Disconnect from session so that the updates on updatedCandidateComment are not directly saved in db
        em.detach(updatedCandidateComment);
        updatedCandidateComment
            .context(UPDATED_CONTEXT)
            .commenter(UPDATED_COMMENTER);
        CandidateCommentDTO candidateCommentDTO = candidateCommentMapper.toDto(updatedCandidateComment);

        restCandidateCommentMockMvc.perform(put("/api/candidate-comments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(candidateCommentDTO)))
            .andExpect(status().isOk());

        // Validate the CandidateComment in the database
        List<CandidateComment> candidateCommentList = candidateCommentRepository.findAll();
        assertThat(candidateCommentList).hasSize(databaseSizeBeforeUpdate);
        CandidateComment testCandidateComment = candidateCommentList.get(candidateCommentList.size() - 1);
        assertThat(testCandidateComment.getContext()).isEqualTo(UPDATED_CONTEXT);
        assertThat(testCandidateComment.getCommenter()).isEqualTo(UPDATED_COMMENTER);
    }

    @Test
    @Transactional
    public void updateNonExistingCandidateComment() throws Exception {
        int databaseSizeBeforeUpdate = candidateCommentRepository.findAll().size();

        // Create the CandidateComment
        CandidateCommentDTO candidateCommentDTO = candidateCommentMapper.toDto(candidateComment);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCandidateCommentMockMvc.perform(put("/api/candidate-comments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(candidateCommentDTO)))
            .andExpect(status().isBadRequest());

        // Validate the CandidateComment in the database
        List<CandidateComment> candidateCommentList = candidateCommentRepository.findAll();
        assertThat(candidateCommentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCandidateComment() throws Exception {
        // Initialize the database
        candidateCommentRepository.saveAndFlush(candidateComment);

        int databaseSizeBeforeDelete = candidateCommentRepository.findAll().size();

        // Delete the candidateComment
        restCandidateCommentMockMvc.perform(delete("/api/candidate-comments/{id}", candidateComment.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<CandidateComment> candidateCommentList = candidateCommentRepository.findAll();
        assertThat(candidateCommentList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CandidateComment.class);
        CandidateComment candidateComment1 = new CandidateComment();
        candidateComment1.setId(1L);
        CandidateComment candidateComment2 = new CandidateComment();
        candidateComment2.setId(candidateComment1.getId());
        assertThat(candidateComment1).isEqualTo(candidateComment2);
        candidateComment2.setId(2L);
        assertThat(candidateComment1).isNotEqualTo(candidateComment2);
        candidateComment1.setId(null);
        assertThat(candidateComment1).isNotEqualTo(candidateComment2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CandidateCommentDTO.class);
        CandidateCommentDTO candidateCommentDTO1 = new CandidateCommentDTO();
        candidateCommentDTO1.setId(1L);
        CandidateCommentDTO candidateCommentDTO2 = new CandidateCommentDTO();
        assertThat(candidateCommentDTO1).isNotEqualTo(candidateCommentDTO2);
        candidateCommentDTO2.setId(candidateCommentDTO1.getId());
        assertThat(candidateCommentDTO1).isEqualTo(candidateCommentDTO2);
        candidateCommentDTO2.setId(2L);
        assertThat(candidateCommentDTO1).isNotEqualTo(candidateCommentDTO2);
        candidateCommentDTO1.setId(null);
        assertThat(candidateCommentDTO1).isNotEqualTo(candidateCommentDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(candidateCommentMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(candidateCommentMapper.fromId(null)).isNull();
    }
}
