package be.jhipster.recruitment.web.rest;

import be.jhipster.recruitment.JhipsterRecruitmentApp;
import be.jhipster.recruitment.config.TestSecurityConfiguration;
import be.jhipster.recruitment.domain.Candidate;
import be.jhipster.recruitment.repository.CandidateRepository;
import be.jhipster.recruitment.service.CandidateService;
import be.jhipster.recruitment.service.dto.CandidateDTO;
import be.jhipster.recruitment.service.mapper.CandidateMapper;
import be.jhipster.recruitment.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static be.jhipster.recruitment.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import be.jhipster.recruitment.domain.enumeration.CandidateStatus;
import be.jhipster.recruitment.domain.enumeration.CandidateFuntion;
import be.jhipster.recruitment.domain.enumeration.CandidateTrack;
import be.jhipster.recruitment.domain.enumeration.RecruitmentChannel;
/**
 * Integration tests for the {@Link CandidateResource} REST controller.
 */
@SpringBootTest(classes = {JhipsterRecruitmentApp.class, TestSecurityConfiguration.class})
public class CandidateResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_PHONE = "AAAAAAAAAA";
    private static final String UPDATED_PHONE = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final CandidateStatus DEFAULT_CANDIDATE_STATUS = CandidateStatus.NEW_APPLICANT;
    private static final CandidateStatus UPDATED_CANDIDATE_STATUS = CandidateStatus.SCREENING_ONGOING;

    private static final CandidateFuntion DEFAULT_CANDIDATE_FUNCTION = CandidateFuntion.JAVA_DEVELOPER;
    private static final CandidateFuntion UPDATED_CANDIDATE_FUNCTION = CandidateFuntion.SALES_MANAGER;

    private static final String DEFAULT_DATE_AVAILABLE = "AAAAAAAAAA";
    private static final String UPDATED_DATE_AVAILABLE = "BBBBBBBBBB";

    private static final byte[] DEFAULT_IMAGE_CONTENT = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_IMAGE_CONTENT = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_IMAGE_CONTENT_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_IMAGE_CONTENT_CONTENT_TYPE = "image/png";

    private static final CandidateTrack DEFAULT_CANDIDATE_TRACK = CandidateTrack.BUSINESS;
    private static final CandidateTrack UPDATED_CANDIDATE_TRACK = CandidateTrack.TECHNICAL;

    private static final RecruitmentChannel DEFAULT_RECRUITMENT_CHANNEL = RecruitmentChannel.WEBSITE;
    private static final RecruitmentChannel UPDATED_RECRUITMENT_CHANNEL = RecruitmentChannel.SOCIAL_MEDIA;

    private static final String DEFAULT_LINKED_IN_PROFILE_PATH = "AAAAAAAAAA";
    private static final String UPDATED_LINKED_IN_PROFILE_PATH = "BBBBBBBBBB";

    @Autowired
    private CandidateRepository candidateRepository;

    @Autowired
    private CandidateMapper candidateMapper;

    @Autowired
    private CandidateService candidateService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restCandidateMockMvc;

    private Candidate candidate;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final CandidateResource candidateResource = new CandidateResource(candidateService);
        this.restCandidateMockMvc = MockMvcBuilders.standaloneSetup(candidateResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Candidate createEntity(EntityManager em) {
        Candidate candidate = new Candidate()
            .name(DEFAULT_NAME)
            .phone(DEFAULT_PHONE)
            .email(DEFAULT_EMAIL)
            .candidateStatus(DEFAULT_CANDIDATE_STATUS)
            .candidateFunction(DEFAULT_CANDIDATE_FUNCTION)
            .dateAvailable(DEFAULT_DATE_AVAILABLE)
            .imageContent(DEFAULT_IMAGE_CONTENT)
            .imageContentContentType(DEFAULT_IMAGE_CONTENT_CONTENT_TYPE)
            .candidateTrack(DEFAULT_CANDIDATE_TRACK)
            .recruitmentChannel(DEFAULT_RECRUITMENT_CHANNEL)
            .linkedInProfilePath(DEFAULT_LINKED_IN_PROFILE_PATH);
        return candidate;
    }

    @BeforeEach
    public void initTest() {
        candidate = createEntity(em);
    }

    @Test
    @Transactional
    public void createCandidate() throws Exception {
        int databaseSizeBeforeCreate = candidateRepository.findAll().size();

        // Create the Candidate
        CandidateDTO candidateDTO = candidateMapper.toDto(candidate);
        restCandidateMockMvc.perform(post("/api/candidates")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(candidateDTO)))
            .andExpect(status().isCreated());

        // Validate the Candidate in the database
        List<Candidate> candidateList = candidateRepository.findAll();
        assertThat(candidateList).hasSize(databaseSizeBeforeCreate + 1);
        Candidate testCandidate = candidateList.get(candidateList.size() - 1);
        assertThat(testCandidate.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testCandidate.getPhone()).isEqualTo(DEFAULT_PHONE);
        assertThat(testCandidate.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testCandidate.getCandidateStatus()).isEqualTo(DEFAULT_CANDIDATE_STATUS);
        assertThat(testCandidate.getCandidateFunction()).isEqualTo(DEFAULT_CANDIDATE_FUNCTION);
        assertThat(testCandidate.getDateAvailable()).isEqualTo(DEFAULT_DATE_AVAILABLE);
        assertThat(testCandidate.getImageContent()).isEqualTo(DEFAULT_IMAGE_CONTENT);
        assertThat(testCandidate.getImageContentContentType()).isEqualTo(DEFAULT_IMAGE_CONTENT_CONTENT_TYPE);
        assertThat(testCandidate.getCandidateTrack()).isEqualTo(DEFAULT_CANDIDATE_TRACK);
        assertThat(testCandidate.getRecruitmentChannel()).isEqualTo(DEFAULT_RECRUITMENT_CHANNEL);
        assertThat(testCandidate.getLinkedInProfilePath()).isEqualTo(DEFAULT_LINKED_IN_PROFILE_PATH);
    }

    @Test
    @Transactional
    public void createCandidateWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = candidateRepository.findAll().size();

        // Create the Candidate with an existing ID
        candidate.setId(1L);
        CandidateDTO candidateDTO = candidateMapper.toDto(candidate);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCandidateMockMvc.perform(post("/api/candidates")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(candidateDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Candidate in the database
        List<Candidate> candidateList = candidateRepository.findAll();
        assertThat(candidateList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = candidateRepository.findAll().size();
        // set the field null
        candidate.setName(null);

        // Create the Candidate, which fails.
        CandidateDTO candidateDTO = candidateMapper.toDto(candidate);

        restCandidateMockMvc.perform(post("/api/candidates")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(candidateDTO)))
            .andExpect(status().isBadRequest());

        List<Candidate> candidateList = candidateRepository.findAll();
        assertThat(candidateList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPhoneIsRequired() throws Exception {
        int databaseSizeBeforeTest = candidateRepository.findAll().size();
        // set the field null
        candidate.setPhone(null);

        // Create the Candidate, which fails.
        CandidateDTO candidateDTO = candidateMapper.toDto(candidate);

        restCandidateMockMvc.perform(post("/api/candidates")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(candidateDTO)))
            .andExpect(status().isBadRequest());

        List<Candidate> candidateList = candidateRepository.findAll();
        assertThat(candidateList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEmailIsRequired() throws Exception {
        int databaseSizeBeforeTest = candidateRepository.findAll().size();
        // set the field null
        candidate.setEmail(null);

        // Create the Candidate, which fails.
        CandidateDTO candidateDTO = candidateMapper.toDto(candidate);

        restCandidateMockMvc.perform(post("/api/candidates")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(candidateDTO)))
            .andExpect(status().isBadRequest());

        List<Candidate> candidateList = candidateRepository.findAll();
        assertThat(candidateList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCandidateFunctionIsRequired() throws Exception {
        int databaseSizeBeforeTest = candidateRepository.findAll().size();
        // set the field null
        candidate.setCandidateFunction(null);

        // Create the Candidate, which fails.
        CandidateDTO candidateDTO = candidateMapper.toDto(candidate);

        restCandidateMockMvc.perform(post("/api/candidates")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(candidateDTO)))
            .andExpect(status().isBadRequest());

        List<Candidate> candidateList = candidateRepository.findAll();
        assertThat(candidateList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDateAvailableIsRequired() throws Exception {
        int databaseSizeBeforeTest = candidateRepository.findAll().size();
        // set the field null
        candidate.setDateAvailable(null);

        // Create the Candidate, which fails.
        CandidateDTO candidateDTO = candidateMapper.toDto(candidate);

        restCandidateMockMvc.perform(post("/api/candidates")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(candidateDTO)))
            .andExpect(status().isBadRequest());

        List<Candidate> candidateList = candidateRepository.findAll();
        assertThat(candidateList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCandidateTrackIsRequired() throws Exception {
        int databaseSizeBeforeTest = candidateRepository.findAll().size();
        // set the field null
        candidate.setCandidateTrack(null);

        // Create the Candidate, which fails.
        CandidateDTO candidateDTO = candidateMapper.toDto(candidate);

        restCandidateMockMvc.perform(post("/api/candidates")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(candidateDTO)))
            .andExpect(status().isBadRequest());

        List<Candidate> candidateList = candidateRepository.findAll();
        assertThat(candidateList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkRecruitmentChannelIsRequired() throws Exception {
        int databaseSizeBeforeTest = candidateRepository.findAll().size();
        // set the field null
        candidate.setRecruitmentChannel(null);

        // Create the Candidate, which fails.
        CandidateDTO candidateDTO = candidateMapper.toDto(candidate);

        restCandidateMockMvc.perform(post("/api/candidates")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(candidateDTO)))
            .andExpect(status().isBadRequest());

        List<Candidate> candidateList = candidateRepository.findAll();
        assertThat(candidateList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllCandidates() throws Exception {
        // Initialize the database
        candidateRepository.saveAndFlush(candidate);

        // Get all the candidateList
        restCandidateMockMvc.perform(get("/api/candidates?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(candidate.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].phone").value(hasItem(DEFAULT_PHONE.toString())))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL.toString())))
            .andExpect(jsonPath("$.[*].candidateStatus").value(hasItem(DEFAULT_CANDIDATE_STATUS.toString())))
            .andExpect(jsonPath("$.[*].candidateFunction").value(hasItem(DEFAULT_CANDIDATE_FUNCTION.toString())))
            .andExpect(jsonPath("$.[*].dateAvailable").value(hasItem(DEFAULT_DATE_AVAILABLE.toString())))
            .andExpect(jsonPath("$.[*].imageContentContentType").value(hasItem(DEFAULT_IMAGE_CONTENT_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].imageContent").value(hasItem(Base64Utils.encodeToString(DEFAULT_IMAGE_CONTENT))))
            .andExpect(jsonPath("$.[*].candidateTrack").value(hasItem(DEFAULT_CANDIDATE_TRACK.toString())))
            .andExpect(jsonPath("$.[*].recruitmentChannel").value(hasItem(DEFAULT_RECRUITMENT_CHANNEL.toString())))
            .andExpect(jsonPath("$.[*].linkedInProfilePath").value(hasItem(DEFAULT_LINKED_IN_PROFILE_PATH.toString())));
    }
    
    @Test
    @Transactional
    public void getCandidate() throws Exception {
        // Initialize the database
        candidateRepository.saveAndFlush(candidate);

        // Get the candidate
        restCandidateMockMvc.perform(get("/api/candidates/{id}", candidate.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(candidate.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.phone").value(DEFAULT_PHONE.toString()))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL.toString()))
            .andExpect(jsonPath("$.candidateStatus").value(DEFAULT_CANDIDATE_STATUS.toString()))
            .andExpect(jsonPath("$.candidateFunction").value(DEFAULT_CANDIDATE_FUNCTION.toString()))
            .andExpect(jsonPath("$.dateAvailable").value(DEFAULT_DATE_AVAILABLE.toString()))
            .andExpect(jsonPath("$.imageContentContentType").value(DEFAULT_IMAGE_CONTENT_CONTENT_TYPE))
            .andExpect(jsonPath("$.imageContent").value(Base64Utils.encodeToString(DEFAULT_IMAGE_CONTENT)))
            .andExpect(jsonPath("$.candidateTrack").value(DEFAULT_CANDIDATE_TRACK.toString()))
            .andExpect(jsonPath("$.recruitmentChannel").value(DEFAULT_RECRUITMENT_CHANNEL.toString()))
            .andExpect(jsonPath("$.linkedInProfilePath").value(DEFAULT_LINKED_IN_PROFILE_PATH.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingCandidate() throws Exception {
        // Get the candidate
        restCandidateMockMvc.perform(get("/api/candidates/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCandidate() throws Exception {
        // Initialize the database
        candidateRepository.saveAndFlush(candidate);

        int databaseSizeBeforeUpdate = candidateRepository.findAll().size();

        // Update the candidate
        Candidate updatedCandidate = candidateRepository.findById(candidate.getId()).get();
        // Disconnect from session so that the updates on updatedCandidate are not directly saved in db
        em.detach(updatedCandidate);
        updatedCandidate
            .name(UPDATED_NAME)
            .phone(UPDATED_PHONE)
            .email(UPDATED_EMAIL)
            .candidateStatus(UPDATED_CANDIDATE_STATUS)
            .candidateFunction(UPDATED_CANDIDATE_FUNCTION)
            .dateAvailable(UPDATED_DATE_AVAILABLE)
            .imageContent(UPDATED_IMAGE_CONTENT)
            .imageContentContentType(UPDATED_IMAGE_CONTENT_CONTENT_TYPE)
            .candidateTrack(UPDATED_CANDIDATE_TRACK)
            .recruitmentChannel(UPDATED_RECRUITMENT_CHANNEL)
            .linkedInProfilePath(UPDATED_LINKED_IN_PROFILE_PATH);
        CandidateDTO candidateDTO = candidateMapper.toDto(updatedCandidate);

        restCandidateMockMvc.perform(put("/api/candidates")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(candidateDTO)))
            .andExpect(status().isOk());

        // Validate the Candidate in the database
        List<Candidate> candidateList = candidateRepository.findAll();
        assertThat(candidateList).hasSize(databaseSizeBeforeUpdate);
        Candidate testCandidate = candidateList.get(candidateList.size() - 1);
        assertThat(testCandidate.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCandidate.getPhone()).isEqualTo(UPDATED_PHONE);
        assertThat(testCandidate.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testCandidate.getCandidateStatus()).isEqualTo(UPDATED_CANDIDATE_STATUS);
        assertThat(testCandidate.getCandidateFunction()).isEqualTo(UPDATED_CANDIDATE_FUNCTION);
        assertThat(testCandidate.getDateAvailable()).isEqualTo(UPDATED_DATE_AVAILABLE);
        assertThat(testCandidate.getImageContent()).isEqualTo(UPDATED_IMAGE_CONTENT);
        assertThat(testCandidate.getImageContentContentType()).isEqualTo(UPDATED_IMAGE_CONTENT_CONTENT_TYPE);
        assertThat(testCandidate.getCandidateTrack()).isEqualTo(UPDATED_CANDIDATE_TRACK);
        assertThat(testCandidate.getRecruitmentChannel()).isEqualTo(UPDATED_RECRUITMENT_CHANNEL);
        assertThat(testCandidate.getLinkedInProfilePath()).isEqualTo(UPDATED_LINKED_IN_PROFILE_PATH);
    }

    @Test
    @Transactional
    public void updateNonExistingCandidate() throws Exception {
        int databaseSizeBeforeUpdate = candidateRepository.findAll().size();

        // Create the Candidate
        CandidateDTO candidateDTO = candidateMapper.toDto(candidate);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCandidateMockMvc.perform(put("/api/candidates")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(candidateDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Candidate in the database
        List<Candidate> candidateList = candidateRepository.findAll();
        assertThat(candidateList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCandidate() throws Exception {
        // Initialize the database
        candidateRepository.saveAndFlush(candidate);

        int databaseSizeBeforeDelete = candidateRepository.findAll().size();

        // Delete the candidate
        restCandidateMockMvc.perform(delete("/api/candidates/{id}", candidate.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<Candidate> candidateList = candidateRepository.findAll();
        assertThat(candidateList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Candidate.class);
        Candidate candidate1 = new Candidate();
        candidate1.setId(1L);
        Candidate candidate2 = new Candidate();
        candidate2.setId(candidate1.getId());
        assertThat(candidate1).isEqualTo(candidate2);
        candidate2.setId(2L);
        assertThat(candidate1).isNotEqualTo(candidate2);
        candidate1.setId(null);
        assertThat(candidate1).isNotEqualTo(candidate2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CandidateDTO.class);
        CandidateDTO candidateDTO1 = new CandidateDTO();
        candidateDTO1.setId(1L);
        CandidateDTO candidateDTO2 = new CandidateDTO();
        assertThat(candidateDTO1).isNotEqualTo(candidateDTO2);
        candidateDTO2.setId(candidateDTO1.getId());
        assertThat(candidateDTO1).isEqualTo(candidateDTO2);
        candidateDTO2.setId(2L);
        assertThat(candidateDTO1).isNotEqualTo(candidateDTO2);
        candidateDTO1.setId(null);
        assertThat(candidateDTO1).isNotEqualTo(candidateDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(candidateMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(candidateMapper.fromId(null)).isNull();
    }
}
