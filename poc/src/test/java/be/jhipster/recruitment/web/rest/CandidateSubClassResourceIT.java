package be.jhipster.recruitment.web.rest;

import be.jhipster.recruitment.JhipsterRecruitmentApp;
import be.jhipster.recruitment.config.TestSecurityConfiguration;
import be.jhipster.recruitment.domain.CandidateSubClass;
import be.jhipster.recruitment.repository.CandidateSubClassRepository;
import be.jhipster.recruitment.service.CandidateSubClassService;
import be.jhipster.recruitment.service.dto.CandidateSubClassDTO;
import be.jhipster.recruitment.service.mapper.CandidateSubClassMapper;
import be.jhipster.recruitment.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static be.jhipster.recruitment.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import be.jhipster.recruitment.domain.enumeration.CandidateType;
/**
 * Integration tests for the {@Link CandidateSubClassResource} REST controller.
 */
@SpringBootTest(classes = {JhipsterRecruitmentApp.class, TestSecurityConfiguration.class})
public class CandidateSubClassResourceIT {

    private static final CandidateType DEFAULT_CANDIDATE_TYPE = CandidateType.FREELANCE;
    private static final CandidateType UPDATED_CANDIDATE_TYPE = CandidateType.INTERN;

    @Autowired
    private CandidateSubClassRepository candidateSubClassRepository;

    @Autowired
    private CandidateSubClassMapper candidateSubClassMapper;

    @Autowired
    private CandidateSubClassService candidateSubClassService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restCandidateSubClassMockMvc;

    private CandidateSubClass candidateSubClass;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final CandidateSubClassResource candidateSubClassResource = new CandidateSubClassResource(candidateSubClassService);
        this.restCandidateSubClassMockMvc = MockMvcBuilders.standaloneSetup(candidateSubClassResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CandidateSubClass createEntity(EntityManager em) {
        CandidateSubClass candidateSubClass = new CandidateSubClass()
            .candidateType(DEFAULT_CANDIDATE_TYPE);
        return candidateSubClass;
    }

    @BeforeEach
    public void initTest() {
        candidateSubClass = createEntity(em);
    }

    @Test
    @Transactional
    public void createCandidateSubClass() throws Exception {
        int databaseSizeBeforeCreate = candidateSubClassRepository.findAll().size();

        // Create the CandidateSubClass
        CandidateSubClassDTO candidateSubClassDTO = candidateSubClassMapper.toDto(candidateSubClass);
        restCandidateSubClassMockMvc.perform(post("/api/candidate-sub-classes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(candidateSubClassDTO)))
            .andExpect(status().isCreated());

        // Validate the CandidateSubClass in the database
        List<CandidateSubClass> candidateSubClassList = candidateSubClassRepository.findAll();
        assertThat(candidateSubClassList).hasSize(databaseSizeBeforeCreate + 1);
        CandidateSubClass testCandidateSubClass = candidateSubClassList.get(candidateSubClassList.size() - 1);
        assertThat(testCandidateSubClass.getCandidateType()).isEqualTo(DEFAULT_CANDIDATE_TYPE);
    }

    @Test
    @Transactional
    public void createCandidateSubClassWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = candidateSubClassRepository.findAll().size();

        // Create the CandidateSubClass with an existing ID
        candidateSubClass.setId(1L);
        CandidateSubClassDTO candidateSubClassDTO = candidateSubClassMapper.toDto(candidateSubClass);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCandidateSubClassMockMvc.perform(post("/api/candidate-sub-classes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(candidateSubClassDTO)))
            .andExpect(status().isBadRequest());

        // Validate the CandidateSubClass in the database
        List<CandidateSubClass> candidateSubClassList = candidateSubClassRepository.findAll();
        assertThat(candidateSubClassList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllCandidateSubClasses() throws Exception {
        // Initialize the database
        candidateSubClassRepository.saveAndFlush(candidateSubClass);

        // Get all the candidateSubClassList
        restCandidateSubClassMockMvc.perform(get("/api/candidate-sub-classes?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(candidateSubClass.getId().intValue())))
            .andExpect(jsonPath("$.[*].candidateType").value(hasItem(DEFAULT_CANDIDATE_TYPE.toString())));
    }
    
    @Test
    @Transactional
    public void getCandidateSubClass() throws Exception {
        // Initialize the database
        candidateSubClassRepository.saveAndFlush(candidateSubClass);

        // Get the candidateSubClass
        restCandidateSubClassMockMvc.perform(get("/api/candidate-sub-classes/{id}", candidateSubClass.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(candidateSubClass.getId().intValue()))
            .andExpect(jsonPath("$.candidateType").value(DEFAULT_CANDIDATE_TYPE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingCandidateSubClass() throws Exception {
        // Get the candidateSubClass
        restCandidateSubClassMockMvc.perform(get("/api/candidate-sub-classes/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCandidateSubClass() throws Exception {
        // Initialize the database
        candidateSubClassRepository.saveAndFlush(candidateSubClass);

        int databaseSizeBeforeUpdate = candidateSubClassRepository.findAll().size();

        // Update the candidateSubClass
        CandidateSubClass updatedCandidateSubClass = candidateSubClassRepository.findById(candidateSubClass.getId()).get();
        // Disconnect from session so that the updates on updatedCandidateSubClass are not directly saved in db
        em.detach(updatedCandidateSubClass);
        updatedCandidateSubClass
            .candidateType(UPDATED_CANDIDATE_TYPE);
        CandidateSubClassDTO candidateSubClassDTO = candidateSubClassMapper.toDto(updatedCandidateSubClass);

        restCandidateSubClassMockMvc.perform(put("/api/candidate-sub-classes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(candidateSubClassDTO)))
            .andExpect(status().isOk());

        // Validate the CandidateSubClass in the database
        List<CandidateSubClass> candidateSubClassList = candidateSubClassRepository.findAll();
        assertThat(candidateSubClassList).hasSize(databaseSizeBeforeUpdate);
        CandidateSubClass testCandidateSubClass = candidateSubClassList.get(candidateSubClassList.size() - 1);
        assertThat(testCandidateSubClass.getCandidateType()).isEqualTo(UPDATED_CANDIDATE_TYPE);
    }

    @Test
    @Transactional
    public void updateNonExistingCandidateSubClass() throws Exception {
        int databaseSizeBeforeUpdate = candidateSubClassRepository.findAll().size();

        // Create the CandidateSubClass
        CandidateSubClassDTO candidateSubClassDTO = candidateSubClassMapper.toDto(candidateSubClass);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCandidateSubClassMockMvc.perform(put("/api/candidate-sub-classes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(candidateSubClassDTO)))
            .andExpect(status().isBadRequest());

        // Validate the CandidateSubClass in the database
        List<CandidateSubClass> candidateSubClassList = candidateSubClassRepository.findAll();
        assertThat(candidateSubClassList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCandidateSubClass() throws Exception {
        // Initialize the database
        candidateSubClassRepository.saveAndFlush(candidateSubClass);

        int databaseSizeBeforeDelete = candidateSubClassRepository.findAll().size();

        // Delete the candidateSubClass
        restCandidateSubClassMockMvc.perform(delete("/api/candidate-sub-classes/{id}", candidateSubClass.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<CandidateSubClass> candidateSubClassList = candidateSubClassRepository.findAll();
        assertThat(candidateSubClassList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CandidateSubClass.class);
        CandidateSubClass candidateSubClass1 = new CandidateSubClass();
        candidateSubClass1.setId(1L);
        CandidateSubClass candidateSubClass2 = new CandidateSubClass();
        candidateSubClass2.setId(candidateSubClass1.getId());
        assertThat(candidateSubClass1).isEqualTo(candidateSubClass2);
        candidateSubClass2.setId(2L);
        assertThat(candidateSubClass1).isNotEqualTo(candidateSubClass2);
        candidateSubClass1.setId(null);
        assertThat(candidateSubClass1).isNotEqualTo(candidateSubClass2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CandidateSubClassDTO.class);
        CandidateSubClassDTO candidateSubClassDTO1 = new CandidateSubClassDTO();
        candidateSubClassDTO1.setId(1L);
        CandidateSubClassDTO candidateSubClassDTO2 = new CandidateSubClassDTO();
        assertThat(candidateSubClassDTO1).isNotEqualTo(candidateSubClassDTO2);
        candidateSubClassDTO2.setId(candidateSubClassDTO1.getId());
        assertThat(candidateSubClassDTO1).isEqualTo(candidateSubClassDTO2);
        candidateSubClassDTO2.setId(2L);
        assertThat(candidateSubClassDTO1).isNotEqualTo(candidateSubClassDTO2);
        candidateSubClassDTO1.setId(null);
        assertThat(candidateSubClassDTO1).isNotEqualTo(candidateSubClassDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(candidateSubClassMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(candidateSubClassMapper.fromId(null)).isNull();
    }
}
