package be.jhipster.recruitment.web.rest;

import be.jhipster.recruitment.JhipsterRecruitmentApp;
import be.jhipster.recruitment.config.TestSecurityConfiguration;
import be.jhipster.recruitment.domain.ContactMoment;
import be.jhipster.recruitment.repository.ContactMomentRepository;
import be.jhipster.recruitment.service.ContactMomentService;
import be.jhipster.recruitment.service.dto.ContactMomentDTO;
import be.jhipster.recruitment.service.mapper.ContactMomentMapper;
import be.jhipster.recruitment.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static be.jhipster.recruitment.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import be.jhipster.recruitment.domain.enumeration.ContactType;
import be.jhipster.recruitment.domain.enumeration.ContactContext;
/**
 * Integration tests for the {@Link ContactMomentResource} REST controller.
 */
@SpringBootTest(classes = {JhipsterRecruitmentApp.class, TestSecurityConfiguration.class})
public class ContactMomentResourceIT {

    private static final ContactType DEFAULT_CONTACT_TYPE = ContactType.PHONE_CALL;
    private static final ContactType UPDATED_CONTACT_TYPE = ContactType.INTERVIEW;

    private static final LocalDate DEFAULT_CONTACT_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CONTACT_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final ContactContext DEFAULT_CONTACT_CONTEXT = ContactContext.INTRODUCTION;
    private static final ContactContext UPDATED_CONTACT_CONTEXT = ContactContext.FIRST_MEETING;

    private static final String DEFAULT_INTERVIEWER = "AAAAAAAAAA";
    private static final String UPDATED_INTERVIEWER = "BBBBBBBBBB";

    private static final String DEFAULT_LOCATION = "AAAAAAAAAA";
    private static final String UPDATED_LOCATION = "BBBBBBBBBB";

    @Autowired
    private ContactMomentRepository contactMomentRepository;

    @Autowired
    private ContactMomentMapper contactMomentMapper;

    @Autowired
    private ContactMomentService contactMomentService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restContactMomentMockMvc;

    private ContactMoment contactMoment;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ContactMomentResource contactMomentResource = new ContactMomentResource(contactMomentService);
        this.restContactMomentMockMvc = MockMvcBuilders.standaloneSetup(contactMomentResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ContactMoment createEntity(EntityManager em) {
        ContactMoment contactMoment = new ContactMoment()
            .contactType(DEFAULT_CONTACT_TYPE)
            .contactDate(DEFAULT_CONTACT_DATE)
            .contactContext(DEFAULT_CONTACT_CONTEXT)
            .interviewer(DEFAULT_INTERVIEWER)
            .location(DEFAULT_LOCATION);
        return contactMoment;
    }

    @BeforeEach
    public void initTest() {
        contactMoment = createEntity(em);
    }

    @Test
    @Transactional
    public void createContactMoment() throws Exception {
        int databaseSizeBeforeCreate = contactMomentRepository.findAll().size();

        // Create the ContactMoment
        ContactMomentDTO contactMomentDTO = contactMomentMapper.toDto(contactMoment);
        restContactMomentMockMvc.perform(post("/api/contact-moments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(contactMomentDTO)))
            .andExpect(status().isCreated());

        // Validate the ContactMoment in the database
        List<ContactMoment> contactMomentList = contactMomentRepository.findAll();
        assertThat(contactMomentList).hasSize(databaseSizeBeforeCreate + 1);
        ContactMoment testContactMoment = contactMomentList.get(contactMomentList.size() - 1);
        assertThat(testContactMoment.getContactType()).isEqualTo(DEFAULT_CONTACT_TYPE);
        assertThat(testContactMoment.getContactDate()).isEqualTo(DEFAULT_CONTACT_DATE);
        assertThat(testContactMoment.getContactContext()).isEqualTo(DEFAULT_CONTACT_CONTEXT);
        assertThat(testContactMoment.getInterviewer()).isEqualTo(DEFAULT_INTERVIEWER);
        assertThat(testContactMoment.getLocation()).isEqualTo(DEFAULT_LOCATION);
    }

    @Test
    @Transactional
    public void createContactMomentWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = contactMomentRepository.findAll().size();

        // Create the ContactMoment with an existing ID
        contactMoment.setId(1L);
        ContactMomentDTO contactMomentDTO = contactMomentMapper.toDto(contactMoment);

        // An entity with an existing ID cannot be created, so this API call must fail
        restContactMomentMockMvc.perform(post("/api/contact-moments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(contactMomentDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ContactMoment in the database
        List<ContactMoment> contactMomentList = contactMomentRepository.findAll();
        assertThat(contactMomentList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkContactTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = contactMomentRepository.findAll().size();
        // set the field null
        contactMoment.setContactType(null);

        // Create the ContactMoment, which fails.
        ContactMomentDTO contactMomentDTO = contactMomentMapper.toDto(contactMoment);

        restContactMomentMockMvc.perform(post("/api/contact-moments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(contactMomentDTO)))
            .andExpect(status().isBadRequest());

        List<ContactMoment> contactMomentList = contactMomentRepository.findAll();
        assertThat(contactMomentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkContactDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = contactMomentRepository.findAll().size();
        // set the field null
        contactMoment.setContactDate(null);

        // Create the ContactMoment, which fails.
        ContactMomentDTO contactMomentDTO = contactMomentMapper.toDto(contactMoment);

        restContactMomentMockMvc.perform(post("/api/contact-moments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(contactMomentDTO)))
            .andExpect(status().isBadRequest());

        List<ContactMoment> contactMomentList = contactMomentRepository.findAll();
        assertThat(contactMomentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkContactContextIsRequired() throws Exception {
        int databaseSizeBeforeTest = contactMomentRepository.findAll().size();
        // set the field null
        contactMoment.setContactContext(null);

        // Create the ContactMoment, which fails.
        ContactMomentDTO contactMomentDTO = contactMomentMapper.toDto(contactMoment);

        restContactMomentMockMvc.perform(post("/api/contact-moments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(contactMomentDTO)))
            .andExpect(status().isBadRequest());

        List<ContactMoment> contactMomentList = contactMomentRepository.findAll();
        assertThat(contactMomentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkInterviewerIsRequired() throws Exception {
        int databaseSizeBeforeTest = contactMomentRepository.findAll().size();
        // set the field null
        contactMoment.setInterviewer(null);

        // Create the ContactMoment, which fails.
        ContactMomentDTO contactMomentDTO = contactMomentMapper.toDto(contactMoment);

        restContactMomentMockMvc.perform(post("/api/contact-moments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(contactMomentDTO)))
            .andExpect(status().isBadRequest());

        List<ContactMoment> contactMomentList = contactMomentRepository.findAll();
        assertThat(contactMomentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllContactMoments() throws Exception {
        // Initialize the database
        contactMomentRepository.saveAndFlush(contactMoment);

        // Get all the contactMomentList
        restContactMomentMockMvc.perform(get("/api/contact-moments?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(contactMoment.getId().intValue())))
            .andExpect(jsonPath("$.[*].contactType").value(hasItem(DEFAULT_CONTACT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].contactDate").value(hasItem(DEFAULT_CONTACT_DATE.toString())))
            .andExpect(jsonPath("$.[*].contactContext").value(hasItem(DEFAULT_CONTACT_CONTEXT.toString())))
            .andExpect(jsonPath("$.[*].interviewer").value(hasItem(DEFAULT_INTERVIEWER.toString())))
            .andExpect(jsonPath("$.[*].location").value(hasItem(DEFAULT_LOCATION.toString())));
    }
    
    @Test
    @Transactional
    public void getContactMoment() throws Exception {
        // Initialize the database
        contactMomentRepository.saveAndFlush(contactMoment);

        // Get the contactMoment
        restContactMomentMockMvc.perform(get("/api/contact-moments/{id}", contactMoment.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(contactMoment.getId().intValue()))
            .andExpect(jsonPath("$.contactType").value(DEFAULT_CONTACT_TYPE.toString()))
            .andExpect(jsonPath("$.contactDate").value(DEFAULT_CONTACT_DATE.toString()))
            .andExpect(jsonPath("$.contactContext").value(DEFAULT_CONTACT_CONTEXT.toString()))
            .andExpect(jsonPath("$.interviewer").value(DEFAULT_INTERVIEWER.toString()))
            .andExpect(jsonPath("$.location").value(DEFAULT_LOCATION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingContactMoment() throws Exception {
        // Get the contactMoment
        restContactMomentMockMvc.perform(get("/api/contact-moments/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateContactMoment() throws Exception {
        // Initialize the database
        contactMomentRepository.saveAndFlush(contactMoment);

        int databaseSizeBeforeUpdate = contactMomentRepository.findAll().size();

        // Update the contactMoment
        ContactMoment updatedContactMoment = contactMomentRepository.findById(contactMoment.getId()).get();
        // Disconnect from session so that the updates on updatedContactMoment are not directly saved in db
        em.detach(updatedContactMoment);
        updatedContactMoment
            .contactType(UPDATED_CONTACT_TYPE)
            .contactDate(UPDATED_CONTACT_DATE)
            .contactContext(UPDATED_CONTACT_CONTEXT)
            .interviewer(UPDATED_INTERVIEWER)
            .location(UPDATED_LOCATION);
        ContactMomentDTO contactMomentDTO = contactMomentMapper.toDto(updatedContactMoment);

        restContactMomentMockMvc.perform(put("/api/contact-moments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(contactMomentDTO)))
            .andExpect(status().isOk());

        // Validate the ContactMoment in the database
        List<ContactMoment> contactMomentList = contactMomentRepository.findAll();
        assertThat(contactMomentList).hasSize(databaseSizeBeforeUpdate);
        ContactMoment testContactMoment = contactMomentList.get(contactMomentList.size() - 1);
        assertThat(testContactMoment.getContactType()).isEqualTo(UPDATED_CONTACT_TYPE);
        assertThat(testContactMoment.getContactDate()).isEqualTo(UPDATED_CONTACT_DATE);
        assertThat(testContactMoment.getContactContext()).isEqualTo(UPDATED_CONTACT_CONTEXT);
        assertThat(testContactMoment.getInterviewer()).isEqualTo(UPDATED_INTERVIEWER);
        assertThat(testContactMoment.getLocation()).isEqualTo(UPDATED_LOCATION);
    }

    @Test
    @Transactional
    public void updateNonExistingContactMoment() throws Exception {
        int databaseSizeBeforeUpdate = contactMomentRepository.findAll().size();

        // Create the ContactMoment
        ContactMomentDTO contactMomentDTO = contactMomentMapper.toDto(contactMoment);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restContactMomentMockMvc.perform(put("/api/contact-moments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(contactMomentDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ContactMoment in the database
        List<ContactMoment> contactMomentList = contactMomentRepository.findAll();
        assertThat(contactMomentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteContactMoment() throws Exception {
        // Initialize the database
        contactMomentRepository.saveAndFlush(contactMoment);

        int databaseSizeBeforeDelete = contactMomentRepository.findAll().size();

        // Delete the contactMoment
        restContactMomentMockMvc.perform(delete("/api/contact-moments/{id}", contactMoment.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<ContactMoment> contactMomentList = contactMomentRepository.findAll();
        assertThat(contactMomentList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ContactMoment.class);
        ContactMoment contactMoment1 = new ContactMoment();
        contactMoment1.setId(1L);
        ContactMoment contactMoment2 = new ContactMoment();
        contactMoment2.setId(contactMoment1.getId());
        assertThat(contactMoment1).isEqualTo(contactMoment2);
        contactMoment2.setId(2L);
        assertThat(contactMoment1).isNotEqualTo(contactMoment2);
        contactMoment1.setId(null);
        assertThat(contactMoment1).isNotEqualTo(contactMoment2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ContactMomentDTO.class);
        ContactMomentDTO contactMomentDTO1 = new ContactMomentDTO();
        contactMomentDTO1.setId(1L);
        ContactMomentDTO contactMomentDTO2 = new ContactMomentDTO();
        assertThat(contactMomentDTO1).isNotEqualTo(contactMomentDTO2);
        contactMomentDTO2.setId(contactMomentDTO1.getId());
        assertThat(contactMomentDTO1).isEqualTo(contactMomentDTO2);
        contactMomentDTO2.setId(2L);
        assertThat(contactMomentDTO1).isNotEqualTo(contactMomentDTO2);
        contactMomentDTO1.setId(null);
        assertThat(contactMomentDTO1).isNotEqualTo(contactMomentDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(contactMomentMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(contactMomentMapper.fromId(null)).isNull();
    }
}
