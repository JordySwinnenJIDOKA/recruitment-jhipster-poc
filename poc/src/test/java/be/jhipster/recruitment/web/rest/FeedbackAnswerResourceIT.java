package be.jhipster.recruitment.web.rest;

import be.jhipster.recruitment.JhipsterRecruitmentApp;
import be.jhipster.recruitment.config.TestSecurityConfiguration;
import be.jhipster.recruitment.domain.FeedbackAnswer;
import be.jhipster.recruitment.repository.FeedbackAnswerRepository;
import be.jhipster.recruitment.service.FeedbackAnswerService;
import be.jhipster.recruitment.service.dto.FeedbackAnswerDTO;
import be.jhipster.recruitment.service.mapper.FeedbackAnswerMapper;
import be.jhipster.recruitment.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static be.jhipster.recruitment.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link FeedbackAnswerResource} REST controller.
 */
@SpringBootTest(classes = {JhipsterRecruitmentApp.class, TestSecurityConfiguration.class})
public class FeedbackAnswerResourceIT {

    private static final String DEFAULT_ANSWER = "AAAAAAAAAA";
    private static final String UPDATED_ANSWER = "BBBBBBBBBB";

    @Autowired
    private FeedbackAnswerRepository feedbackAnswerRepository;

    @Autowired
    private FeedbackAnswerMapper feedbackAnswerMapper;

    @Autowired
    private FeedbackAnswerService feedbackAnswerService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restFeedbackAnswerMockMvc;

    private FeedbackAnswer feedbackAnswer;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final FeedbackAnswerResource feedbackAnswerResource = new FeedbackAnswerResource(feedbackAnswerService);
        this.restFeedbackAnswerMockMvc = MockMvcBuilders.standaloneSetup(feedbackAnswerResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FeedbackAnswer createEntity(EntityManager em) {
        FeedbackAnswer feedbackAnswer = new FeedbackAnswer()
            .answer(DEFAULT_ANSWER);
        return feedbackAnswer;
    }

    @BeforeEach
    public void initTest() {
        feedbackAnswer = createEntity(em);
    }

    @Test
    @Transactional
    public void createFeedbackAnswer() throws Exception {
        int databaseSizeBeforeCreate = feedbackAnswerRepository.findAll().size();

        // Create the FeedbackAnswer
        FeedbackAnswerDTO feedbackAnswerDTO = feedbackAnswerMapper.toDto(feedbackAnswer);
        restFeedbackAnswerMockMvc.perform(post("/api/feedback-answers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(feedbackAnswerDTO)))
            .andExpect(status().isCreated());

        // Validate the FeedbackAnswer in the database
        List<FeedbackAnswer> feedbackAnswerList = feedbackAnswerRepository.findAll();
        assertThat(feedbackAnswerList).hasSize(databaseSizeBeforeCreate + 1);
        FeedbackAnswer testFeedbackAnswer = feedbackAnswerList.get(feedbackAnswerList.size() - 1);
        assertThat(testFeedbackAnswer.getAnswer()).isEqualTo(DEFAULT_ANSWER);
    }

    @Test
    @Transactional
    public void createFeedbackAnswerWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = feedbackAnswerRepository.findAll().size();

        // Create the FeedbackAnswer with an existing ID
        feedbackAnswer.setId(1L);
        FeedbackAnswerDTO feedbackAnswerDTO = feedbackAnswerMapper.toDto(feedbackAnswer);

        // An entity with an existing ID cannot be created, so this API call must fail
        restFeedbackAnswerMockMvc.perform(post("/api/feedback-answers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(feedbackAnswerDTO)))
            .andExpect(status().isBadRequest());

        // Validate the FeedbackAnswer in the database
        List<FeedbackAnswer> feedbackAnswerList = feedbackAnswerRepository.findAll();
        assertThat(feedbackAnswerList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkAnswerIsRequired() throws Exception {
        int databaseSizeBeforeTest = feedbackAnswerRepository.findAll().size();
        // set the field null
        feedbackAnswer.setAnswer(null);

        // Create the FeedbackAnswer, which fails.
        FeedbackAnswerDTO feedbackAnswerDTO = feedbackAnswerMapper.toDto(feedbackAnswer);

        restFeedbackAnswerMockMvc.perform(post("/api/feedback-answers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(feedbackAnswerDTO)))
            .andExpect(status().isBadRequest());

        List<FeedbackAnswer> feedbackAnswerList = feedbackAnswerRepository.findAll();
        assertThat(feedbackAnswerList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllFeedbackAnswers() throws Exception {
        // Initialize the database
        feedbackAnswerRepository.saveAndFlush(feedbackAnswer);

        // Get all the feedbackAnswerList
        restFeedbackAnswerMockMvc.perform(get("/api/feedback-answers?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(feedbackAnswer.getId().intValue())))
            .andExpect(jsonPath("$.[*].answer").value(hasItem(DEFAULT_ANSWER.toString())));
    }
    
    @Test
    @Transactional
    public void getFeedbackAnswer() throws Exception {
        // Initialize the database
        feedbackAnswerRepository.saveAndFlush(feedbackAnswer);

        // Get the feedbackAnswer
        restFeedbackAnswerMockMvc.perform(get("/api/feedback-answers/{id}", feedbackAnswer.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(feedbackAnswer.getId().intValue()))
            .andExpect(jsonPath("$.answer").value(DEFAULT_ANSWER.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingFeedbackAnswer() throws Exception {
        // Get the feedbackAnswer
        restFeedbackAnswerMockMvc.perform(get("/api/feedback-answers/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateFeedbackAnswer() throws Exception {
        // Initialize the database
        feedbackAnswerRepository.saveAndFlush(feedbackAnswer);

        int databaseSizeBeforeUpdate = feedbackAnswerRepository.findAll().size();

        // Update the feedbackAnswer
        FeedbackAnswer updatedFeedbackAnswer = feedbackAnswerRepository.findById(feedbackAnswer.getId()).get();
        // Disconnect from session so that the updates on updatedFeedbackAnswer are not directly saved in db
        em.detach(updatedFeedbackAnswer);
        updatedFeedbackAnswer
            .answer(UPDATED_ANSWER);
        FeedbackAnswerDTO feedbackAnswerDTO = feedbackAnswerMapper.toDto(updatedFeedbackAnswer);

        restFeedbackAnswerMockMvc.perform(put("/api/feedback-answers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(feedbackAnswerDTO)))
            .andExpect(status().isOk());

        // Validate the FeedbackAnswer in the database
        List<FeedbackAnswer> feedbackAnswerList = feedbackAnswerRepository.findAll();
        assertThat(feedbackAnswerList).hasSize(databaseSizeBeforeUpdate);
        FeedbackAnswer testFeedbackAnswer = feedbackAnswerList.get(feedbackAnswerList.size() - 1);
        assertThat(testFeedbackAnswer.getAnswer()).isEqualTo(UPDATED_ANSWER);
    }

    @Test
    @Transactional
    public void updateNonExistingFeedbackAnswer() throws Exception {
        int databaseSizeBeforeUpdate = feedbackAnswerRepository.findAll().size();

        // Create the FeedbackAnswer
        FeedbackAnswerDTO feedbackAnswerDTO = feedbackAnswerMapper.toDto(feedbackAnswer);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFeedbackAnswerMockMvc.perform(put("/api/feedback-answers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(feedbackAnswerDTO)))
            .andExpect(status().isBadRequest());

        // Validate the FeedbackAnswer in the database
        List<FeedbackAnswer> feedbackAnswerList = feedbackAnswerRepository.findAll();
        assertThat(feedbackAnswerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteFeedbackAnswer() throws Exception {
        // Initialize the database
        feedbackAnswerRepository.saveAndFlush(feedbackAnswer);

        int databaseSizeBeforeDelete = feedbackAnswerRepository.findAll().size();

        // Delete the feedbackAnswer
        restFeedbackAnswerMockMvc.perform(delete("/api/feedback-answers/{id}", feedbackAnswer.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<FeedbackAnswer> feedbackAnswerList = feedbackAnswerRepository.findAll();
        assertThat(feedbackAnswerList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(FeedbackAnswer.class);
        FeedbackAnswer feedbackAnswer1 = new FeedbackAnswer();
        feedbackAnswer1.setId(1L);
        FeedbackAnswer feedbackAnswer2 = new FeedbackAnswer();
        feedbackAnswer2.setId(feedbackAnswer1.getId());
        assertThat(feedbackAnswer1).isEqualTo(feedbackAnswer2);
        feedbackAnswer2.setId(2L);
        assertThat(feedbackAnswer1).isNotEqualTo(feedbackAnswer2);
        feedbackAnswer1.setId(null);
        assertThat(feedbackAnswer1).isNotEqualTo(feedbackAnswer2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(FeedbackAnswerDTO.class);
        FeedbackAnswerDTO feedbackAnswerDTO1 = new FeedbackAnswerDTO();
        feedbackAnswerDTO1.setId(1L);
        FeedbackAnswerDTO feedbackAnswerDTO2 = new FeedbackAnswerDTO();
        assertThat(feedbackAnswerDTO1).isNotEqualTo(feedbackAnswerDTO2);
        feedbackAnswerDTO2.setId(feedbackAnswerDTO1.getId());
        assertThat(feedbackAnswerDTO1).isEqualTo(feedbackAnswerDTO2);
        feedbackAnswerDTO2.setId(2L);
        assertThat(feedbackAnswerDTO1).isNotEqualTo(feedbackAnswerDTO2);
        feedbackAnswerDTO1.setId(null);
        assertThat(feedbackAnswerDTO1).isNotEqualTo(feedbackAnswerDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(feedbackAnswerMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(feedbackAnswerMapper.fromId(null)).isNull();
    }
}
