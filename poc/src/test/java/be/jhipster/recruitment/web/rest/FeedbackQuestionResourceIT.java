package be.jhipster.recruitment.web.rest;

import be.jhipster.recruitment.JhipsterRecruitmentApp;
import be.jhipster.recruitment.config.TestSecurityConfiguration;
import be.jhipster.recruitment.domain.FeedbackQuestion;
import be.jhipster.recruitment.repository.FeedbackQuestionRepository;
import be.jhipster.recruitment.service.FeedbackQuestionService;
import be.jhipster.recruitment.service.dto.FeedbackQuestionDTO;
import be.jhipster.recruitment.service.mapper.FeedbackQuestionMapper;
import be.jhipster.recruitment.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static be.jhipster.recruitment.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link FeedbackQuestionResource} REST controller.
 */
@SpringBootTest(classes = {JhipsterRecruitmentApp.class, TestSecurityConfiguration.class})
public class FeedbackQuestionResourceIT {

    private static final String DEFAULT_QUESTION = "AAAAAAAAAA";
    private static final String UPDATED_QUESTION = "BBBBBBBBBB";

    @Autowired
    private FeedbackQuestionRepository feedbackQuestionRepository;

    @Autowired
    private FeedbackQuestionMapper feedbackQuestionMapper;

    @Autowired
    private FeedbackQuestionService feedbackQuestionService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restFeedbackQuestionMockMvc;

    private FeedbackQuestion feedbackQuestion;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final FeedbackQuestionResource feedbackQuestionResource = new FeedbackQuestionResource(feedbackQuestionService);
        this.restFeedbackQuestionMockMvc = MockMvcBuilders.standaloneSetup(feedbackQuestionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FeedbackQuestion createEntity(EntityManager em) {
        FeedbackQuestion feedbackQuestion = new FeedbackQuestion()
            .question(DEFAULT_QUESTION);
        return feedbackQuestion;
    }

    @BeforeEach
    public void initTest() {
        feedbackQuestion = createEntity(em);
    }

    @Test
    @Transactional
    public void createFeedbackQuestion() throws Exception {
        int databaseSizeBeforeCreate = feedbackQuestionRepository.findAll().size();

        // Create the FeedbackQuestion
        FeedbackQuestionDTO feedbackQuestionDTO = feedbackQuestionMapper.toDto(feedbackQuestion);
        restFeedbackQuestionMockMvc.perform(post("/api/feedback-questions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(feedbackQuestionDTO)))
            .andExpect(status().isCreated());

        // Validate the FeedbackQuestion in the database
        List<FeedbackQuestion> feedbackQuestionList = feedbackQuestionRepository.findAll();
        assertThat(feedbackQuestionList).hasSize(databaseSizeBeforeCreate + 1);
        FeedbackQuestion testFeedbackQuestion = feedbackQuestionList.get(feedbackQuestionList.size() - 1);
        assertThat(testFeedbackQuestion.getQuestion()).isEqualTo(DEFAULT_QUESTION);
    }

    @Test
    @Transactional
    public void createFeedbackQuestionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = feedbackQuestionRepository.findAll().size();

        // Create the FeedbackQuestion with an existing ID
        feedbackQuestion.setId(1L);
        FeedbackQuestionDTO feedbackQuestionDTO = feedbackQuestionMapper.toDto(feedbackQuestion);

        // An entity with an existing ID cannot be created, so this API call must fail
        restFeedbackQuestionMockMvc.perform(post("/api/feedback-questions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(feedbackQuestionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the FeedbackQuestion in the database
        List<FeedbackQuestion> feedbackQuestionList = feedbackQuestionRepository.findAll();
        assertThat(feedbackQuestionList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkQuestionIsRequired() throws Exception {
        int databaseSizeBeforeTest = feedbackQuestionRepository.findAll().size();
        // set the field null
        feedbackQuestion.setQuestion(null);

        // Create the FeedbackQuestion, which fails.
        FeedbackQuestionDTO feedbackQuestionDTO = feedbackQuestionMapper.toDto(feedbackQuestion);

        restFeedbackQuestionMockMvc.perform(post("/api/feedback-questions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(feedbackQuestionDTO)))
            .andExpect(status().isBadRequest());

        List<FeedbackQuestion> feedbackQuestionList = feedbackQuestionRepository.findAll();
        assertThat(feedbackQuestionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllFeedbackQuestions() throws Exception {
        // Initialize the database
        feedbackQuestionRepository.saveAndFlush(feedbackQuestion);

        // Get all the feedbackQuestionList
        restFeedbackQuestionMockMvc.perform(get("/api/feedback-questions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(feedbackQuestion.getId().intValue())))
            .andExpect(jsonPath("$.[*].question").value(hasItem(DEFAULT_QUESTION.toString())));
    }
    
    @Test
    @Transactional
    public void getFeedbackQuestion() throws Exception {
        // Initialize the database
        feedbackQuestionRepository.saveAndFlush(feedbackQuestion);

        // Get the feedbackQuestion
        restFeedbackQuestionMockMvc.perform(get("/api/feedback-questions/{id}", feedbackQuestion.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(feedbackQuestion.getId().intValue()))
            .andExpect(jsonPath("$.question").value(DEFAULT_QUESTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingFeedbackQuestion() throws Exception {
        // Get the feedbackQuestion
        restFeedbackQuestionMockMvc.perform(get("/api/feedback-questions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateFeedbackQuestion() throws Exception {
        // Initialize the database
        feedbackQuestionRepository.saveAndFlush(feedbackQuestion);

        int databaseSizeBeforeUpdate = feedbackQuestionRepository.findAll().size();

        // Update the feedbackQuestion
        FeedbackQuestion updatedFeedbackQuestion = feedbackQuestionRepository.findById(feedbackQuestion.getId()).get();
        // Disconnect from session so that the updates on updatedFeedbackQuestion are not directly saved in db
        em.detach(updatedFeedbackQuestion);
        updatedFeedbackQuestion
            .question(UPDATED_QUESTION);
        FeedbackQuestionDTO feedbackQuestionDTO = feedbackQuestionMapper.toDto(updatedFeedbackQuestion);

        restFeedbackQuestionMockMvc.perform(put("/api/feedback-questions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(feedbackQuestionDTO)))
            .andExpect(status().isOk());

        // Validate the FeedbackQuestion in the database
        List<FeedbackQuestion> feedbackQuestionList = feedbackQuestionRepository.findAll();
        assertThat(feedbackQuestionList).hasSize(databaseSizeBeforeUpdate);
        FeedbackQuestion testFeedbackQuestion = feedbackQuestionList.get(feedbackQuestionList.size() - 1);
        assertThat(testFeedbackQuestion.getQuestion()).isEqualTo(UPDATED_QUESTION);
    }

    @Test
    @Transactional
    public void updateNonExistingFeedbackQuestion() throws Exception {
        int databaseSizeBeforeUpdate = feedbackQuestionRepository.findAll().size();

        // Create the FeedbackQuestion
        FeedbackQuestionDTO feedbackQuestionDTO = feedbackQuestionMapper.toDto(feedbackQuestion);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFeedbackQuestionMockMvc.perform(put("/api/feedback-questions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(feedbackQuestionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the FeedbackQuestion in the database
        List<FeedbackQuestion> feedbackQuestionList = feedbackQuestionRepository.findAll();
        assertThat(feedbackQuestionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteFeedbackQuestion() throws Exception {
        // Initialize the database
        feedbackQuestionRepository.saveAndFlush(feedbackQuestion);

        int databaseSizeBeforeDelete = feedbackQuestionRepository.findAll().size();

        // Delete the feedbackQuestion
        restFeedbackQuestionMockMvc.perform(delete("/api/feedback-questions/{id}", feedbackQuestion.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<FeedbackQuestion> feedbackQuestionList = feedbackQuestionRepository.findAll();
        assertThat(feedbackQuestionList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(FeedbackQuestion.class);
        FeedbackQuestion feedbackQuestion1 = new FeedbackQuestion();
        feedbackQuestion1.setId(1L);
        FeedbackQuestion feedbackQuestion2 = new FeedbackQuestion();
        feedbackQuestion2.setId(feedbackQuestion1.getId());
        assertThat(feedbackQuestion1).isEqualTo(feedbackQuestion2);
        feedbackQuestion2.setId(2L);
        assertThat(feedbackQuestion1).isNotEqualTo(feedbackQuestion2);
        feedbackQuestion1.setId(null);
        assertThat(feedbackQuestion1).isNotEqualTo(feedbackQuestion2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(FeedbackQuestionDTO.class);
        FeedbackQuestionDTO feedbackQuestionDTO1 = new FeedbackQuestionDTO();
        feedbackQuestionDTO1.setId(1L);
        FeedbackQuestionDTO feedbackQuestionDTO2 = new FeedbackQuestionDTO();
        assertThat(feedbackQuestionDTO1).isNotEqualTo(feedbackQuestionDTO2);
        feedbackQuestionDTO2.setId(feedbackQuestionDTO1.getId());
        assertThat(feedbackQuestionDTO1).isEqualTo(feedbackQuestionDTO2);
        feedbackQuestionDTO2.setId(2L);
        assertThat(feedbackQuestionDTO1).isNotEqualTo(feedbackQuestionDTO2);
        feedbackQuestionDTO1.setId(null);
        assertThat(feedbackQuestionDTO1).isNotEqualTo(feedbackQuestionDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(feedbackQuestionMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(feedbackQuestionMapper.fromId(null)).isNull();
    }
}
