package be.jhipster.recruitment.web.rest;

import be.jhipster.recruitment.JhipsterRecruitmentApp;
import be.jhipster.recruitment.config.TestSecurityConfiguration;
import be.jhipster.recruitment.domain.Vacancy;
import be.jhipster.recruitment.repository.VacancyRepository;
import be.jhipster.recruitment.service.VacancyService;
import be.jhipster.recruitment.service.dto.VacancyDTO;
import be.jhipster.recruitment.service.mapper.VacancyMapper;
import be.jhipster.recruitment.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static be.jhipster.recruitment.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import be.jhipster.recruitment.domain.enumeration.VacancyStatus;
import be.jhipster.recruitment.domain.enumeration.CandidateType;
/**
 * Integration tests for the {@Link VacancyResource} REST controller.
 */
@SpringBootTest(classes = {JhipsterRecruitmentApp.class, TestSecurityConfiguration.class})
public class VacancyResourceIT {

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_CREATION_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATION_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final VacancyStatus DEFAULT_VACANCY_STATUS = VacancyStatus.OPEN;
    private static final VacancyStatus UPDATED_VACANCY_STATUS = VacancyStatus.ON_HOLD;

    private static final String DEFAULT_CHARACTERISTICS = "AAAAAAAAAA";
    private static final String UPDATED_CHARACTERISTICS = "BBBBBBBBBB";

    private static final String DEFAULT_SKILLS = "AAAAAAAAAA";
    private static final String UPDATED_SKILLS = "BBBBBBBBBB";

    private static final String DEFAULT_DAILY_ROUTINE = "AAAAAAAAAA";
    private static final String UPDATED_DAILY_ROUTINE = "BBBBBBBBBB";

    private static final String DEFAULT_LOCATION = "AAAAAAAAAA";
    private static final String UPDATED_LOCATION = "BBBBBBBBBB";

    private static final CandidateType DEFAULT_CANDIDATE_TYPES = CandidateType.FREELANCE;
    private static final CandidateType UPDATED_CANDIDATE_TYPES = CandidateType.INTERN;

    @Autowired
    private VacancyRepository vacancyRepository;

    @Autowired
    private VacancyMapper vacancyMapper;

    @Autowired
    private VacancyService vacancyService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restVacancyMockMvc;

    private Vacancy vacancy;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final VacancyResource vacancyResource = new VacancyResource(vacancyService);
        this.restVacancyMockMvc = MockMvcBuilders.standaloneSetup(vacancyResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Vacancy createEntity(EntityManager em) {
        Vacancy vacancy = new Vacancy()
            .code(DEFAULT_CODE)
            .description(DEFAULT_DESCRIPTION)
            .creationDate(DEFAULT_CREATION_DATE)
            .vacancyStatus(DEFAULT_VACANCY_STATUS)
            .characteristics(DEFAULT_CHARACTERISTICS)
            .skills(DEFAULT_SKILLS)
            .dailyRoutine(DEFAULT_DAILY_ROUTINE)
            .location(DEFAULT_LOCATION)
            .candidateTypes(DEFAULT_CANDIDATE_TYPES);
        return vacancy;
    }

    @BeforeEach
    public void initTest() {
        vacancy = createEntity(em);
    }

    @Test
    @Transactional
    public void createVacancy() throws Exception {
        int databaseSizeBeforeCreate = vacancyRepository.findAll().size();

        // Create the Vacancy
        VacancyDTO vacancyDTO = vacancyMapper.toDto(vacancy);
        restVacancyMockMvc.perform(post("/api/vacancies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(vacancyDTO)))
            .andExpect(status().isCreated());

        // Validate the Vacancy in the database
        List<Vacancy> vacancyList = vacancyRepository.findAll();
        assertThat(vacancyList).hasSize(databaseSizeBeforeCreate + 1);
        Vacancy testVacancy = vacancyList.get(vacancyList.size() - 1);
        assertThat(testVacancy.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testVacancy.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testVacancy.getCreationDate()).isEqualTo(DEFAULT_CREATION_DATE);
        assertThat(testVacancy.getVacancyStatus()).isEqualTo(DEFAULT_VACANCY_STATUS);
        assertThat(testVacancy.getCharacteristics()).isEqualTo(DEFAULT_CHARACTERISTICS);
        assertThat(testVacancy.getSkills()).isEqualTo(DEFAULT_SKILLS);
        assertThat(testVacancy.getDailyRoutine()).isEqualTo(DEFAULT_DAILY_ROUTINE);
        assertThat(testVacancy.getLocation()).isEqualTo(DEFAULT_LOCATION);
        assertThat(testVacancy.getCandidateTypes()).isEqualTo(DEFAULT_CANDIDATE_TYPES);
    }

    @Test
    @Transactional
    public void createVacancyWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = vacancyRepository.findAll().size();

        // Create the Vacancy with an existing ID
        vacancy.setId(1L);
        VacancyDTO vacancyDTO = vacancyMapper.toDto(vacancy);

        // An entity with an existing ID cannot be created, so this API call must fail
        restVacancyMockMvc.perform(post("/api/vacancies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(vacancyDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Vacancy in the database
        List<Vacancy> vacancyList = vacancyRepository.findAll();
        assertThat(vacancyList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = vacancyRepository.findAll().size();
        // set the field null
        vacancy.setCode(null);

        // Create the Vacancy, which fails.
        VacancyDTO vacancyDTO = vacancyMapper.toDto(vacancy);

        restVacancyMockMvc.perform(post("/api/vacancies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(vacancyDTO)))
            .andExpect(status().isBadRequest());

        List<Vacancy> vacancyList = vacancyRepository.findAll();
        assertThat(vacancyList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDescriptionIsRequired() throws Exception {
        int databaseSizeBeforeTest = vacancyRepository.findAll().size();
        // set the field null
        vacancy.setDescription(null);

        // Create the Vacancy, which fails.
        VacancyDTO vacancyDTO = vacancyMapper.toDto(vacancy);

        restVacancyMockMvc.perform(post("/api/vacancies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(vacancyDTO)))
            .andExpect(status().isBadRequest());

        List<Vacancy> vacancyList = vacancyRepository.findAll();
        assertThat(vacancyList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCreationDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = vacancyRepository.findAll().size();
        // set the field null
        vacancy.setCreationDate(null);

        // Create the Vacancy, which fails.
        VacancyDTO vacancyDTO = vacancyMapper.toDto(vacancy);

        restVacancyMockMvc.perform(post("/api/vacancies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(vacancyDTO)))
            .andExpect(status().isBadRequest());

        List<Vacancy> vacancyList = vacancyRepository.findAll();
        assertThat(vacancyList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllVacancies() throws Exception {
        // Initialize the database
        vacancyRepository.saveAndFlush(vacancy);

        // Get all the vacancyList
        restVacancyMockMvc.perform(get("/api/vacancies?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(vacancy.getId().intValue())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].creationDate").value(hasItem(DEFAULT_CREATION_DATE.toString())))
            .andExpect(jsonPath("$.[*].vacancyStatus").value(hasItem(DEFAULT_VACANCY_STATUS.toString())))
            .andExpect(jsonPath("$.[*].characteristics").value(hasItem(DEFAULT_CHARACTERISTICS.toString())))
            .andExpect(jsonPath("$.[*].skills").value(hasItem(DEFAULT_SKILLS.toString())))
            .andExpect(jsonPath("$.[*].dailyRoutine").value(hasItem(DEFAULT_DAILY_ROUTINE.toString())))
            .andExpect(jsonPath("$.[*].location").value(hasItem(DEFAULT_LOCATION.toString())))
            .andExpect(jsonPath("$.[*].candidateTypes").value(hasItem(DEFAULT_CANDIDATE_TYPES.toString())));
    }
    
    @Test
    @Transactional
    public void getVacancy() throws Exception {
        // Initialize the database
        vacancyRepository.saveAndFlush(vacancy);

        // Get the vacancy
        restVacancyMockMvc.perform(get("/api/vacancies/{id}", vacancy.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(vacancy.getId().intValue()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.creationDate").value(DEFAULT_CREATION_DATE.toString()))
            .andExpect(jsonPath("$.vacancyStatus").value(DEFAULT_VACANCY_STATUS.toString()))
            .andExpect(jsonPath("$.characteristics").value(DEFAULT_CHARACTERISTICS.toString()))
            .andExpect(jsonPath("$.skills").value(DEFAULT_SKILLS.toString()))
            .andExpect(jsonPath("$.dailyRoutine").value(DEFAULT_DAILY_ROUTINE.toString()))
            .andExpect(jsonPath("$.location").value(DEFAULT_LOCATION.toString()))
            .andExpect(jsonPath("$.candidateTypes").value(DEFAULT_CANDIDATE_TYPES.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingVacancy() throws Exception {
        // Get the vacancy
        restVacancyMockMvc.perform(get("/api/vacancies/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateVacancy() throws Exception {
        // Initialize the database
        vacancyRepository.saveAndFlush(vacancy);

        int databaseSizeBeforeUpdate = vacancyRepository.findAll().size();

        // Update the vacancy
        Vacancy updatedVacancy = vacancyRepository.findById(vacancy.getId()).get();
        // Disconnect from session so that the updates on updatedVacancy are not directly saved in db
        em.detach(updatedVacancy);
        updatedVacancy
            .code(UPDATED_CODE)
            .description(UPDATED_DESCRIPTION)
            .creationDate(UPDATED_CREATION_DATE)
            .vacancyStatus(UPDATED_VACANCY_STATUS)
            .characteristics(UPDATED_CHARACTERISTICS)
            .skills(UPDATED_SKILLS)
            .dailyRoutine(UPDATED_DAILY_ROUTINE)
            .location(UPDATED_LOCATION)
            .candidateTypes(UPDATED_CANDIDATE_TYPES);
        VacancyDTO vacancyDTO = vacancyMapper.toDto(updatedVacancy);

        restVacancyMockMvc.perform(put("/api/vacancies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(vacancyDTO)))
            .andExpect(status().isOk());

        // Validate the Vacancy in the database
        List<Vacancy> vacancyList = vacancyRepository.findAll();
        assertThat(vacancyList).hasSize(databaseSizeBeforeUpdate);
        Vacancy testVacancy = vacancyList.get(vacancyList.size() - 1);
        assertThat(testVacancy.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testVacancy.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testVacancy.getCreationDate()).isEqualTo(UPDATED_CREATION_DATE);
        assertThat(testVacancy.getVacancyStatus()).isEqualTo(UPDATED_VACANCY_STATUS);
        assertThat(testVacancy.getCharacteristics()).isEqualTo(UPDATED_CHARACTERISTICS);
        assertThat(testVacancy.getSkills()).isEqualTo(UPDATED_SKILLS);
        assertThat(testVacancy.getDailyRoutine()).isEqualTo(UPDATED_DAILY_ROUTINE);
        assertThat(testVacancy.getLocation()).isEqualTo(UPDATED_LOCATION);
        assertThat(testVacancy.getCandidateTypes()).isEqualTo(UPDATED_CANDIDATE_TYPES);
    }

    @Test
    @Transactional
    public void updateNonExistingVacancy() throws Exception {
        int databaseSizeBeforeUpdate = vacancyRepository.findAll().size();

        // Create the Vacancy
        VacancyDTO vacancyDTO = vacancyMapper.toDto(vacancy);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restVacancyMockMvc.perform(put("/api/vacancies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(vacancyDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Vacancy in the database
        List<Vacancy> vacancyList = vacancyRepository.findAll();
        assertThat(vacancyList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteVacancy() throws Exception {
        // Initialize the database
        vacancyRepository.saveAndFlush(vacancy);

        int databaseSizeBeforeDelete = vacancyRepository.findAll().size();

        // Delete the vacancy
        restVacancyMockMvc.perform(delete("/api/vacancies/{id}", vacancy.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<Vacancy> vacancyList = vacancyRepository.findAll();
        assertThat(vacancyList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Vacancy.class);
        Vacancy vacancy1 = new Vacancy();
        vacancy1.setId(1L);
        Vacancy vacancy2 = new Vacancy();
        vacancy2.setId(vacancy1.getId());
        assertThat(vacancy1).isEqualTo(vacancy2);
        vacancy2.setId(2L);
        assertThat(vacancy1).isNotEqualTo(vacancy2);
        vacancy1.setId(null);
        assertThat(vacancy1).isNotEqualTo(vacancy2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(VacancyDTO.class);
        VacancyDTO vacancyDTO1 = new VacancyDTO();
        vacancyDTO1.setId(1L);
        VacancyDTO vacancyDTO2 = new VacancyDTO();
        assertThat(vacancyDTO1).isNotEqualTo(vacancyDTO2);
        vacancyDTO2.setId(vacancyDTO1.getId());
        assertThat(vacancyDTO1).isEqualTo(vacancyDTO2);
        vacancyDTO2.setId(2L);
        assertThat(vacancyDTO1).isNotEqualTo(vacancyDTO2);
        vacancyDTO1.setId(null);
        assertThat(vacancyDTO1).isNotEqualTo(vacancyDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(vacancyMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(vacancyMapper.fromId(null)).isNull();
    }
}
