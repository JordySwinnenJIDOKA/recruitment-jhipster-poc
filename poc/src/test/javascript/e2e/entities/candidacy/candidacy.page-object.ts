import { browser, ExpectedConditions, element, by, ElementFinder } from 'protractor';

export class CandidacyComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-candidacy div table .btn-danger'));
  title = element.all(by.css('jhi-candidacy div h2#page-heading span')).first();

  async clickOnCreateButton(timeout?: number) {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(timeout?: number) {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons() {
    return this.deleteButtons.count();
  }

  async getTitle() {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class CandidacyUpdatePage {
  pageTitle = element(by.id('jhi-candidacy-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  candidacyStatusSelect = element(by.id('field_candidacyStatus'));
  motivationInput = element(by.id('field_motivation'));
  candidateSelect = element(by.id('field_candidate'));
  vacancySelect = element(by.id('field_vacancy'));

  async getPageTitle() {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setCandidacyStatusSelect(candidacyStatus) {
    await this.candidacyStatusSelect.sendKeys(candidacyStatus);
  }

  async getCandidacyStatusSelect() {
    return await this.candidacyStatusSelect.element(by.css('option:checked')).getText();
  }

  async candidacyStatusSelectLastOption(timeout?: number) {
    await this.candidacyStatusSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async setMotivationInput(motivation) {
    await this.motivationInput.sendKeys(motivation);
  }

  async getMotivationInput() {
    return await this.motivationInput.getAttribute('value');
  }

  async candidateSelectLastOption(timeout?: number) {
    await this.candidateSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async candidateSelectOption(option) {
    await this.candidateSelect.sendKeys(option);
  }

  getCandidateSelect(): ElementFinder {
    return this.candidateSelect;
  }

  async getCandidateSelectedOption() {
    return await this.candidateSelect.element(by.css('option:checked')).getText();
  }

  async vacancySelectLastOption(timeout?: number) {
    await this.vacancySelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async vacancySelectOption(option) {
    await this.vacancySelect.sendKeys(option);
  }

  getVacancySelect(): ElementFinder {
    return this.vacancySelect;
  }

  async getVacancySelectedOption() {
    return await this.vacancySelect.element(by.css('option:checked')).getText();
  }

  async save(timeout?: number) {
    await this.saveButton.click();
  }

  async cancel(timeout?: number) {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class CandidacyDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-candidacy-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-candidacy'));

  async getDialogTitle() {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(timeout?: number) {
    await this.confirmButton.click();
  }
}
