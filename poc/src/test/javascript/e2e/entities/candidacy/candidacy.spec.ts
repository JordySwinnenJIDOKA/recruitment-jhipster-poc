/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { CandidacyComponentsPage, CandidacyDeleteDialog, CandidacyUpdatePage } from './candidacy.page-object';

const expect = chai.expect;

describe('Candidacy e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let candidacyUpdatePage: CandidacyUpdatePage;
  let candidacyComponentsPage: CandidacyComponentsPage;
  let candidacyDeleteDialog: CandidacyDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.loginWithOAuth('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Candidacies', async () => {
    await navBarPage.goToEntity('candidacy');
    candidacyComponentsPage = new CandidacyComponentsPage();
    await browser.wait(ec.visibilityOf(candidacyComponentsPage.title), 5000);
    expect(await candidacyComponentsPage.getTitle()).to.eq('jhipsterRecruitmentApp.candidacy.home.title');
  });

  it('should load create Candidacy page', async () => {
    await candidacyComponentsPage.clickOnCreateButton();
    candidacyUpdatePage = new CandidacyUpdatePage();
    expect(await candidacyUpdatePage.getPageTitle()).to.eq('jhipsterRecruitmentApp.candidacy.home.createOrEditLabel');
    await candidacyUpdatePage.cancel();
  });

  it('should create and save Candidacies', async () => {
    const nbButtonsBeforeCreate = await candidacyComponentsPage.countDeleteButtons();

    await candidacyComponentsPage.clickOnCreateButton();
    await promise.all([
      candidacyUpdatePage.candidacyStatusSelectLastOption(),
      candidacyUpdatePage.setMotivationInput('motivation'),
      candidacyUpdatePage.candidateSelectLastOption(),
      candidacyUpdatePage.vacancySelectLastOption()
    ]);
    expect(await candidacyUpdatePage.getMotivationInput()).to.eq('motivation', 'Expected Motivation value to be equals to motivation');
    await candidacyUpdatePage.save();
    expect(await candidacyUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await candidacyComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Candidacy', async () => {
    const nbButtonsBeforeDelete = await candidacyComponentsPage.countDeleteButtons();
    await candidacyComponentsPage.clickOnLastDeleteButton();

    candidacyDeleteDialog = new CandidacyDeleteDialog();
    expect(await candidacyDeleteDialog.getDialogTitle()).to.eq('jhipsterRecruitmentApp.candidacy.delete.question');
    await candidacyDeleteDialog.clickOnConfirmButton();

    expect(await candidacyComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
