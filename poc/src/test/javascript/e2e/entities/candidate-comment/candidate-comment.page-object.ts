import { browser, ExpectedConditions, element, by, ElementFinder } from 'protractor';

export class CandidateCommentComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-candidate-comment div table .btn-danger'));
  title = element.all(by.css('jhi-candidate-comment div h2#page-heading span')).first();

  async clickOnCreateButton(timeout?: number) {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(timeout?: number) {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons() {
    return this.deleteButtons.count();
  }

  async getTitle() {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class CandidateCommentUpdatePage {
  pageTitle = element(by.id('jhi-candidate-comment-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  contextInput = element(by.id('field_context'));
  commenterInput = element(by.id('field_commenter'));
  candidateSelect = element(by.id('field_candidate'));

  async getPageTitle() {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setContextInput(context) {
    await this.contextInput.sendKeys(context);
  }

  async getContextInput() {
    return await this.contextInput.getAttribute('value');
  }

  async setCommenterInput(commenter) {
    await this.commenterInput.sendKeys(commenter);
  }

  async getCommenterInput() {
    return await this.commenterInput.getAttribute('value');
  }

  async candidateSelectLastOption(timeout?: number) {
    await this.candidateSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async candidateSelectOption(option) {
    await this.candidateSelect.sendKeys(option);
  }

  getCandidateSelect(): ElementFinder {
    return this.candidateSelect;
  }

  async getCandidateSelectedOption() {
    return await this.candidateSelect.element(by.css('option:checked')).getText();
  }

  async save(timeout?: number) {
    await this.saveButton.click();
  }

  async cancel(timeout?: number) {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class CandidateCommentDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-candidateComment-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-candidateComment'));

  async getDialogTitle() {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(timeout?: number) {
    await this.confirmButton.click();
  }
}
