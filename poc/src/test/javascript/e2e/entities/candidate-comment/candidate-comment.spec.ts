/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { CandidateCommentComponentsPage, CandidateCommentDeleteDialog, CandidateCommentUpdatePage } from './candidate-comment.page-object';

const expect = chai.expect;

describe('CandidateComment e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let candidateCommentUpdatePage: CandidateCommentUpdatePage;
  let candidateCommentComponentsPage: CandidateCommentComponentsPage;
  let candidateCommentDeleteDialog: CandidateCommentDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.loginWithOAuth('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load CandidateComments', async () => {
    await navBarPage.goToEntity('candidate-comment');
    candidateCommentComponentsPage = new CandidateCommentComponentsPage();
    await browser.wait(ec.visibilityOf(candidateCommentComponentsPage.title), 5000);
    expect(await candidateCommentComponentsPage.getTitle()).to.eq('jhipsterRecruitmentApp.candidateComment.home.title');
  });

  it('should load create CandidateComment page', async () => {
    await candidateCommentComponentsPage.clickOnCreateButton();
    candidateCommentUpdatePage = new CandidateCommentUpdatePage();
    expect(await candidateCommentUpdatePage.getPageTitle()).to.eq('jhipsterRecruitmentApp.candidateComment.home.createOrEditLabel');
    await candidateCommentUpdatePage.cancel();
  });

  it('should create and save CandidateComments', async () => {
    const nbButtonsBeforeCreate = await candidateCommentComponentsPage.countDeleteButtons();

    await candidateCommentComponentsPage.clickOnCreateButton();
    await promise.all([
      candidateCommentUpdatePage.setContextInput('context'),
      candidateCommentUpdatePage.setCommenterInput('commenter'),
      candidateCommentUpdatePage.candidateSelectLastOption()
    ]);
    expect(await candidateCommentUpdatePage.getContextInput()).to.eq('context', 'Expected Context value to be equals to context');
    expect(await candidateCommentUpdatePage.getCommenterInput()).to.eq('commenter', 'Expected Commenter value to be equals to commenter');
    await candidateCommentUpdatePage.save();
    expect(await candidateCommentUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await candidateCommentComponentsPage.countDeleteButtons()).to.eq(
      nbButtonsBeforeCreate + 1,
      'Expected one more entry in the table'
    );
  });

  it('should delete last CandidateComment', async () => {
    const nbButtonsBeforeDelete = await candidateCommentComponentsPage.countDeleteButtons();
    await candidateCommentComponentsPage.clickOnLastDeleteButton();

    candidateCommentDeleteDialog = new CandidateCommentDeleteDialog();
    expect(await candidateCommentDeleteDialog.getDialogTitle()).to.eq('jhipsterRecruitmentApp.candidateComment.delete.question');
    await candidateCommentDeleteDialog.clickOnConfirmButton();

    expect(await candidateCommentComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
