import { browser, ExpectedConditions, element, by, ElementFinder } from 'protractor';

export class CandidateSubClassComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-candidate-sub-class div table .btn-danger'));
  title = element.all(by.css('jhi-candidate-sub-class div h2#page-heading span')).first();

  async clickOnCreateButton(timeout?: number) {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(timeout?: number) {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons() {
    return this.deleteButtons.count();
  }

  async getTitle() {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class CandidateSubClassUpdatePage {
  pageTitle = element(by.id('jhi-candidate-sub-class-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  candidateTypeSelect = element(by.id('field_candidateType'));

  async getPageTitle() {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setCandidateTypeSelect(candidateType) {
    await this.candidateTypeSelect.sendKeys(candidateType);
  }

  async getCandidateTypeSelect() {
    return await this.candidateTypeSelect.element(by.css('option:checked')).getText();
  }

  async candidateTypeSelectLastOption(timeout?: number) {
    await this.candidateTypeSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async save(timeout?: number) {
    await this.saveButton.click();
  }

  async cancel(timeout?: number) {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class CandidateSubClassDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-candidateSubClass-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-candidateSubClass'));

  async getDialogTitle() {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(timeout?: number) {
    await this.confirmButton.click();
  }
}
