/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import {
  CandidateSubClassComponentsPage,
  CandidateSubClassDeleteDialog,
  CandidateSubClassUpdatePage
} from './candidate-sub-class.page-object';

const expect = chai.expect;

describe('CandidateSubClass e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let candidateSubClassUpdatePage: CandidateSubClassUpdatePage;
  let candidateSubClassComponentsPage: CandidateSubClassComponentsPage;
  let candidateSubClassDeleteDialog: CandidateSubClassDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.loginWithOAuth('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load CandidateSubClasses', async () => {
    await navBarPage.goToEntity('candidate-sub-class');
    candidateSubClassComponentsPage = new CandidateSubClassComponentsPage();
    await browser.wait(ec.visibilityOf(candidateSubClassComponentsPage.title), 5000);
    expect(await candidateSubClassComponentsPage.getTitle()).to.eq('jhipsterRecruitmentApp.candidateSubClass.home.title');
  });

  it('should load create CandidateSubClass page', async () => {
    await candidateSubClassComponentsPage.clickOnCreateButton();
    candidateSubClassUpdatePage = new CandidateSubClassUpdatePage();
    expect(await candidateSubClassUpdatePage.getPageTitle()).to.eq('jhipsterRecruitmentApp.candidateSubClass.home.createOrEditLabel');
    await candidateSubClassUpdatePage.cancel();
  });

  it('should create and save CandidateSubClasses', async () => {
    const nbButtonsBeforeCreate = await candidateSubClassComponentsPage.countDeleteButtons();

    await candidateSubClassComponentsPage.clickOnCreateButton();
    await promise.all([candidateSubClassUpdatePage.candidateTypeSelectLastOption()]);
    await candidateSubClassUpdatePage.save();
    expect(await candidateSubClassUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await candidateSubClassComponentsPage.countDeleteButtons()).to.eq(
      nbButtonsBeforeCreate + 1,
      'Expected one more entry in the table'
    );
  });

  it('should delete last CandidateSubClass', async () => {
    const nbButtonsBeforeDelete = await candidateSubClassComponentsPage.countDeleteButtons();
    await candidateSubClassComponentsPage.clickOnLastDeleteButton();

    candidateSubClassDeleteDialog = new CandidateSubClassDeleteDialog();
    expect(await candidateSubClassDeleteDialog.getDialogTitle()).to.eq('jhipsterRecruitmentApp.candidateSubClass.delete.question');
    await candidateSubClassDeleteDialog.clickOnConfirmButton();

    expect(await candidateSubClassComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
