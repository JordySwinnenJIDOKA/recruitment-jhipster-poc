import { browser, ExpectedConditions, element, by, ElementFinder } from 'protractor';

export class CandidateComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-candidate div table .btn-danger'));
  title = element.all(by.css('jhi-candidate div h2#page-heading span')).first();

  async clickOnCreateButton(timeout?: number) {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(timeout?: number) {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons() {
    return this.deleteButtons.count();
  }

  async getTitle() {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class CandidateUpdatePage {
  pageTitle = element(by.id('jhi-candidate-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  nameInput = element(by.id('field_name'));
  phoneInput = element(by.id('field_phone'));
  emailInput = element(by.id('field_email'));
  candidateStatusSelect = element(by.id('field_candidateStatus'));
  candidateFunctionSelect = element(by.id('field_candidateFunction'));
  dateAvailableInput = element(by.id('field_dateAvailable'));
  imageContentInput = element(by.id('file_imageContent'));
  candidateTrackSelect = element(by.id('field_candidateTrack'));
  recruitmentChannelSelect = element(by.id('field_recruitmentChannel'));
  linkedInProfilePathInput = element(by.id('field_linkedInProfilePath'));
  documentSelect = element(by.id('field_document'));
  subClassSelect = element(by.id('field_subClass'));

  async getPageTitle() {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setNameInput(name) {
    await this.nameInput.sendKeys(name);
  }

  async getNameInput() {
    return await this.nameInput.getAttribute('value');
  }

  async setPhoneInput(phone) {
    await this.phoneInput.sendKeys(phone);
  }

  async getPhoneInput() {
    return await this.phoneInput.getAttribute('value');
  }

  async setEmailInput(email) {
    await this.emailInput.sendKeys(email);
  }

  async getEmailInput() {
    return await this.emailInput.getAttribute('value');
  }

  async setCandidateStatusSelect(candidateStatus) {
    await this.candidateStatusSelect.sendKeys(candidateStatus);
  }

  async getCandidateStatusSelect() {
    return await this.candidateStatusSelect.element(by.css('option:checked')).getText();
  }

  async candidateStatusSelectLastOption(timeout?: number) {
    await this.candidateStatusSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async setCandidateFunctionSelect(candidateFunction) {
    await this.candidateFunctionSelect.sendKeys(candidateFunction);
  }

  async getCandidateFunctionSelect() {
    return await this.candidateFunctionSelect.element(by.css('option:checked')).getText();
  }

  async candidateFunctionSelectLastOption(timeout?: number) {
    await this.candidateFunctionSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async setDateAvailableInput(dateAvailable) {
    await this.dateAvailableInput.sendKeys(dateAvailable);
  }

  async getDateAvailableInput() {
    return await this.dateAvailableInput.getAttribute('value');
  }

  async setImageContentInput(imageContent) {
    await this.imageContentInput.sendKeys(imageContent);
  }

  async getImageContentInput() {
    return await this.imageContentInput.getAttribute('value');
  }

  async setCandidateTrackSelect(candidateTrack) {
    await this.candidateTrackSelect.sendKeys(candidateTrack);
  }

  async getCandidateTrackSelect() {
    return await this.candidateTrackSelect.element(by.css('option:checked')).getText();
  }

  async candidateTrackSelectLastOption(timeout?: number) {
    await this.candidateTrackSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async setRecruitmentChannelSelect(recruitmentChannel) {
    await this.recruitmentChannelSelect.sendKeys(recruitmentChannel);
  }

  async getRecruitmentChannelSelect() {
    return await this.recruitmentChannelSelect.element(by.css('option:checked')).getText();
  }

  async recruitmentChannelSelectLastOption(timeout?: number) {
    await this.recruitmentChannelSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async setLinkedInProfilePathInput(linkedInProfilePath) {
    await this.linkedInProfilePathInput.sendKeys(linkedInProfilePath);
  }

  async getLinkedInProfilePathInput() {
    return await this.linkedInProfilePathInput.getAttribute('value');
  }

  async documentSelectLastOption(timeout?: number) {
    await this.documentSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async documentSelectOption(option) {
    await this.documentSelect.sendKeys(option);
  }

  getDocumentSelect(): ElementFinder {
    return this.documentSelect;
  }

  async getDocumentSelectedOption() {
    return await this.documentSelect.element(by.css('option:checked')).getText();
  }

  async subClassSelectLastOption(timeout?: number) {
    await this.subClassSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async subClassSelectOption(option) {
    await this.subClassSelect.sendKeys(option);
  }

  getSubClassSelect(): ElementFinder {
    return this.subClassSelect;
  }

  async getSubClassSelectedOption() {
    return await this.subClassSelect.element(by.css('option:checked')).getText();
  }

  async save(timeout?: number) {
    await this.saveButton.click();
  }

  async cancel(timeout?: number) {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class CandidateDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-candidate-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-candidate'));

  async getDialogTitle() {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(timeout?: number) {
    await this.confirmButton.click();
  }
}
