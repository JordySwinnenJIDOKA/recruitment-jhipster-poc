/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { CandidateComponentsPage, CandidateDeleteDialog, CandidateUpdatePage } from './candidate.page-object';
import * as path from 'path';

const expect = chai.expect;

describe('Candidate e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let candidateUpdatePage: CandidateUpdatePage;
  let candidateComponentsPage: CandidateComponentsPage;
  let candidateDeleteDialog: CandidateDeleteDialog;
  const fileNameToUpload = 'logo-jhipster.png';
  const fileToUpload = '../../../../../../src/main/webapp/content/images/' + fileNameToUpload;
  const absolutePath = path.resolve(__dirname, fileToUpload);

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.loginWithOAuth('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Candidates', async () => {
    await navBarPage.goToEntity('candidate');
    candidateComponentsPage = new CandidateComponentsPage();
    await browser.wait(ec.visibilityOf(candidateComponentsPage.title), 5000);
    expect(await candidateComponentsPage.getTitle()).to.eq('jhipsterRecruitmentApp.candidate.home.title');
  });

  it('should load create Candidate page', async () => {
    await candidateComponentsPage.clickOnCreateButton();
    candidateUpdatePage = new CandidateUpdatePage();
    expect(await candidateUpdatePage.getPageTitle()).to.eq('jhipsterRecruitmentApp.candidate.home.createOrEditLabel');
    await candidateUpdatePage.cancel();
  });

  it('should create and save Candidates', async () => {
    const nbButtonsBeforeCreate = await candidateComponentsPage.countDeleteButtons();

    await candidateComponentsPage.clickOnCreateButton();
    await promise.all([
      candidateUpdatePage.setNameInput('name'),
      candidateUpdatePage.setPhoneInput('phone'),
      candidateUpdatePage.setEmailInput('email'),
      candidateUpdatePage.candidateStatusSelectLastOption(),
      candidateUpdatePage.candidateFunctionSelectLastOption(),
      candidateUpdatePage.setDateAvailableInput('dateAvailable'),
      candidateUpdatePage.setImageContentInput(absolutePath),
      candidateUpdatePage.candidateTrackSelectLastOption(),
      candidateUpdatePage.recruitmentChannelSelectLastOption(),
      candidateUpdatePage.setLinkedInProfilePathInput('linkedInProfilePath'),
      candidateUpdatePage.documentSelectLastOption(),
      candidateUpdatePage.subClassSelectLastOption()
    ]);
    expect(await candidateUpdatePage.getNameInput()).to.eq('name', 'Expected Name value to be equals to name');
    expect(await candidateUpdatePage.getPhoneInput()).to.eq('phone', 'Expected Phone value to be equals to phone');
    expect(await candidateUpdatePage.getEmailInput()).to.eq('email', 'Expected Email value to be equals to email');
    expect(await candidateUpdatePage.getDateAvailableInput()).to.eq(
      'dateAvailable',
      'Expected DateAvailable value to be equals to dateAvailable'
    );
    expect(await candidateUpdatePage.getImageContentInput()).to.endsWith(
      fileNameToUpload,
      'Expected ImageContent value to be end with ' + fileNameToUpload
    );
    expect(await candidateUpdatePage.getLinkedInProfilePathInput()).to.eq(
      'linkedInProfilePath',
      'Expected LinkedInProfilePath value to be equals to linkedInProfilePath'
    );
    await candidateUpdatePage.save();
    expect(await candidateUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await candidateComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Candidate', async () => {
    const nbButtonsBeforeDelete = await candidateComponentsPage.countDeleteButtons();
    await candidateComponentsPage.clickOnLastDeleteButton();

    candidateDeleteDialog = new CandidateDeleteDialog();
    expect(await candidateDeleteDialog.getDialogTitle()).to.eq('jhipsterRecruitmentApp.candidate.delete.question');
    await candidateDeleteDialog.clickOnConfirmButton();

    expect(await candidateComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
