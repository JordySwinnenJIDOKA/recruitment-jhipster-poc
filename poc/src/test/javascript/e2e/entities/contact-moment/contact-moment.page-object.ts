import { browser, ExpectedConditions, element, by, ElementFinder } from 'protractor';

export class ContactMomentComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-contact-moment div table .btn-danger'));
  title = element.all(by.css('jhi-contact-moment div h2#page-heading span')).first();

  async clickOnCreateButton(timeout?: number) {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(timeout?: number) {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons() {
    return this.deleteButtons.count();
  }

  async getTitle() {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class ContactMomentUpdatePage {
  pageTitle = element(by.id('jhi-contact-moment-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  contactTypeSelect = element(by.id('field_contactType'));
  contactDateInput = element(by.id('field_contactDate'));
  contactContextSelect = element(by.id('field_contactContext'));
  interviewerInput = element(by.id('field_interviewer'));
  locationInput = element(by.id('field_location'));
  candidacySelect = element(by.id('field_candidacy'));
  candidateSelect = element(by.id('field_candidate'));

  async getPageTitle() {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setContactTypeSelect(contactType) {
    await this.contactTypeSelect.sendKeys(contactType);
  }

  async getContactTypeSelect() {
    return await this.contactTypeSelect.element(by.css('option:checked')).getText();
  }

  async contactTypeSelectLastOption(timeout?: number) {
    await this.contactTypeSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async setContactDateInput(contactDate) {
    await this.contactDateInput.sendKeys(contactDate);
  }

  async getContactDateInput() {
    return await this.contactDateInput.getAttribute('value');
  }

  async setContactContextSelect(contactContext) {
    await this.contactContextSelect.sendKeys(contactContext);
  }

  async getContactContextSelect() {
    return await this.contactContextSelect.element(by.css('option:checked')).getText();
  }

  async contactContextSelectLastOption(timeout?: number) {
    await this.contactContextSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async setInterviewerInput(interviewer) {
    await this.interviewerInput.sendKeys(interviewer);
  }

  async getInterviewerInput() {
    return await this.interviewerInput.getAttribute('value');
  }

  async setLocationInput(location) {
    await this.locationInput.sendKeys(location);
  }

  async getLocationInput() {
    return await this.locationInput.getAttribute('value');
  }

  async candidacySelectLastOption(timeout?: number) {
    await this.candidacySelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async candidacySelectOption(option) {
    await this.candidacySelect.sendKeys(option);
  }

  getCandidacySelect(): ElementFinder {
    return this.candidacySelect;
  }

  async getCandidacySelectedOption() {
    return await this.candidacySelect.element(by.css('option:checked')).getText();
  }

  async candidateSelectLastOption(timeout?: number) {
    await this.candidateSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async candidateSelectOption(option) {
    await this.candidateSelect.sendKeys(option);
  }

  getCandidateSelect(): ElementFinder {
    return this.candidateSelect;
  }

  async getCandidateSelectedOption() {
    return await this.candidateSelect.element(by.css('option:checked')).getText();
  }

  async save(timeout?: number) {
    await this.saveButton.click();
  }

  async cancel(timeout?: number) {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class ContactMomentDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-contactMoment-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-contactMoment'));

  async getDialogTitle() {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(timeout?: number) {
    await this.confirmButton.click();
  }
}
