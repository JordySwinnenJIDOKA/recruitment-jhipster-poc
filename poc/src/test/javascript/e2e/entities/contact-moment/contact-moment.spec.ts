/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { ContactMomentComponentsPage, ContactMomentDeleteDialog, ContactMomentUpdatePage } from './contact-moment.page-object';

const expect = chai.expect;

describe('ContactMoment e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let contactMomentUpdatePage: ContactMomentUpdatePage;
  let contactMomentComponentsPage: ContactMomentComponentsPage;
  let contactMomentDeleteDialog: ContactMomentDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.loginWithOAuth('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load ContactMoments', async () => {
    await navBarPage.goToEntity('contact-moment');
    contactMomentComponentsPage = new ContactMomentComponentsPage();
    await browser.wait(ec.visibilityOf(contactMomentComponentsPage.title), 5000);
    expect(await contactMomentComponentsPage.getTitle()).to.eq('jhipsterRecruitmentApp.contactMoment.home.title');
  });

  it('should load create ContactMoment page', async () => {
    await contactMomentComponentsPage.clickOnCreateButton();
    contactMomentUpdatePage = new ContactMomentUpdatePage();
    expect(await contactMomentUpdatePage.getPageTitle()).to.eq('jhipsterRecruitmentApp.contactMoment.home.createOrEditLabel');
    await contactMomentUpdatePage.cancel();
  });

  it('should create and save ContactMoments', async () => {
    const nbButtonsBeforeCreate = await contactMomentComponentsPage.countDeleteButtons();

    await contactMomentComponentsPage.clickOnCreateButton();
    await promise.all([
      contactMomentUpdatePage.contactTypeSelectLastOption(),
      contactMomentUpdatePage.setContactDateInput('2000-12-31'),
      contactMomentUpdatePage.contactContextSelectLastOption(),
      contactMomentUpdatePage.setInterviewerInput('interviewer'),
      contactMomentUpdatePage.setLocationInput('location'),
      contactMomentUpdatePage.candidacySelectLastOption(),
      contactMomentUpdatePage.candidateSelectLastOption()
    ]);
    expect(await contactMomentUpdatePage.getContactDateInput()).to.eq(
      '2000-12-31',
      'Expected contactDate value to be equals to 2000-12-31'
    );
    expect(await contactMomentUpdatePage.getInterviewerInput()).to.eq(
      'interviewer',
      'Expected Interviewer value to be equals to interviewer'
    );
    expect(await contactMomentUpdatePage.getLocationInput()).to.eq('location', 'Expected Location value to be equals to location');
    await contactMomentUpdatePage.save();
    expect(await contactMomentUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await contactMomentComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last ContactMoment', async () => {
    const nbButtonsBeforeDelete = await contactMomentComponentsPage.countDeleteButtons();
    await contactMomentComponentsPage.clickOnLastDeleteButton();

    contactMomentDeleteDialog = new ContactMomentDeleteDialog();
    expect(await contactMomentDeleteDialog.getDialogTitle()).to.eq('jhipsterRecruitmentApp.contactMoment.delete.question');
    await contactMomentDeleteDialog.clickOnConfirmButton();

    expect(await contactMomentComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
