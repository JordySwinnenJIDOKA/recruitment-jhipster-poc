import { browser, ExpectedConditions, element, by, ElementFinder } from 'protractor';

export class FeedbackAnswerComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-feedback-answer div table .btn-danger'));
  title = element.all(by.css('jhi-feedback-answer div h2#page-heading span')).first();

  async clickOnCreateButton(timeout?: number) {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(timeout?: number) {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons() {
    return this.deleteButtons.count();
  }

  async getTitle() {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class FeedbackAnswerUpdatePage {
  pageTitle = element(by.id('jhi-feedback-answer-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  answerInput = element(by.id('field_answer'));
  feedbackQuestionSelect = element(by.id('field_feedbackQuestion'));
  candidacySelect = element(by.id('field_candidacy'));

  async getPageTitle() {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setAnswerInput(answer) {
    await this.answerInput.sendKeys(answer);
  }

  async getAnswerInput() {
    return await this.answerInput.getAttribute('value');
  }

  async feedbackQuestionSelectLastOption(timeout?: number) {
    await this.feedbackQuestionSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async feedbackQuestionSelectOption(option) {
    await this.feedbackQuestionSelect.sendKeys(option);
  }

  getFeedbackQuestionSelect(): ElementFinder {
    return this.feedbackQuestionSelect;
  }

  async getFeedbackQuestionSelectedOption() {
    return await this.feedbackQuestionSelect.element(by.css('option:checked')).getText();
  }

  async candidacySelectLastOption(timeout?: number) {
    await this.candidacySelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async candidacySelectOption(option) {
    await this.candidacySelect.sendKeys(option);
  }

  getCandidacySelect(): ElementFinder {
    return this.candidacySelect;
  }

  async getCandidacySelectedOption() {
    return await this.candidacySelect.element(by.css('option:checked')).getText();
  }

  async save(timeout?: number) {
    await this.saveButton.click();
  }

  async cancel(timeout?: number) {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class FeedbackAnswerDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-feedbackAnswer-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-feedbackAnswer'));

  async getDialogTitle() {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(timeout?: number) {
    await this.confirmButton.click();
  }
}
