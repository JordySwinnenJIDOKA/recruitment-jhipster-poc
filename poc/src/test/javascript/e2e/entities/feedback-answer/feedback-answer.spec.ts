/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { FeedbackAnswerComponentsPage, FeedbackAnswerDeleteDialog, FeedbackAnswerUpdatePage } from './feedback-answer.page-object';

const expect = chai.expect;

describe('FeedbackAnswer e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let feedbackAnswerUpdatePage: FeedbackAnswerUpdatePage;
  let feedbackAnswerComponentsPage: FeedbackAnswerComponentsPage;
  let feedbackAnswerDeleteDialog: FeedbackAnswerDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.loginWithOAuth('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load FeedbackAnswers', async () => {
    await navBarPage.goToEntity('feedback-answer');
    feedbackAnswerComponentsPage = new FeedbackAnswerComponentsPage();
    await browser.wait(ec.visibilityOf(feedbackAnswerComponentsPage.title), 5000);
    expect(await feedbackAnswerComponentsPage.getTitle()).to.eq('jhipsterRecruitmentApp.feedbackAnswer.home.title');
  });

  it('should load create FeedbackAnswer page', async () => {
    await feedbackAnswerComponentsPage.clickOnCreateButton();
    feedbackAnswerUpdatePage = new FeedbackAnswerUpdatePage();
    expect(await feedbackAnswerUpdatePage.getPageTitle()).to.eq('jhipsterRecruitmentApp.feedbackAnswer.home.createOrEditLabel');
    await feedbackAnswerUpdatePage.cancel();
  });

  it('should create and save FeedbackAnswers', async () => {
    const nbButtonsBeforeCreate = await feedbackAnswerComponentsPage.countDeleteButtons();

    await feedbackAnswerComponentsPage.clickOnCreateButton();
    await promise.all([
      feedbackAnswerUpdatePage.setAnswerInput('answer'),
      feedbackAnswerUpdatePage.feedbackQuestionSelectLastOption(),
      feedbackAnswerUpdatePage.candidacySelectLastOption()
    ]);
    expect(await feedbackAnswerUpdatePage.getAnswerInput()).to.eq('answer', 'Expected Answer value to be equals to answer');
    await feedbackAnswerUpdatePage.save();
    expect(await feedbackAnswerUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await feedbackAnswerComponentsPage.countDeleteButtons()).to.eq(
      nbButtonsBeforeCreate + 1,
      'Expected one more entry in the table'
    );
  });

  it('should delete last FeedbackAnswer', async () => {
    const nbButtonsBeforeDelete = await feedbackAnswerComponentsPage.countDeleteButtons();
    await feedbackAnswerComponentsPage.clickOnLastDeleteButton();

    feedbackAnswerDeleteDialog = new FeedbackAnswerDeleteDialog();
    expect(await feedbackAnswerDeleteDialog.getDialogTitle()).to.eq('jhipsterRecruitmentApp.feedbackAnswer.delete.question');
    await feedbackAnswerDeleteDialog.clickOnConfirmButton();

    expect(await feedbackAnswerComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
