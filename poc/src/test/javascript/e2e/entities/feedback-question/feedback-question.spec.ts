/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { FeedbackQuestionComponentsPage, FeedbackQuestionDeleteDialog, FeedbackQuestionUpdatePage } from './feedback-question.page-object';

const expect = chai.expect;

describe('FeedbackQuestion e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let feedbackQuestionUpdatePage: FeedbackQuestionUpdatePage;
  let feedbackQuestionComponentsPage: FeedbackQuestionComponentsPage;
  let feedbackQuestionDeleteDialog: FeedbackQuestionDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.loginWithOAuth('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load FeedbackQuestions', async () => {
    await navBarPage.goToEntity('feedback-question');
    feedbackQuestionComponentsPage = new FeedbackQuestionComponentsPage();
    await browser.wait(ec.visibilityOf(feedbackQuestionComponentsPage.title), 5000);
    expect(await feedbackQuestionComponentsPage.getTitle()).to.eq('jhipsterRecruitmentApp.feedbackQuestion.home.title');
  });

  it('should load create FeedbackQuestion page', async () => {
    await feedbackQuestionComponentsPage.clickOnCreateButton();
    feedbackQuestionUpdatePage = new FeedbackQuestionUpdatePage();
    expect(await feedbackQuestionUpdatePage.getPageTitle()).to.eq('jhipsterRecruitmentApp.feedbackQuestion.home.createOrEditLabel');
    await feedbackQuestionUpdatePage.cancel();
  });

  it('should create and save FeedbackQuestions', async () => {
    const nbButtonsBeforeCreate = await feedbackQuestionComponentsPage.countDeleteButtons();

    await feedbackQuestionComponentsPage.clickOnCreateButton();
    await promise.all([feedbackQuestionUpdatePage.setQuestionInput('question')]);
    expect(await feedbackQuestionUpdatePage.getQuestionInput()).to.eq('question', 'Expected Question value to be equals to question');
    await feedbackQuestionUpdatePage.save();
    expect(await feedbackQuestionUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await feedbackQuestionComponentsPage.countDeleteButtons()).to.eq(
      nbButtonsBeforeCreate + 1,
      'Expected one more entry in the table'
    );
  });

  it('should delete last FeedbackQuestion', async () => {
    const nbButtonsBeforeDelete = await feedbackQuestionComponentsPage.countDeleteButtons();
    await feedbackQuestionComponentsPage.clickOnLastDeleteButton();

    feedbackQuestionDeleteDialog = new FeedbackQuestionDeleteDialog();
    expect(await feedbackQuestionDeleteDialog.getDialogTitle()).to.eq('jhipsterRecruitmentApp.feedbackQuestion.delete.question');
    await feedbackQuestionDeleteDialog.clickOnConfirmButton();

    expect(await feedbackQuestionComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
