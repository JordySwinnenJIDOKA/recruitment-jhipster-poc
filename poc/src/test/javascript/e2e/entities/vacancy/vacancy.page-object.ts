import { browser, ExpectedConditions, element, by, ElementFinder } from 'protractor';

export class VacancyComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-vacancy div table .btn-danger'));
  title = element.all(by.css('jhi-vacancy div h2#page-heading span')).first();

  async clickOnCreateButton(timeout?: number) {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(timeout?: number) {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons() {
    return this.deleteButtons.count();
  }

  async getTitle() {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class VacancyUpdatePage {
  pageTitle = element(by.id('jhi-vacancy-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  codeInput = element(by.id('field_code'));
  descriptionInput = element(by.id('field_description'));
  creationDateInput = element(by.id('field_creationDate'));
  vacancyStatusSelect = element(by.id('field_vacancyStatus'));
  characteristicsInput = element(by.id('field_characteristics'));
  skillsInput = element(by.id('field_skills'));
  dailyRoutineInput = element(by.id('field_dailyRoutine'));
  locationInput = element(by.id('field_location'));
  candidateTypesSelect = element(by.id('field_candidateTypes'));

  async getPageTitle() {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setCodeInput(code) {
    await this.codeInput.sendKeys(code);
  }

  async getCodeInput() {
    return await this.codeInput.getAttribute('value');
  }

  async setDescriptionInput(description) {
    await this.descriptionInput.sendKeys(description);
  }

  async getDescriptionInput() {
    return await this.descriptionInput.getAttribute('value');
  }

  async setCreationDateInput(creationDate) {
    await this.creationDateInput.sendKeys(creationDate);
  }

  async getCreationDateInput() {
    return await this.creationDateInput.getAttribute('value');
  }

  async setVacancyStatusSelect(vacancyStatus) {
    await this.vacancyStatusSelect.sendKeys(vacancyStatus);
  }

  async getVacancyStatusSelect() {
    return await this.vacancyStatusSelect.element(by.css('option:checked')).getText();
  }

  async vacancyStatusSelectLastOption(timeout?: number) {
    await this.vacancyStatusSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async setCharacteristicsInput(characteristics) {
    await this.characteristicsInput.sendKeys(characteristics);
  }

  async getCharacteristicsInput() {
    return await this.characteristicsInput.getAttribute('value');
  }

  async setSkillsInput(skills) {
    await this.skillsInput.sendKeys(skills);
  }

  async getSkillsInput() {
    return await this.skillsInput.getAttribute('value');
  }

  async setDailyRoutineInput(dailyRoutine) {
    await this.dailyRoutineInput.sendKeys(dailyRoutine);
  }

  async getDailyRoutineInput() {
    return await this.dailyRoutineInput.getAttribute('value');
  }

  async setLocationInput(location) {
    await this.locationInput.sendKeys(location);
  }

  async getLocationInput() {
    return await this.locationInput.getAttribute('value');
  }

  async setCandidateTypesSelect(candidateTypes) {
    await this.candidateTypesSelect.sendKeys(candidateTypes);
  }

  async getCandidateTypesSelect() {
    return await this.candidateTypesSelect.element(by.css('option:checked')).getText();
  }

  async candidateTypesSelectLastOption(timeout?: number) {
    await this.candidateTypesSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async save(timeout?: number) {
    await this.saveButton.click();
  }

  async cancel(timeout?: number) {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class VacancyDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-vacancy-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-vacancy'));

  async getDialogTitle() {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(timeout?: number) {
    await this.confirmButton.click();
  }
}
