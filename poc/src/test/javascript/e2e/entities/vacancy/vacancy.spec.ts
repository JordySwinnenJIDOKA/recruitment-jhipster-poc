/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { VacancyComponentsPage, VacancyDeleteDialog, VacancyUpdatePage } from './vacancy.page-object';

const expect = chai.expect;

describe('Vacancy e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let vacancyUpdatePage: VacancyUpdatePage;
  let vacancyComponentsPage: VacancyComponentsPage;
  let vacancyDeleteDialog: VacancyDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.loginWithOAuth('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Vacancies', async () => {
    await navBarPage.goToEntity('vacancy');
    vacancyComponentsPage = new VacancyComponentsPage();
    await browser.wait(ec.visibilityOf(vacancyComponentsPage.title), 5000);
    expect(await vacancyComponentsPage.getTitle()).to.eq('jhipsterRecruitmentApp.vacancy.home.title');
  });

  it('should load create Vacancy page', async () => {
    await vacancyComponentsPage.clickOnCreateButton();
    vacancyUpdatePage = new VacancyUpdatePage();
    expect(await vacancyUpdatePage.getPageTitle()).to.eq('jhipsterRecruitmentApp.vacancy.home.createOrEditLabel');
    await vacancyUpdatePage.cancel();
  });

  it('should create and save Vacancies', async () => {
    const nbButtonsBeforeCreate = await vacancyComponentsPage.countDeleteButtons();

    await vacancyComponentsPage.clickOnCreateButton();
    await promise.all([
      vacancyUpdatePage.setCodeInput('code'),
      vacancyUpdatePage.setDescriptionInput('description'),
      vacancyUpdatePage.setCreationDateInput('2000-12-31'),
      vacancyUpdatePage.vacancyStatusSelectLastOption(),
      vacancyUpdatePage.setCharacteristicsInput('characteristics'),
      vacancyUpdatePage.setSkillsInput('skills'),
      vacancyUpdatePage.setDailyRoutineInput('dailyRoutine'),
      vacancyUpdatePage.setLocationInput('location'),
      vacancyUpdatePage.candidateTypesSelectLastOption()
    ]);
    expect(await vacancyUpdatePage.getCodeInput()).to.eq('code', 'Expected Code value to be equals to code');
    expect(await vacancyUpdatePage.getDescriptionInput()).to.eq('description', 'Expected Description value to be equals to description');
    expect(await vacancyUpdatePage.getCreationDateInput()).to.eq('2000-12-31', 'Expected creationDate value to be equals to 2000-12-31');
    expect(await vacancyUpdatePage.getCharacteristicsInput()).to.eq(
      'characteristics',
      'Expected Characteristics value to be equals to characteristics'
    );
    expect(await vacancyUpdatePage.getSkillsInput()).to.eq('skills', 'Expected Skills value to be equals to skills');
    expect(await vacancyUpdatePage.getDailyRoutineInput()).to.eq(
      'dailyRoutine',
      'Expected DailyRoutine value to be equals to dailyRoutine'
    );
    expect(await vacancyUpdatePage.getLocationInput()).to.eq('location', 'Expected Location value to be equals to location');
    await vacancyUpdatePage.save();
    expect(await vacancyUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await vacancyComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Vacancy', async () => {
    const nbButtonsBeforeDelete = await vacancyComponentsPage.countDeleteButtons();
    await vacancyComponentsPage.clickOnLastDeleteButton();

    vacancyDeleteDialog = new VacancyDeleteDialog();
    expect(await vacancyDeleteDialog.getDialogTitle()).to.eq('jhipsterRecruitmentApp.vacancy.delete.question');
    await vacancyDeleteDialog.clickOnConfirmButton();

    expect(await vacancyComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
