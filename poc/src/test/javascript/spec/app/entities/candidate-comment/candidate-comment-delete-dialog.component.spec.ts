/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { JhipsterRecruitmentTestModule } from '../../../test.module';
import { CandidateCommentDeleteDialogComponent } from 'app/entities/candidate-comment/candidate-comment-delete-dialog.component';
import { CandidateCommentService } from 'app/entities/candidate-comment/candidate-comment.service';

describe('Component Tests', () => {
  describe('CandidateComment Management Delete Component', () => {
    let comp: CandidateCommentDeleteDialogComponent;
    let fixture: ComponentFixture<CandidateCommentDeleteDialogComponent>;
    let service: CandidateCommentService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [JhipsterRecruitmentTestModule],
        declarations: [CandidateCommentDeleteDialogComponent]
      })
        .overrideTemplate(CandidateCommentDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(CandidateCommentDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CandidateCommentService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
