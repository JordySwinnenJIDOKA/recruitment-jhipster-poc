/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { JhipsterRecruitmentTestModule } from '../../../test.module';
import { CandidateCommentDetailComponent } from 'app/entities/candidate-comment/candidate-comment-detail.component';
import { CandidateComment } from 'app/shared/model/candidate-comment.model';

describe('Component Tests', () => {
  describe('CandidateComment Management Detail Component', () => {
    let comp: CandidateCommentDetailComponent;
    let fixture: ComponentFixture<CandidateCommentDetailComponent>;
    const route = ({ data: of({ candidateComment: new CandidateComment(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [JhipsterRecruitmentTestModule],
        declarations: [CandidateCommentDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(CandidateCommentDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(CandidateCommentDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.candidateComment).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
