/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { JhipsterRecruitmentTestModule } from '../../../test.module';
import { CandidateCommentUpdateComponent } from 'app/entities/candidate-comment/candidate-comment-update.component';
import { CandidateCommentService } from 'app/entities/candidate-comment/candidate-comment.service';
import { CandidateComment } from 'app/shared/model/candidate-comment.model';

describe('Component Tests', () => {
  describe('CandidateComment Management Update Component', () => {
    let comp: CandidateCommentUpdateComponent;
    let fixture: ComponentFixture<CandidateCommentUpdateComponent>;
    let service: CandidateCommentService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [JhipsterRecruitmentTestModule],
        declarations: [CandidateCommentUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(CandidateCommentUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CandidateCommentUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CandidateCommentService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new CandidateComment(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new CandidateComment();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
