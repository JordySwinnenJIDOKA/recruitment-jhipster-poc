/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { JhipsterRecruitmentTestModule } from '../../../test.module';
import { CandidateCommentComponent } from 'app/entities/candidate-comment/candidate-comment.component';
import { CandidateCommentService } from 'app/entities/candidate-comment/candidate-comment.service';
import { CandidateComment } from 'app/shared/model/candidate-comment.model';

describe('Component Tests', () => {
  describe('CandidateComment Management Component', () => {
    let comp: CandidateCommentComponent;
    let fixture: ComponentFixture<CandidateCommentComponent>;
    let service: CandidateCommentService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [JhipsterRecruitmentTestModule],
        declarations: [CandidateCommentComponent],
        providers: []
      })
        .overrideTemplate(CandidateCommentComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CandidateCommentComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CandidateCommentService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new CandidateComment(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.candidateComments[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
