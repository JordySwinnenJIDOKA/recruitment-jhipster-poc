/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { JhipsterRecruitmentTestModule } from '../../../test.module';
import { CandidateSubClassDeleteDialogComponent } from 'app/entities/candidate-sub-class/candidate-sub-class-delete-dialog.component';
import { CandidateSubClassService } from 'app/entities/candidate-sub-class/candidate-sub-class.service';

describe('Component Tests', () => {
  describe('CandidateSubClass Management Delete Component', () => {
    let comp: CandidateSubClassDeleteDialogComponent;
    let fixture: ComponentFixture<CandidateSubClassDeleteDialogComponent>;
    let service: CandidateSubClassService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [JhipsterRecruitmentTestModule],
        declarations: [CandidateSubClassDeleteDialogComponent]
      })
        .overrideTemplate(CandidateSubClassDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(CandidateSubClassDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CandidateSubClassService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
