/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { JhipsterRecruitmentTestModule } from '../../../test.module';
import { CandidateSubClassDetailComponent } from 'app/entities/candidate-sub-class/candidate-sub-class-detail.component';
import { CandidateSubClass } from 'app/shared/model/candidate-sub-class.model';

describe('Component Tests', () => {
  describe('CandidateSubClass Management Detail Component', () => {
    let comp: CandidateSubClassDetailComponent;
    let fixture: ComponentFixture<CandidateSubClassDetailComponent>;
    const route = ({ data: of({ candidateSubClass: new CandidateSubClass(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [JhipsterRecruitmentTestModule],
        declarations: [CandidateSubClassDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(CandidateSubClassDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(CandidateSubClassDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.candidateSubClass).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
