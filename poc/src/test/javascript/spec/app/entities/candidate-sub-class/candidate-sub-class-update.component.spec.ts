/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { JhipsterRecruitmentTestModule } from '../../../test.module';
import { CandidateSubClassUpdateComponent } from 'app/entities/candidate-sub-class/candidate-sub-class-update.component';
import { CandidateSubClassService } from 'app/entities/candidate-sub-class/candidate-sub-class.service';
import { CandidateSubClass } from 'app/shared/model/candidate-sub-class.model';

describe('Component Tests', () => {
  describe('CandidateSubClass Management Update Component', () => {
    let comp: CandidateSubClassUpdateComponent;
    let fixture: ComponentFixture<CandidateSubClassUpdateComponent>;
    let service: CandidateSubClassService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [JhipsterRecruitmentTestModule],
        declarations: [CandidateSubClassUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(CandidateSubClassUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CandidateSubClassUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CandidateSubClassService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new CandidateSubClass(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new CandidateSubClass();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
