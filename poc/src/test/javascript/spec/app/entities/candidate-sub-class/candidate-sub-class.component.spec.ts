/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { JhipsterRecruitmentTestModule } from '../../../test.module';
import { CandidateSubClassComponent } from 'app/entities/candidate-sub-class/candidate-sub-class.component';
import { CandidateSubClassService } from 'app/entities/candidate-sub-class/candidate-sub-class.service';
import { CandidateSubClass } from 'app/shared/model/candidate-sub-class.model';

describe('Component Tests', () => {
  describe('CandidateSubClass Management Component', () => {
    let comp: CandidateSubClassComponent;
    let fixture: ComponentFixture<CandidateSubClassComponent>;
    let service: CandidateSubClassService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [JhipsterRecruitmentTestModule],
        declarations: [CandidateSubClassComponent],
        providers: []
      })
        .overrideTemplate(CandidateSubClassComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CandidateSubClassComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CandidateSubClassService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new CandidateSubClass(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.candidateSubClasses[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
