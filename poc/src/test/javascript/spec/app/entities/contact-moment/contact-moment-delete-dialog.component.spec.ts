/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { JhipsterRecruitmentTestModule } from '../../../test.module';
import { ContactMomentDeleteDialogComponent } from 'app/entities/contact-moment/contact-moment-delete-dialog.component';
import { ContactMomentService } from 'app/entities/contact-moment/contact-moment.service';

describe('Component Tests', () => {
  describe('ContactMoment Management Delete Component', () => {
    let comp: ContactMomentDeleteDialogComponent;
    let fixture: ComponentFixture<ContactMomentDeleteDialogComponent>;
    let service: ContactMomentService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [JhipsterRecruitmentTestModule],
        declarations: [ContactMomentDeleteDialogComponent]
      })
        .overrideTemplate(ContactMomentDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ContactMomentDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ContactMomentService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
