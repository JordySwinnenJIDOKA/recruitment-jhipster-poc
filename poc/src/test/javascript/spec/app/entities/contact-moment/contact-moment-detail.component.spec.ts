/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { JhipsterRecruitmentTestModule } from '../../../test.module';
import { ContactMomentDetailComponent } from 'app/entities/contact-moment/contact-moment-detail.component';
import { ContactMoment } from 'app/shared/model/contact-moment.model';

describe('Component Tests', () => {
  describe('ContactMoment Management Detail Component', () => {
    let comp: ContactMomentDetailComponent;
    let fixture: ComponentFixture<ContactMomentDetailComponent>;
    const route = ({ data: of({ contactMoment: new ContactMoment(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [JhipsterRecruitmentTestModule],
        declarations: [ContactMomentDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(ContactMomentDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ContactMomentDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.contactMoment).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
