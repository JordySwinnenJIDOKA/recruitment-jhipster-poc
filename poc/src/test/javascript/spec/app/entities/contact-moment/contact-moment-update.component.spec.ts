/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { JhipsterRecruitmentTestModule } from '../../../test.module';
import { ContactMomentUpdateComponent } from 'app/entities/contact-moment/contact-moment-update.component';
import { ContactMomentService } from 'app/entities/contact-moment/contact-moment.service';
import { ContactMoment } from 'app/shared/model/contact-moment.model';

describe('Component Tests', () => {
  describe('ContactMoment Management Update Component', () => {
    let comp: ContactMomentUpdateComponent;
    let fixture: ComponentFixture<ContactMomentUpdateComponent>;
    let service: ContactMomentService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [JhipsterRecruitmentTestModule],
        declarations: [ContactMomentUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(ContactMomentUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ContactMomentUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ContactMomentService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new ContactMoment(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new ContactMoment();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
