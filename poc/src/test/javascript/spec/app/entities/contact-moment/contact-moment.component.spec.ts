/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { JhipsterRecruitmentTestModule } from '../../../test.module';
import { ContactMomentComponent } from 'app/entities/contact-moment/contact-moment.component';
import { ContactMomentService } from 'app/entities/contact-moment/contact-moment.service';
import { ContactMoment } from 'app/shared/model/contact-moment.model';

describe('Component Tests', () => {
  describe('ContactMoment Management Component', () => {
    let comp: ContactMomentComponent;
    let fixture: ComponentFixture<ContactMomentComponent>;
    let service: ContactMomentService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [JhipsterRecruitmentTestModule],
        declarations: [ContactMomentComponent],
        providers: []
      })
        .overrideTemplate(ContactMomentComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ContactMomentComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ContactMomentService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new ContactMoment(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.contactMoments[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
