/* tslint:disable max-line-length */
import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { take, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { ContactMomentService } from 'app/entities/contact-moment/contact-moment.service';
import { IContactMoment, ContactMoment, ContactType, ContactContext } from 'app/shared/model/contact-moment.model';

describe('Service Tests', () => {
  describe('ContactMoment Service', () => {
    let injector: TestBed;
    let service: ContactMomentService;
    let httpMock: HttpTestingController;
    let elemDefault: IContactMoment;
    let expectedResult;
    let currentDate: moment.Moment;
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = {};
      injector = getTestBed();
      service = injector.get(ContactMomentService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new ContactMoment(0, ContactType.PHONE_CALL, currentDate, ContactContext.INTRODUCTION, 'AAAAAAA', 'AAAAAAA');
    });

    describe('Service methods', () => {
      it('should find an element', async () => {
        const returnedFromService = Object.assign(
          {
            contactDate: currentDate.format(DATE_FORMAT)
          },
          elemDefault
        );
        service
          .find(123)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: elemDefault });
      });

      it('should create a ContactMoment', async () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            contactDate: currentDate.format(DATE_FORMAT)
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            contactDate: currentDate
          },
          returnedFromService
        );
        service
          .create(new ContactMoment(null))
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should update a ContactMoment', async () => {
        const returnedFromService = Object.assign(
          {
            contactType: 'BBBBBB',
            contactDate: currentDate.format(DATE_FORMAT),
            contactContext: 'BBBBBB',
            interviewer: 'BBBBBB',
            location: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            contactDate: currentDate
          },
          returnedFromService
        );
        service
          .update(expected)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should return a list of ContactMoment', async () => {
        const returnedFromService = Object.assign(
          {
            contactType: 'BBBBBB',
            contactDate: currentDate.format(DATE_FORMAT),
            contactContext: 'BBBBBB',
            interviewer: 'BBBBBB',
            location: 'BBBBBB'
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            contactDate: currentDate
          },
          returnedFromService
        );
        service
          .query(expected)
          .pipe(
            take(1),
            map(resp => resp.body)
          )
          .subscribe(body => (expectedResult = body));
        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a ContactMoment', async () => {
        const rxPromise = service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
