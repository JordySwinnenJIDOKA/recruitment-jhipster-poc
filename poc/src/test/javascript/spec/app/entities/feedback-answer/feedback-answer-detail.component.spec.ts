/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { JhipsterRecruitmentTestModule } from '../../../test.module';
import { FeedbackAnswerDetailComponent } from 'app/entities/feedback-answer/feedback-answer-detail.component';
import { FeedbackAnswer } from 'app/shared/model/feedback-answer.model';

describe('Component Tests', () => {
  describe('FeedbackAnswer Management Detail Component', () => {
    let comp: FeedbackAnswerDetailComponent;
    let fixture: ComponentFixture<FeedbackAnswerDetailComponent>;
    const route = ({ data: of({ feedbackAnswer: new FeedbackAnswer(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [JhipsterRecruitmentTestModule],
        declarations: [FeedbackAnswerDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(FeedbackAnswerDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(FeedbackAnswerDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.feedbackAnswer).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
