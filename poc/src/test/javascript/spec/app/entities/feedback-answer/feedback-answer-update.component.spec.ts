/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { JhipsterRecruitmentTestModule } from '../../../test.module';
import { FeedbackAnswerUpdateComponent } from 'app/entities/feedback-answer/feedback-answer-update.component';
import { FeedbackAnswerService } from 'app/entities/feedback-answer/feedback-answer.service';
import { FeedbackAnswer } from 'app/shared/model/feedback-answer.model';

describe('Component Tests', () => {
  describe('FeedbackAnswer Management Update Component', () => {
    let comp: FeedbackAnswerUpdateComponent;
    let fixture: ComponentFixture<FeedbackAnswerUpdateComponent>;
    let service: FeedbackAnswerService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [JhipsterRecruitmentTestModule],
        declarations: [FeedbackAnswerUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(FeedbackAnswerUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(FeedbackAnswerUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(FeedbackAnswerService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new FeedbackAnswer(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new FeedbackAnswer();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
