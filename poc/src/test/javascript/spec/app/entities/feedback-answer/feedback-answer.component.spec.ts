/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { JhipsterRecruitmentTestModule } from '../../../test.module';
import { FeedbackAnswerComponent } from 'app/entities/feedback-answer/feedback-answer.component';
import { FeedbackAnswerService } from 'app/entities/feedback-answer/feedback-answer.service';
import { FeedbackAnswer } from 'app/shared/model/feedback-answer.model';

describe('Component Tests', () => {
  describe('FeedbackAnswer Management Component', () => {
    let comp: FeedbackAnswerComponent;
    let fixture: ComponentFixture<FeedbackAnswerComponent>;
    let service: FeedbackAnswerService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [JhipsterRecruitmentTestModule],
        declarations: [FeedbackAnswerComponent],
        providers: []
      })
        .overrideTemplate(FeedbackAnswerComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(FeedbackAnswerComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(FeedbackAnswerService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new FeedbackAnswer(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.feedbackAnswers[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
