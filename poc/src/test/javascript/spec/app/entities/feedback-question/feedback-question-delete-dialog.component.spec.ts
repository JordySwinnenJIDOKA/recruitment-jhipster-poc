/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { JhipsterRecruitmentTestModule } from '../../../test.module';
import { FeedbackQuestionDeleteDialogComponent } from 'app/entities/feedback-question/feedback-question-delete-dialog.component';
import { FeedbackQuestionService } from 'app/entities/feedback-question/feedback-question.service';

describe('Component Tests', () => {
  describe('FeedbackQuestion Management Delete Component', () => {
    let comp: FeedbackQuestionDeleteDialogComponent;
    let fixture: ComponentFixture<FeedbackQuestionDeleteDialogComponent>;
    let service: FeedbackQuestionService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [JhipsterRecruitmentTestModule],
        declarations: [FeedbackQuestionDeleteDialogComponent]
      })
        .overrideTemplate(FeedbackQuestionDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(FeedbackQuestionDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(FeedbackQuestionService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
