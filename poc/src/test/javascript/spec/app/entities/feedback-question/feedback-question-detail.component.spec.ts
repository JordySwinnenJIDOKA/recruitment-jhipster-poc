/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { JhipsterRecruitmentTestModule } from '../../../test.module';
import { FeedbackQuestionDetailComponent } from 'app/entities/feedback-question/feedback-question-detail.component';
import { FeedbackQuestion } from 'app/shared/model/feedback-question.model';

describe('Component Tests', () => {
  describe('FeedbackQuestion Management Detail Component', () => {
    let comp: FeedbackQuestionDetailComponent;
    let fixture: ComponentFixture<FeedbackQuestionDetailComponent>;
    const route = ({ data: of({ feedbackQuestion: new FeedbackQuestion(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [JhipsterRecruitmentTestModule],
        declarations: [FeedbackQuestionDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(FeedbackQuestionDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(FeedbackQuestionDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.feedbackQuestion).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
