/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { JhipsterRecruitmentTestModule } from '../../../test.module';
import { FeedbackQuestionUpdateComponent } from 'app/entities/feedback-question/feedback-question-update.component';
import { FeedbackQuestionService } from 'app/entities/feedback-question/feedback-question.service';
import { FeedbackQuestion } from 'app/shared/model/feedback-question.model';

describe('Component Tests', () => {
  describe('FeedbackQuestion Management Update Component', () => {
    let comp: FeedbackQuestionUpdateComponent;
    let fixture: ComponentFixture<FeedbackQuestionUpdateComponent>;
    let service: FeedbackQuestionService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [JhipsterRecruitmentTestModule],
        declarations: [FeedbackQuestionUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(FeedbackQuestionUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(FeedbackQuestionUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(FeedbackQuestionService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new FeedbackQuestion(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new FeedbackQuestion();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
