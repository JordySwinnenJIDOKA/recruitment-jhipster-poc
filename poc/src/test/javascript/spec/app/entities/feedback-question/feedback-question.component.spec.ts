/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { JhipsterRecruitmentTestModule } from '../../../test.module';
import { FeedbackQuestionComponent } from 'app/entities/feedback-question/feedback-question.component';
import { FeedbackQuestionService } from 'app/entities/feedback-question/feedback-question.service';
import { FeedbackQuestion } from 'app/shared/model/feedback-question.model';

describe('Component Tests', () => {
  describe('FeedbackQuestion Management Component', () => {
    let comp: FeedbackQuestionComponent;
    let fixture: ComponentFixture<FeedbackQuestionComponent>;
    let service: FeedbackQuestionService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [JhipsterRecruitmentTestModule],
        declarations: [FeedbackQuestionComponent],
        providers: []
      })
        .overrideTemplate(FeedbackQuestionComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(FeedbackQuestionComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(FeedbackQuestionService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new FeedbackQuestion(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.feedbackQuestions[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
